
self.myInit = function () {
  var ctx = self.ctx;

  // ctx.toPbuTreeData = function (pbu_list, parent_id) {
  //   return _.sortBy(
  //     _.filter(pbu_list, function (sub_pbu) {
  //       return (sub_pbu.parent_id === parent_id);
  //     }),
  //     ['order']
  //   ).map(function (node) {
  //     return {
  //       id: node.subpbu_id,
  //       label: node.parent_id? node.subpbu_name: node.pbu_name,
  //       order: node.order,
  //       children: ctx.toPbuTreeData(pbu_list, node.subpbu_id)
  //     }
  //   });
  // };

  //init date-picker default value, which is previous month.
  var dd = new Date();
  self.data.filter.time_id_s = dd.getFullYear() + '01';
  self.data.filter.time_id_e = dd.getFullYear() + dd.getMonth().toString().padStart(2, '0');

  //初始選單資料
  var foreign_data = JSON.parse(window.foreigndata);
  Promise.all([
    ctx.getDataSources({label: 'overall_actual_pbu_list', params: {user_id: foreign_data.usercode}}),
    ctx.getDataSources({label: 'common_currency_list'}),
    ctx.getDataSources({label: 'overall_actual_version_list'}),
  ]).then(function (results) {

    //將資料加入Root結點
    var all_pbu_list = [{
      id : 0,
      label : "Overall",
      order : 0,
      children : self.toPbuTreeData(results[0])
    }];//因為Tree的Data輸入需要Array，Object會出錯
    var currency_table = results[1];
    var actual_version_table = results[2];
    // console.log("===============================1===============================");
    // console.log(all_pbu_list);
    // console.log("===============================2===============================");

    self.data.all_pbu_list = all_pbu_list;
    self.data.currency_table = currency_table;
    self.data.actual_version_table = actual_version_table;
    // self.data.fraction = self.ctx.self.granularityConfigs.thousand;

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.toPbuTreeData = function ( pbulist ) {
  var pbu_lv1 = [];
  var subpbu_map = {};
  // console.log("toPbuTreeData");

  pbulist.forEach(function (sub_pbu) {
    if(_.find(pbu_lv1 , { "pbu_id" : sub_pbu.pbu_id}) === undefined){
      pbu_lv1.push(sub_pbu);
      _.set(subpbu_map, sub_pbu.pbu_id, []);
    }
    var tmp_ary = _.get(subpbu_map, sub_pbu.pbu_id);
    // console.log(tmp_ary)
    tmp_ary.push(sub_pbu);
    _.set(subpbu_map, sub_pbu.pbu_id, tmp_ary);
  });

  // console.log("=================== LV1");
  // console.log(pbu_lv1);
  // console.log("=================== MAP");
  // console.log(subpbu_map);

  var treeobj = [];
  pbu_lv1.forEach(function (pbu) {
    var childs = [];
    var lv1_id = "";
    
    _.get(subpbu_map,pbu.pbu_id).forEach(function (sub_pbu) {
      var node = {
        id: sub_pbu.subpbu_id,
        label: sub_pbu.subpbu_name,
        order: sub_pbu.order
      };
      lv1_id = lv1_id + sub_pbu.subpbu_id;
      childs.push(node);
    });
    //console.log("toPbuTreeData_TEST_3:" + childs.length + ",pbu=" + pbu.pbu_id);

    if (childs.length === 1) { // 表示自己為母節點
      treeobj.push(childs[0]);
    } else { //表示需要產生一個母節點
      var node = {
        id: lv1_id,
        label: pbu.pbu_name,
        order: pbu.order,
        children: childs
      };
      treeobj.push(node);
    }
  });
  //console.log(treeobj);
  return treeobj;
};

self.searchHandler = function () {
  var params =  self.data.filter;
  params["subpbu_id_list"] = _.map(self.data.pickup_pbu_list,'id');

  // console.log(JSON.stringify(params));

  //TODO: compose search criteria
  self.searchOverallData(params);
};


//===========================   自訂的一些 Methods  @Kenny ===========================
//回傳依據Order排序挑選的Pbu
self.orderedPickupPbuList = function () {
  if(self.data.pickup_pbu_list.length > 0){
    return _.sortBy(Object.values(self.data.pickup_pbu_list), ['order']);
  }else
    return [];
};

//移除Tag使用
self.pbuTagRemove = function (tag) {
  //去出選的ID，將該ID移除，再塞回Tree當中
  self.ctx.self.$refs.tree.setCheckedKeys(_.without(_.map(self.data.pickup_pbu_list,'id'),tag.id));
};

//Tree過濾用
self.pbuFilterBy = function (keyword, item) {
  if (!keyword) return true;
  return item.label.toLowerCase().indexOf(keyword.toLowerCase()) >= 0;
};

//Tree的Checkbox Check用
self.treeNodeCheckChange = function (item, isChecked, haveChildSelected) {
  if (!item.children || item.children.length === 0) {
    if (isChecked) {
      // console.log(":Set=" + item.id);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat(item);
    } else {
      // console.log(":Delete=" + item.id);
      self.data.pickup_pbu_list.splice(_.findIndex(self.data.pickup_pbu_list,{id:item.id}),1);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat();
    }
  }
};

/*** Content相關js 開始 */

self.searchOverallData = function (params) {
  // var params = {
  //   "time_id_s": "202101",
  //   "time_id_e": "202103",
  //   "subpbu_id_list": [1,2,3,4,5],
  //   "actual_version_id": 1,
  //   "currency_id": 1
  // };

  //TODO: fetch data from service api

  self.ctx.executeDataSources({label: 'overall_actual_report', body: params}).then(function (result) {

    // console.log(result);
    var collapse_count = _.filter(result.items, {item_type: 'C'}).length;
    // var current_collapse_index = -1;

    // generate pbu tree structure for UI, table header.
    // 編排 result.pub_list 的內容
    var pbu_group_list = _.values(_.groupBy(_.values(result.subpbu_list), 'pbu_id'));

    // pbu_group_list : 將 pbu_list 與 sub_pbu_list 寫入 list
    pbu_group_list = _.map(pbu_group_list, function (sub_pbu_list) {
      return _.sortBy(sub_pbu_list, ['subpbu_order'])
    });

    // 以 pbu_group_list 排序
    pbu_group_list = _.sortBy(pbu_group_list, [function (pbu) { return pbu[0].subpbu_order; }]);
    // console.log(result.items);

    // pre-process item values
    // 先取出 results.items 的內容
    var _items = self.ctx.lineItemProcessor(result.items, pbu_group_list);

    self.data.table_header_tree = pbu_group_list;
    self.data.actual_report = result;
    self.data.actual_report_items = _items;
    // self.data.inMonthColspan=params.time_id_e-params.time_id_s+1+1;
    // self.data.volume=result.items[0].item_name;
    // console.log("self.data.inMonthColspan : ", self.data.inMonthColspan);
    // console.log("self.data.volume : ", self.data.volume);
    // console.log(self.data.actual_report);
    // console.log(result.subpbu_list);
    // console.log("self.data.table_header_tree = " + self.data.table_header_tree);

    // 把 results.subpbu_list 走一遍，計算全部 subpbu.categories 總合，self.data.column_length 為所有欄位數量
    self.data.column_length = _.reduce(result.subpbu_list, function(column_length, subpbu){
      return column_length + subpbu.categories.length;
    }, 1);
    // console.log("colspan : ", colspan);
    // self.data.column_length = column_length;

    self.data.is_open_group = Array(collapse_count).fill(true);
    self.data.is_highlight_row = Array(result.items.length).fill(false);
    self.data.caption = 'PBU ' + result.time_id_e.toString().substring(0, 4) + ' Actual Dashboard';

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

// 點選 sidebar 時，隱藏與顯示都會更換 icon
self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

// 點選 toggleSidebar 時，把 self.data.is_hide_sidebar 選項變相反： true -> false / false -> true
self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

// 折行效果
self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

// 折行效果
self.toggleItemGroup = function (collapse_index) {
  self.toggleFlagInSet(self.data.is_open_group, collapse_index);
};

// 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
self.tbodyCellSignClass = function (value) {
  var _class = null;
  // var _value = Number(value);
  if (value === 0) { // 0 == 0.0
    _class = 'deuce';   // 把字 color 設定為 initial
  } else {
    _class = (value > 0)? 'success': 'alert';
  }
  return {
    [_class]: _class
  };
};

// // 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
//   var _class = null;
//   // var _value = Number(value);
//   if (value === 0) { // 0 == 0.0
//     _class = 'deuce';
//   } else if (flag === 'positive') {
//     _class = (value > 0)? 'success': 'alert';
//   } else if (flag === 'negative') {
//     _class = (value < 0)? 'success': 'alert';
//   }
//   return {
//     [_class]: _class,
//     [flag]: flag
//   };
// };

// 第算 title 第一列的 colspan
self.tableHeaderPbuColspan = function (sub_pbus) {
  return _.reduce(sub_pbus, function (colspan, sub_pbu) {
    return colspan + sub_pbu.categories.length;
  }, 0);
};

// 根據每一列的 item_type 決定使用的 class
self.tbodyRowClass = function (item, index) {
  var item_type = item.item_type;
  var clazz = [];

  if (self.data.is_highlight_row[index]) {
    clazz.push('highlight');
  }

  return item_type.split('').reduce(function(clazz, type) {
    switch (type) {
      case 'C': //（分類/縮合，灰底粗字）
        clazz.push('category');
        break;
      case 'E': // emphasize（粗體/字體16）
        clazz.push('emphasize');
        break;
      case 'M': //粗體/水藍背景
        clazz.push('mark');
        break;
      case 'U':
        clazz.push('underline');
        break;
      case 'P':
        clazz.push('percentage');
    }
    return clazz;
  }, clazz);
  // return {
  //   'important': item_type.indexOf('S') >= 0 || item_type === 'T',
  //   'summary': item_type === 'S',
  //   'margin-summary': item_type === 'MS',
  //   'margin': item_type === 'M',
  //   'row-collapse': item_type === 'T',
  //   'highlight': self.data.is_highlight_row[index]
  // };
};

// // tooltip 的內容要顯示 raw_data 的 value
// self.tipCurrency = function (value) {
//   return self.ctx.self.toCurrency(value, self.ctx.self.granularityConfigs.raw_data);
// };
//
// // tooltip 的內容要顯示 raw_data 的 value
// self.tipPercentage = function (value) {
//   var config = self.ctx.self.granularityConfigs.raw_data;
//   config.digits_of_percentage = undefined;
//   return self.ctx.self.toPercentage(value, config) + '%';
// };
//
//
// // 定義數值的欄位，最大最小位數
// self.toCurrency = function (value, config) {
//   var fraction = config || self.data.fraction;
//   // return isNaN(value)? value: Number(value / self.data.base).toLocaleString(undefined, {
//   return isNaN(value)? value: Number(value / fraction.base).toLocaleString(undefined, {
//     minimumFractionDigits: fraction.digits_of_currency,
//     maximumFractionDigits: (fraction.digits_of_currency === undefined)? 20: fraction.digits_of_currency
//   });
// };
//
// // 定義小數點的欄位，總共幾位數？ fraction.digits_of_percentage
// self.toPercentage = function (value, config) {
//   var fraction = config || self.data.fraction;
//   return isNaN(value)? value: Number(value * 100).toLocaleString(undefined, {
//     minimumFractionDigits: fraction.digits_of_percentage,
//     maximumFractionDigits: (fraction.digits_of_percentage === undefined)? 20: fraction.digits_of_percentage
//   });
// };

// 頁面點選 radio 取得 config 的設定
self.granularityHandler = function (config) {
  self.data.fraction = config;
};

/**** Excel Export 功能 */


self.exportToExcel = function () {
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.category img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px');//.html('Granularity: ' + self.data.fraction.label);
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.category td').css('background', '#F2F2F2');
  table_box.find('tbody tr.mark td').css('background', '#C9F0FF');
  table_box.find('tbody tr.emphasize td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td.alert').css('color', '#FF4F4F').css('textAlign', 'right');

  table_box.find('span.percentage').each(function () {
    $(this).html($(this).html() + '%');
  });

  // self.ctx.file.exportDataByHtml(table_box.html(), {
  //   filename: self.data.caption,
  //   sheetName: self.data.caption,
  //   style: style
  // });

  // 上龍宮的新寫法(ref龍宮文件)
  var exportData = [{
    sheetname : self.data.caption,
    data: table_box.html()
  }];

  self.ctx.file.exportDataByHtml(exportData, {
    filename: self.data.caption,
    style: style
  });

};

self.decimalPlaceChangeHandler = function (e) {
  self.data.actual_report_items = self.ctx.lineItemProcessor(self.data.actual_report_items, self.data.table_header_tree);
};

/*** Content相關js 結束 */
self.dataComputed = {
  time_id: {
    // getter
    get: function () {
      return [self.data.filter.time_id_s, self.data.filter.time_id_e];
    },
    set: function (value) {
      console.log("dataComputed time_id");
      if(value[0].substring(0,4) === value[1].substring(0,4)){
        self.data.filter.time_id_s = value[0];
        self.data.filter.time_id_e = value[1];
      }else{
        self.data.filter.time_id_s = "";
        self.data.filter.time_id_e = "";
        
        self.ctx.self.$message({
          message: '起訖年份需相同',
          type: 'error'
        });
      }
      
    }
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  },
  numberScaleConfigs: function () {
    return {
      raw_data: {
        label: 'Raw Data',
        number_scale: 1,
      },
      thousand: {
        label: 'Thousand',
        number_scale: 1000,
      },
      million: {
        label: 'Million',
        number_scale: 1000000,
      }
    };
  }
};

self.dataWatch = {
  pbu_text: {
    handler: function (keyword) {
      self.ctx.self.$refs.tree.filter(keyword);
    }
  },
  currency_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.currency_id = newVal.length > 0 ? newVal[0].currency_id : null;
    }
  },
  number_scale: {
    handler: function (newVal, oldVal) {
      self.ctx.lineItemProcessor(self.data.actual_report_items, self.data.table_header_tree);
    }
  }
};

self.onInit = function () {
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  // _ctx.lineItemQuantityHandler = function (base) {
  //   return function (value) {
  //     return isNaN(value)? value: (value / base);
  //   };
  // };
  //
  // _ctx.lineItemMarginHandler = function () {
  //   return function (value) {
  //     // console.log('percentageHandler: ', value);
  //     return isNaN(value)? value: (value * 100);
  //   }
  // };

  _ctx.toCurrency = function (value, fixed) {
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: fixed,
      maximumFractionDigits: (fixed === undefined)? 20: fixed
    });
  };

  _ctx.getGranularityConfig = function (item_type) {
    if (item_type.indexOf('S') >= 0) {
      if (item_type.indexOf('P') >= 0) { // Percentage
        return { decimal_place: 2, number_scale: 1 };
      } else if (item_type.indexOf('Q') >= 0) { // Quantity
        return { decimal_place: 0, number_scale: 1 };
      } else if (item_type.indexOf('N') >= 0) { // Number
        return { decimal_place: 2, number_scale: 1 };
      }
    } else {
      return {
        decimal_place: self.data.decimal_place,
        number_scale: _ctx.self.numberScaleConfigs[self.data.number_scale].number_scale
      };
    }
  };

  _ctx.getItemValueHandlers = function (item_type) {
    if (item_type.indexOf('P') >= 0) {  // 整個row都是百分比，例如margin
      return function (value, config = {}) {
        return _ctx.toCurrency(value * 100, config.decimal_place);
      };
    } else {
      return function (value, config = {}) {
        return _ctx.toCurrency((value / (config.number_scale || 1)), config.decimal_place);
      };
    }
  };

  // _ctx.numberFormatter = function (value, fixed) {
  //   // return isNaN(value)? value: Number(value / self.data.base).toLocaleString(undefined, {
  //   return isNaN(value)? value: Number(value).toLocaleString(undefined, {
  //     minimumFractionDigits: fixed,
  //     maximumFractionDigits: (fixed === undefined)? 20: fixed
  //   });
  // };

  _ctx.lineItemProcessor = function (items, pbu_group_list) {
    // pre-process item values
    // 先取出 results.items 的內容
    var current_collapse_index = -1;
    return items.map(function (item) {

      // 將每個 item 新增一個 item.collapse_index 屬性
      if (item.item_type === 'C') {
        item.collapse_index = ++current_collapse_index;
        return item;
      }

      item.collapse_index = current_collapse_index;

      var config = _ctx.getGranularityConfig(item.item_type);
      var handler = _ctx.getItemValueHandlers(item.item_type);


      //loop in pbu of ui table header
      // 將 sub_pbus mapping 進 pbu_group_list 中
      var item_values = pbu_group_list.map(function (sub_pbus) {

        // 將每一組 sub_pbu mapping 進 sub_pbus 中
        return sub_pbus.map(function (sub_pbu) { // [[x,x,x],[y,y,y],[z,z,z]]

          // 若 categories 的數量-1 =false
          var last_index = sub_pbu.categories.length - 1;
          return sub_pbu.categories.map(function (category, index) { // [x,x,x]
            var value = item.values[sub_pbu.subpbu_id][category];
            return {
              // items fields.value : 每個 item 列往右的每個 value
              value: value,
              value_text: handler(value),
              value_fixed: handler(value, config),
              style: {
                'table-separator': index === last_index
              }
            };
          });
        });
      });
      // console.log('item_values 1: ', item_values);
      // _.flattenDeep 把 array 展平
      item.item_fields = _.flattenDeep(item_values);
      return item;

    });
  };

  self.myInit();

  self.data.init = true;
};
self.onDataUpdated = function () {};
self.onResize = function () {};
self.onDestroy = function () {};