self.onInit = function() {
  var _ctx = self.ctx;

  var _promisify = function(_function, middleware) {
    var _middleware = middleware || function(config) {
      return config;
    };

    return function(config, callback) {
      return new Promise(function(resolve, reject) {
        config = _middleware(config);
        _function(config, function(data) {
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function(config) {
    config.params = {
      params: JSON.stringify(config.params || {})
    };
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function(config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};


self.myInit = function() {
  var ctx = self.ctx;

  var foreign_data = JSON.parse(window.foreigndata);
  self.data.user_id = foreign_data.usercode;
  var params = {
    'user_id': foreign_data.usercode
  };

  console.log("[pbulist] params : ", params);


  ctx.getDataSources({
    label: 'performance_pbu_list',
    params: params
  }).then(function(result) {
    console.log("performance_pbu_list : ", result);

    self.data.pbu_list = result;

    self.data.thisMonth = self.ctx.$router.currentRoute.query.time_id;
    self.data.defaultPBU = self.ctx.$router.currentRoute.query.pbu_id;

    self.data.defaultPBU = !(self.data.defaultPBU) ? result[0].pbu_id : self.data.defaultPBU;
    console.log("self.data.defaultPBU : ", self.data.defaultPBU);
    console.log("self.data.thisMonth : ", self.data.thisMonth);
    self.data.performancePbuList = result;
    
    !(self.data.thisMonth) ? self.changeMonth() : self.changeMonth(new Date(self.data.thisMonth+"-01"));
    

    self.getPerformanceObjectives();

  }).catch(function(err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};


self.goToEdit = function(param) {
  var url = self.data.editUrlPrefix + "time_id="+ self.data.thisMonthFormat+"&pbu_id="+self.data.defaultPBU;
  console.log("url : ", url);
  self.data.editUrl = url;
  window.location.href = url;
};


self.getPerformanceObjectives = function() {
  var params = {};
  params['user_id'] = self.data.user_id;
  params['pbu_id'] = self.data.defaultPBU;
  params['time_id'] = self.data.thisMonth.replace("_","");
  console.log("[objectives] params : ", params);
  ctx.executeDataSources({
    label: 'performance_objectives',
    body: params
  }).then(function(result) {
    console.log("performance_objectives : ", result);
    self.data.performanceObjects = result;
  }).catch(function(err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.changeMonth = function(params) {
  console.log("[changeMonth] params: ", params);
  let pickDate = params ? params : new Date();
  var thisMonth = self.getYearMonth(pickDate);
  var prevMonth = self.getYearMonth(new Date(pickDate.getFullYear(), pickDate.getMonth() - 1, 1));
  self.data.thisMonth = thisMonth.monthValue;
  self.data.prevMonth = prevMonth.monthValue;
  self.data.thisMonthFormat = thisMonth.monthValueFormat;
  self.data.prevMonthFormat = prevMonth.monthValueFormat;
  console.log("self.data.thisMonth : ", self.data.thisMonth);
  console.log("self.data.prevMonth : ", self.data.prevMonth);
  console.log("self.data.thisMonthFormat : ", self.data.thisMonthFormat);
  console.log("self.data.prevMonthFormat : ", self.data.prevMonthFormat);
  self.getPerformanceObjectives();
};

self.getYearMonth = function(params) {
  let pickDate = params ? params : new Date(),
    Y = pickDate.getFullYear(),
    M = pickDate.getMonth() + 1,
    MM = M < 10 ? ('0' + M) : M,
    yearMonth = Y + '' + MM,
    yearMonthFormat = Y + '-' + MM;
  var yearMonthValue = {};
  yearMonthValue['monthValue'] = yearMonth;
  yearMonthValue['monthValueFormat'] = yearMonthFormat;
  return yearMonthValue;
};

self.transitionContent = function(str) {
  let reg = /([\s]+)([0-9]+)(\）)/g;
  let reg1 = /([\s]+)([0-9]{1,2}\.[0-9]{1,2}\s)/g;
  let newHtml = str.replace(reg, "$1" + "<span class='jobName-No'>" + "$2" + "</span>");
  newHtml = newHtml.replace(reg1, "$1" + "<span class='jobName-subNo'>" + "$2" + "</span>");
  if (newHtml.slice(0, 2) === '1）') {
    newHtml = "<span class='jobName-No'>1</span>" + newHtml.slice(2);
  }
  return newHtml;
};
