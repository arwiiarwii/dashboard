self.myInit = function() {
  var ctx = self.ctx;

  console.log("myInit");
  self.data.profile = JSON.parse(window.foreigndata);

  // init date-picker default value, 預設開始為當年度的一月
  var dd = new Date();
  self.data.filter.time_id_s = dd.getFullYear() + '01';
  self.data.filter.time_id_e = dd.getFullYear() + dd.getMonth().toString().padStart(2, '0');
  console.log("data : ", dd);


  // 將pnl_reconciliation_expense_breakdown的API結果整理成差異表格與檢測是否有超過警戒值
  ctx.toDiffTableObj = function(data) {
    console.log("[toDiffTableObj] data : ", data);

    var self = this.self;
    var light_flag = self.status_sign.GREEN; // Green
    console.log(light_flag);

    var ign_range = 99; // USD
    // USD or NTD 
    if (data.currency_name === "NTD") {
      ign_range = 999; // NTD
    }

    console.log("currency_name---------->>",data.currency_name,ign_range);

    // 先收集Thead

    var pnl_headers = _.keys(data.values.pnl);
    var mr_headers = _.keys(data.values.mr);
    var body_list = [];
    // Array.prototype.push.apply(head_list, _.keys(data.values.pnl)); // 將pnl裡的key加入head
    // Array.prototype.push.apply(head_list, _.keys(data.values.mr)); // 將mr裡的key加入head
    var head_list = ["P&L(" + data.currency_name + ")"]
      .concat(pnl_headers, mr_headers, "Difference"); // 最後一欄的Difference
    // console.log('======= head_list: ', head_list);

    // 依據Columns當中逐一取出欄位資訊，並將每列資料加入diff_value中
    data.columns.forEach(function(column) {
      var diff_value = 0;
      var row_data = [column.label]; // 該Row的資料,預設為欄位名稱

      // 針對PNL先取出資料
      _.forEach(pnl_headers, function(key) {
        var tmp = data.values.pnl[key][column.key] || 0;
        diff_value += tmp;
        row_data.push(tmp);
      });

      // 再針對MR取出資料
      _.forEach(mr_headers, function(key) {
        var tmp = data.values.mr[key][column.key] || 0;
        diff_value -= tmp;
        row_data.push(tmp);
      });

      if (Math.abs(diff_value) > ign_range) {
        light_flag = self.status_sign.RED; // Red
      }  

      row_data.push(diff_value); // 將最後一欄的Difference加入
      body_list.push(row_data);
    });

    // comparision_status
    return {
      comparision_status: light_flag,
      head: head_list,
      body: body_list
    };
  };
  
  // ***** 拿到report 的 comp status
  ctx.getBreakdownHandler = function (bu_info, version) {

    return function () {
      var map_key = self.getMapperKey(bu_info, version);

      if (self.data.comparision_status[map_key]) {
        return Promise.resolve();
      }

      // 實作 handler
      var params = self.getBreakdownParameters(bu_info, version);
      return self.ctx.executeDataSources({
        label: 'pnl_reconciliation_expense_breakdown',
        body: params
      }).then(function (data) {

        console.log("params : ", params);
        // console.log("params.comparision_status : ", params.comparision_status);
        // console.log("params.mr_actual_version_id : ", params.mr_actual_version_id, "params.pnl_actual_version_id); : ", params.pnl_actual_version_id);

        // [燈號] 判斷紅綠 get diffTable comp status
        // console.log(" color [G / R]", "params.mr_actual_version_id : ");
        // self.data.diff_table = self.ctx.toDiffTableObj(data);
        var comp_status = self.ctx.toDiffTableObj(data).comparision_status;

        self.ctx.self.$set(self.data.comparision_status, map_key, comp_status);
        // self.data.comparision_status.push({
        //   time_bucode: (params.time_id + params.bu_code),
        //   pnl_actual_version_id: (params.pnl_actual_version_id),
        //   mr_actual_version_id: (params.mr_actual_version_id),
        //   comparision_status: comp_status
        // });
        console.log(
          "timeid + bucode : ", (params.time_id + params.bu_code),
          "self.data.comparision_status[map_key] : " , self.data.comparision_status[map_key]);

      }).catch(function (err) {
        console.log(err);
        self.ctx.self.$message({
          message: 'Something wrong.',
          type: 'error'
        });
      });
    };
  };

  // 初始選單資料
  Promise.all([
    ctx.getDataSources({
      label: 'pnl_reconciliation_pbu_list',
      params: {
        user_id: self.data.profile.usercode
      }
    }),
    ctx.getDataSources({
      label: 'common_currency_list'
    }),
    ctx.getDataSources({
      label: 'pnl_reconciliation_version_list_actual'
    })
  ]).then(function(results) {

    var pbu_table = results[0];
    var currency_table = results[1];
    var actual_version_table = results[2];

    var all_pbu_list = self.toPbuTreeData(pbu_table);
    console.log("all_pbu_list", all_pbu_list);

    self.data.tree_pbu_list = [];
    if (all_pbu_list !== undefined) {
      self.data.tree_pbu_list = [all_pbu_list]; // 因為Tree的Data輸入需要Array，Object會出錯
    }

    self.data.pbu_table = pbu_table;
    self.data.currency_table = currency_table;
    self.data.actual_version_table = actual_version_table;

    // self.getPnlReconciliationReport({});

  }).catch(function (err) {


    console.log(err);
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

// 按下搜尋鈕回傳的大 table 
self.getPnlReconciliationReport = function(params) {
  console.log("[getPnlReconciliationReport] params : ", params);

  self.data.loading_report = true;
  // 清掉舊資料
  self.data.pnl_reconciliation_report_detail = null;
  self.data.comparision_status = {};
  self.data.month_list = [];


  self.ctx.executeDataSources({
    label: 'pnl_reconciliation_report',
    body: params
  }).then(function(response) {

    console.log("[pnl_reconciliation_report_detail] result: ", response);
    self.data.pnl_reconciliation_report_detail = response;
    self.data.month_list = response["month_list"];
    self.data.loading_report = false;
    self.group_param_list(response);

  }).catch(function(e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};

self.getMapperKey = function (bu_info, version) {
  return version.time_id + bu_info.bu_code;
};

self.getBreakdownParameters = function (bu_info, version) {
  return {
    bu_code: bu_info.bu_code,
    currency_id: self.data.pnl_reconciliation_report_detail.currency_id,
    pnl_actual_version_id: version.pnl_actual_version_id,
    mr_actual_version_id: version.mr_actual_version_id,
    time_id: version.time_id
  };
};

self.group_param_list = function (reconciliation_report) {

  // var param_list = [];
  var breakdown_handler_mapper = {};
  // console.log(result, " [currency_id] = ", currency_id);

  reconciliation_report.bu_list.forEach(function (bu_info, index) {
    // console.log("bu_info.version_list");
    // console.log(Object.entries(bu_info.version_list));

    _.forEach(bu_info.version_list, function (version) {

      // console.log(self.data.bu_info);
      // console.log(self.data.bu_info.time_bucode);
      // console.log("version: [", index, "]", version);

      var map_key = self.getMapperKey(bu_info, version);

      /**
       * 以灰燈優先，如果不是灰燈，則先確定該BU CODE還沒被設定過燈號之後，再判斷黃燈，
       * 否則如果曾經被設定黃燈，後來新增同BU CODE的subpbu或是某些版本被unlock導致版本失效卻忘記移除燈號，
       * 就會導致燈號錯亂
       **/
      if (version.pnl_actual_version_id === null
        || version.mr_actual_version_id === null
        || version.pnl_actual_version_id !== version.mr_actual_version_id) {
        // 版本不一致就直接灰燈
        self.ctx.self.$set(self.data.comparision_status, map_key, self.data.status_sign.GRAY);
      } else if (!self.data.comparision_status[map_key] && version.comparision_status) {
        // [燈號] 把原本的 comparision_status: "Y" or "R" 寫入
        self.ctx.self.$set(self.data.comparision_status, map_key, version.comparision_status);
      } else {
        // var param = self.getBreakdownParameters(bu_info, version);
        breakdown_handler_mapper[map_key] = self.ctx.getBreakdownHandler(bu_info, version);
      }
    });
  });

  console.log("new_param : ", breakdown_handler_mapper);

  var breakdown_handlers = _.chunk(Object.values(breakdown_handler_mapper), self.data.breakdown_handler_concurrency); // 只取 list 的 value
  console.log("param_group : ", breakdown_handlers);


  // 實作 handler()
  breakdown_handlers.reduce(function (_promise, group) {
    return _promise.then(function () {
      var handle = group.map(function (handler) {
        return handler();
      });
      return Promise.all(handle); // function 回傳的 promise
    });
  }, Promise.resolve());

  console.log("comparision_status : ", self.data.comparision_status);

};

self.getComparisionVariable = function (bu_info, version) {
  var key = self.getMapperKey(bu_info, version);
  return {
    [key]: {
      comparision_status: self.data.comparision_status[key],
      version_info: version,
      bu_info: bu_info
    }
  };
};

self.reconciliationLightCss = function (comparision_status) {
  var {RED, YELLOW} = self.data.status_sign;
  var clazz = ['point_' + comparision_status];
  if (self.data.allowChangeComparisionStatus && (RED === comparision_status || YELLOW === comparision_status)) {
    clazz.push('changeable');
  }
  return clazz;
};

self.hidePbuName = function () {
  self.data.is_hide_pbu_name = !self.data.is_hide_pbu_name;
};

self.getPbuNameHtml = function (pbu_name) {
  var segments = pbu_name.split(/\s*>\s*/);
  var not_first = segments.splice(1);
  return `<div>${segments}</div>` + not_first.map(function (segment) {
    return `<div>${segment}</div>`;
  }).join('');
};


self.toPbuTreeData = function(pbulist) {
  // console.log("toPbuTreeData",pbulist);

  var pbu_lv1 = [];
  var subpbu_map = {};

  pbulist.forEach(function(sub_pbu) {
    if (_.find(pbu_lv1, {
        "branch_id": sub_pbu.branch_id
      }) === undefined) {
      pbu_lv1.push(sub_pbu);
      _.set(subpbu_map, sub_pbu.branch_id, []);
    }
    var tmp_ary = _.get(subpbu_map, sub_pbu.branch_id);
    // console.log(tmp_ary)
    tmp_ary.push(sub_pbu);
    _.set(subpbu_map, sub_pbu.branch_id, tmp_ary);
  });

  var treeobj = [];
  pbu_lv1.forEach(function(pbu) {
    var childs = [];
    var lv1_id = "";

    _.get(subpbu_map, pbu.branch_id).forEach(function(sub_pbu) {

      if (self.data.filter.scope.indexOf(sub_pbu.scope) >= 0) {
        var node = {
          id: sub_pbu.subpbu_id,
          label: sub_pbu.subpbu_name,
          scope: sub_pbu.scope,
          order: sub_pbu.order
        };
        lv1_id = lv1_id + sub_pbu.subpbu_id;
        childs.push(node);
      }
    });
    // console.log("toPbuTreeData_TEST_3:" + childs.length + ",pbu=" + pbu.pbu_id);

    //有符合in or out的pbu資料才加入Tree顯示
    if (childs.length > 0) {
      var node = {
        id: lv1_id,
        label: pbu.pbu_name,
        order: pbu.order,
        scope: pbu.scope,
        children: childs
      };
      treeobj.push(node);
    }
  });

  // console.log("treeobj.length",treeobj.length);
  if (treeobj.length > 0) {
    // console.log("return treeobj");
    return {
      id: 0,
      label: "Overall",
      order: 0,
      children: treeobj
    };
  } else {
    // console.log("return undefined");
    return undefined;
  }
};


self.searchHandler = function() {
  var params = {
    "time_id_s": self.data.filter.time_id_s,
    "time_id_e": self.data.filter.time_id_e,
    "actual_version_id": self.data.filter.actual_version_id,
    "currency_id": self.data.filter.currency_id,
    "subpbu_id_list": _.map(self.data.pickup_pbu_list, 'id')
  };

  console.log(JSON.stringify(params)); // 搜尋條件
  console.log("params : ", params);

  // self.data.comparision_status = [];
  self.getPnlReconciliationReport(params);



};

// 回傳依據Order排序挑選的Pbu
self.orderedPickupPbuList = function() {
  if (self.data.pickup_pbu_list.length > 0) {
    return _.sortBy(Object.values(self.data.pickup_pbu_list), ['order']);
  } else{
    return [];
  }
};

// 移除Tag使用
self.pbuTagRemove = function(tag) {
  // 找出選的ID，將該ID移除，再塞回Tree當中
  self.ctx.self.$refs.tree.setCheckedKeys(_.without(_.map(self.data.pickup_pbu_list, 'id'), tag.id));
};

// Tree過濾用
self.pbuFilterBy = function(keyword, item) {
  if (!keyword) return true;
  return item.label.toLowerCase().indexOf(keyword.toLowerCase()) >= 0;
};

// Tree的Checkbox Check用
self.treeNodeCheckChange = function(item, isChecked, haveChildSelected) {
  if (!item.children || item.children.length === 0) {
    if (isChecked) {
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat(item);
    } else {
      self.data.pickup_pbu_list.splice(_.findIndex(self.data.pickup_pbu_list, {
        id: item.id
      }), 1);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat();
    }
  }
};

self.scopeChange = function() {

  var tmp_list = [];
  // 將目前已經選的依據in or out scope過濾留下符合的
  self.data.filter.scope.forEach(function(s) {
    tmp_list.push(_.filter(self.data.pickup_pbu_list, ['scope', s]));
  });

  self.data.pickup_pbu_list = []; // 清空已經選的PBU
  self.data.tree_pbu_list = [];

  var treedata = self.toPbuTreeData(self.data.pbu_table);
  if (treedata !== undefined){
    self.data.tree_pbu_list = [treedata]; // 因為Tree的Data輸入需要Array，Object會出錯
  }

  if (tmp_list.length > 0) {
    // 設定treecheck
    self.ctx.self.$refs.tree.setCheckedKeys(_.map(tmp_list[0], 'id'));
    // 將pub重設定回pbu_list產生Tag
    self.data.pickup_pbu_list = tmp_list[0];
  }
};

self.dataComputed = {
  time_id: {
    get: function() {
      return [self.data.filter.time_id_s, self.data.filter.time_id_e];
    },
    set: function(value) {
      self.data.filter.time_id_s = value[0];
      self.data.filter.time_id_e = value[1];
    }
  },
  disableSearch: function() {
    if (_.map(self.data.pickup_pbu_list, 'id').length === 0) {
      return true;
    } else {
      return _.reduce(self.data.filter, function(result, value) {
        return result || !(_.isNumber(value) || !_.isEmpty(value));
      }, false);
    }
  }
};


self.dataWatch = {
  pbu_text: {
    handler: function(keyword) {
      self.ctx.self.$refs.tree.filter(keyword);
    }
  },
  actual_version_table: {
    handler: function(newVal, oldVal) {
      self.data.filter.actual_version_id = newVal.length > 0 ? newVal[0].version_id : null;
    }
  },
  currency_table: {
    handler: function(newVal, oldVal) {
      self.data.filter.currency_id = newVal.length > 0 ? newVal[0].currency_id : null;
    }
  },
  in_scope : {
    handler: function(check_value) {
      if(check_value){
        self.data.filter.scope.push("I");
      }else{
        _.remove(self.data.filter.scope, function(n) {
          return n === "I";
        });
      }
      self.scopeChange();
    }
  },
  out_scope : {
    handler: function(check_value) {
      if(check_value){
        self.data.filter.scope.push("O");
      }else{
         _.remove(self.data.filter.scope, function(n) {
          return n === "O";
        });
      }
      self.scopeChange();
    }
  },
  profile: {
    handler: function() {
      if (self.data.profile) {
        self.data.allowUpload =  (self.data.profile.insightusergroups.indexOf('CM_Reconciliation') >= 0);
        self.data.allowChangeComparisionStatus = (self.data.profile.insightusergroups.indexOf('CM_PL_D') >= 0);
      }
    }
  },
  import_step: {
    handler: function (newVal, oldVal) {
      if (newVal !== 4)
        self.data.progress_step = newVal;
    }
  }
};


self.data.reconciliation_diff_table = {
  
  template: "<div><table> " +
  "<thead> <tr> <th v-for=\"(head, i) in diffTable.head\"> <div> {{head}} </div> </th> </tr> </thead> " +
  "<tbody> <tr v-for=\"(row, i) in diffTable.body\"> <td v-for=\"cell,j in row\" > " +
  "<span v-if=\"(j == row.length-1) && isAlert(cell)\" style='color:red'>{{toFixedFloatValue(cell)}}</span>" + 
  "<span v-else>{{toFixedFloatValue(cell)}}</span>" + 
  "</td></tr></tbody></table> </div>",

  props: ['diffTable'],

  methods: {
    toFixedFloatValue : function (value) {
      return isNaN(value)? value: Number(value).toLocaleString(undefined, {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      });
    },

    isAlert : function (value){
      return Math.abs(value) > 0;
    }
  }
};

self.onInit = function() {

  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function(_function, middleware) {
    var _middleware = middleware || function(config) {
      return config;
    };

    return function(config, callback) {
      return new Promise(function(resolve, reject) {
        config = _middleware(config);
        // console.log("[config]: ", config);
        _function(config, function(data) {
          // error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function(config) {
    config.params = {
      params: JSON.stringify(config.params || {})
    };
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function(config) {
    config.body = config.body || {};
    return config;
  });

  console.log("[pnl-reconciliation.js] onInit");
  self.myInit();

  self.data.init = true;
};


self.toggleSidebar = function() {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
  // self.toggleResizeFlag();
};

self.sidebarToggleIconSrc = function(isHideSidebar) {
  var img_name = isHideSidebar ? 'sidebar-close.svg' : 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.dialog_information = function() {
  self.data.dialog_information_visible = true;
};


self.dialog_reconciliation = function(key, payload) {
  if (self.data.status_sign.GRAY === payload.comparision_status) {
    return ;
  }

  self.data.diff_table = {}; //先清掉舊的資料
  self.data.dialog_reconciliation_visible = true;
  //TODO: 增加loading效果

  // 傳入該欄位相關的資訊
  var req_body = self.getBreakdownParameters(payload.bu_info, payload.version_info);

  self.ctx.executeDataSources({
    label: 'pnl_reconciliation_expense_breakdown',
    body: req_body
  }).then(function(data) {
    // console.log("[payload] : ", payload);
    // console.log("[data] : ", data);
    self.data.diff_table = self.ctx.toDiffTableObj(data);
    // console.log("dialog_reconciliation==>", self.data.diff_table);

  }).catch(function(err) {
    console.log(err);
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.dialog_change_light = function(key, payload) {
  var comparision_status = payload.comparision_status;
  var bu_info = payload.bu_info;
  var version_info = payload.version_info;

  // 如果不是羣經管權限，不能切換燈號，所以忽略
  if (self.data.status_sign.GRAY === comparision_status || !self.data.allowChangeComparisionStatus) {
    return;
  }

  // console.log("timeid : ", timeid, "    bucode : ", bucode, "    color : ", color);
  if ((comparision_status === self.data.status_sign.RED) || (comparision_status === self.data.status_sign.YELLOW)) {
    self.data.click_light = {
      "key": key,
      "time_id": version_info.time_id,
      "bu_code": bu_info.bu_code,
      "pnl_actual_version_id": version_info.pnl_actual_version_id,
      "mr_actual_version_id": version_info.mr_actual_version_id,
      "color": comparision_status
    };

    console.log("click_light : ", self.data.click_light);
    self.data.dialog_change_light_visible = true;
  }
};

self.dialog_change_light_comp_status = function() {
  var comp_key = self.data.click_light["key"];
  var color = self.data.click_light["color"];
  var label = "";

  // 新的燈號呼叫 API 寫入
  var set_comp_status_param = self.data.click_light;
  set_comp_status_param["user_id"] = self.data.profile.usercode;
  console.log("COLOR: ", color);

  // 顯示換燈號
  if (color === self.data.status_sign.RED){
    console.log("from R to Y");
    set_comp_status_param["color"] = self.data.status_sign.YELLOW;
    label = "pnl_reconciliation_set_bu_comparision_status_warning";
  } else if (color === self.data.status_sign.YELLOW) {
    console.log("from Y to R");
    set_comp_status_param["color"] = self.data.status_sign.RED;
    label = "pnl_reconciliation_set_bu_comparision_status_fail";
  } else {
    return ;
  }

  console.log("set_comp_status_param : ", set_comp_status_param, "click_light : ", self.data.click_light);

  self.ctx.executeDataSources({
    label: label,
    body: set_comp_status_param
  }).then(function (result) {
    console.log("api : ", label, ", body = ", set_comp_status_param);
    self.ctx.self.$set(self.data.comparision_status, comp_key, set_comp_status_param["color"]);
    self.data.dialog_change_light_visible = false;
    self.ctx.self.$message( {
      message: '修改成功！',
      type: 'success'
    });
  }).catch(function (e) {
    self.ctx.self.$message( {
      message: 'Something wrong.',
      type: 'error'
    });
  });

};


// ==== 管報上傳 ====
self.openUploadDialog = function () {
  self.ctx.self.$refs.file_input.click();
};

self.progressStepPointCss = function (myStep) {
  return {
    done: self.data.progress_step > myStep,
    ongoing: self.data.progress_step === myStep,
    pending: self.data.progress_step < myStep
  }
};

self.progressStepConnectCss = function (myStep) {
  return {
    done: self.data.progress_step >= myStep,
    pending: self.data.progress_step < myStep
  }
};

self.resetImportStep = function () {
  self.data.import_step = 1;
  self.data.import_message = null;
};

self.interruptImportStep = function (message) {
  self.data.import_step = 4;
  self.data.import_message = message;
};

self.openImportDialog = function () {
  self.data.is_show_import = true;
};



//
//   var template_pbu = _.find(self.data.pbu_table_raw, {
//     pbu_id: self.data.criteria.pbu_id,
//     subpbu_id: self.data.criteria.subpbu_id
//   });
//
//   var template_version = self.data.actual_version_table.find(function (version) {
//     return version.version_id === self.data.criteria.actual_version_id;
//   });
//
//   return (self.data.criteria.time_id.toString() === time_id)  //equal value, not equal type.
//     && (template_pbu.pbu_name === pbu_name)
//     && (template_pbu.subpbu_name === subpbu_name)
//     && (template_version.version_name.toString() === version_name); //equal value, not equal type.
// };

self.getUploadFile = function (e) {
  self.data.import_step = 2;
  var files = e.target.files;
  console.log(files);

  self.ctx.file.parseToJson(files[0]).then(function (val) {
    var sheets = val.results;
    // sheet count
    console.log(sheets);
    var key = Object.keys(sheets).find(function (key) {
      return key.trim() === '管報上傳template';
    });
    var sheet = sheets[key];
    if (!sheet)
      throw 'No sheet available.';

    //validate criteria
    // if (!self.validateImportCriteria(sheet)) {
    //   throw 'Please check YearMonth, PBU, SubPBU and Version are match to your target.';
    // }

    var headers = sheet[0];
    var items = sheet.slice(1).reduce(function (collection, row) {
      var start_index = 5;
      var bu_code = row[0];
      var month = row[2];
      var version = row[3] + "";
      var currency = row[4];

      // check necessary information
      if (_.isEmpty(bu_code) || !_.isInteger(month) || _.isEmpty(version) || _.isEmpty(currency)) {
        return collection;
      }

      // console.log(bu_code, month, version, currency);

      var res = {
        bu_code: bu_code,
        time_id: month,
        actual_version: version,
        currency: currency
      };
      for (var i = start_index; i < headers.length; i++) {
        var n = Number(row[i]);
        if (isNaN(n)) {
          throw '[' + bu_code + '-' + headers[i] + '] not a number.';
        }
        res[headers[i]] = n;
      }
      return collection.concat(res);
    }, []);

    var payload = {
      user_id: self.data.profile.usercode,
      items
    };
    console.log(payload);

    self.ctx.executeDataSources({
      label: 'pnl_reconciliation_mr_expense_save',
      body: payload
    }).then(function (res) {
      if (res.fail > 0) {
        throw res.fail.msg;
      }

      console.log('pnl_reconciliation_mr_expense_save success');
      if (self.data.pnl_reconciliation_report_detail) {
        // self.searchHandler();
        var params = {
          "time_id_s": self.data.pnl_reconciliation_report_detail.time_id_s,
          "time_id_e": self.data.pnl_reconciliation_report_detail.time_id_e,
          "actual_version_id": self.data.pnl_reconciliation_report_detail.actual_version_id,
          "currency_id": self.data.pnl_reconciliation_report_detail.currency_id,
          "subpbu_id_list": _.map(self.data.pnl_reconciliation_report_detail.bu_list, 'pnl_sub_pbu_id')
        };
        self.getPnlReconciliationReport(params);
      }
      self.data.import_step = 3;
    });

  }).catch(function (e) {
    console.error(e);
    self.interruptImportStep(e);
  });

};