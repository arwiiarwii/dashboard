
// ooo-1
self.onInit = function () {
  // alert("onInit");
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  let time_id = null;
  let pbu_id = null;
  
  if (self.ctx.$router.currentRoute !== undefined){
    time_id =  self.ctx.$router.currentRoute.query.time_id || null;
    pbu_id =  self.ctx.$router.currentRoute.query.pbu_id || null;
    console.log(" 龍宮取法 url time_id , pbu_id : " , time_id , pbu_id);
  }
  

  // var url_string = window.location.href;
  // console.log("url_string=" + url_string);
  // var url = new URL(url_string);
  // let tttime_id = url.searchParams.get("time_id");
  // let ttpbu_id = url.searchParams.get("pbu_id");
  // console.log(" URL取法 url tttime_id , pbu_id : " , tttime_id,ttpbu_id);
  
  if (time_id === null && pbu_id === null){
    console.log(" myInit without 參數");
    self.myInit();
  }else{
    console.log(" myInit with=" , time_id, pbu_id);
    self.myInit({"time_id": time_id , "pbu_id": "" + pbu_id});
  }

  self.data.init = true;
};

// ooo-2
self.myInit = function (init_params) {
    var ctx = self.ctx;
    self.data.loginUserInfo = window.foreigndata ? JSON.parse(window.foreigndata) : {
      "usercode": "CM_DEV_UC"
    };
      
    //根據傳入的PBU ID取得PBU相關資訊,並設定目前PBU
    ctx.getDataSources({label: 'performance_pbu_list', params: {user_id: self.data.loginUserInfo.usercode}}).then(function (ret) {
      self.data.pbu_list = ret;

      if(init_params === undefined){
        console.log("default value");
        var now = new Date();
        self.data.monthValue = new Date(now.getFullYear(), now.getMonth(), 1);
        
        self.data.current_pbu_id = self.data.pbu_list[0].pbu_id;
  
      }else{
        console.log("init_params:",init_params);
        let query_time_id = init_params.time_id + "-01";
        console.log(new Date(query_time_id));
        self.data.monthValue = new Date(query_time_id);
        
        self.data.current_pbu_id = init_params.pbu_id;
      }
  
      console.log("self.data.monthValue",self.data.monthValue);
      console.log("self.data.current_pbu_id",self.data.current_pbu_id);
  
      self.data.currentDate = new Date();
      //self.getGmoEmpTenantCode(self.data.loginUserInfo.usercode);
  
      self.initCarousel();
      //self.getRecentMonth(nextMonth);
      self.monthValueChange(self.data.monthValue);
      
    }).catch(function (err) {
      console.log("performance_pbu_list");
      console.log(err);
      self.ctx.self.$message({
        message: 'Something wrong.',
        type: 'error'
      });
    });
    
};

self.getPSReportObjective = function () {
  
  self.data.workContentList = [];
  // let emp_id = self.data.currentPerson.emp_id,
  let emp_id = "GMO";
  //let thisMonth_id = "" + self.data.timerObj.year + self.data.timerObj.month.toString().padStart(2, '0');
  let thisMonth_id = "" + self.data.timerObj.time_id;
  
  if (!emp_id) {
    return;
  };

  let params = { 
      "time_id": thisMonth_id, 
      "user_id": self.data.loginUserInfo.usercode, 
      "pbu_id": self.data.current_pbu_id
  };
  console.log("query params:",params);
  //取回查詢結果資料
  ctx.executeDataSources({label: 'performance_objectives', body: params}).then(function (results) {
    let oblectivesList = results.objectives;
    let check_objectives = self.data.default_objective_description;
    if(oblectivesList.length < check_objectives.length){
      let new_oblectivesList = [];

      check_objectives.forEach(function(description,index){
        let tmp = _.find(oblectivesList, ['objective_description', description]);
        if(tmp === undefined){
          //表示新增
          tmp = {
            "objective_id" : -1,
            "objective_description" : description,
            "work_note" : {},
          };
        }
        new_oblectivesList.push(tmp);
      });
      oblectivesList = new_oblectivesList;
    }    
    //預設顯示這個月的
    self.processPSReportByMonth(oblectivesList,parseInt(thisMonth_id));

  }).catch(function (err) {
    console.log(err);
    console.log("api performance_objectives error!");
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.processPSReportByMonth = function (objectiveList,thisMonth_id) {
  //self.data.workContentList = objectiveList;
  let workContentList = objectiveList.map(el => {
    let obj = {
      ...el,
      pervMonthList: [],
      thisMonthList: []
    };
    let wn = obj.work_note;
    Object.keys(wn).forEach(function callbackFn(key){
      item = wn[key];
      if (item.time_id * 1 === thisMonth_id * 1) {
        obj.thisMonthList.push(self.toJobItem(item));
      } else if (item.time_id * 1 === thisMonth_id - 1) {
        obj.pervMonthList.push(self.toJobItem(item));
      }
    });
    
    if (obj.thisMonthList.length <= 0) {
      let monthId = "" + self.data.timerObj.time_id;
      let newItem = self.createJobItem(monthId);
      obj.thisMonthList.push(newItem);
    };
    if (obj.pervMonthList.length <= 0) {
      let monthId = "" + (parseInt(self.data.timerObj.time_id) - 1);
      let newItem = self.createJobItem(monthId);
      obj.pervMonthList.push(newItem);
    };
    self.data.workContentList.push(obj);
    return el;
  });
  self.loadAutoTextarea();
};


// ================================================


self.loadJS = function (url, callback) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  if (typeof (callback) != "undefined") {
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (script.readyState == "loaded" || script.readyState == "complete") {
          script.onreadystatechange = null;
          callback();
        };
      };
    } else {
      script.onload = function () {
        callback();
      };
    };
  };
  script.src = url;
  document.body.appendChild(script);
};


self.processWorkNote = function (item,wc) {
  if(item.isChange && item.job_name !== "" && self.transformJobName(item.job_name) !== ""){
    wc[item.month_id] = {
      time_id: item.month_id,
      note: self.transformJobName(item.job_name)
    };
  }
};

// ooo-?
self.savePSWeekService = function (that) {
  console.log("===savePSWeekService====");
  //self.postPSSaveService(that);
  
  let bl = false;
  let create_list = [];
  let update_list = [];
  let delete_list = self.data.deleteList;

  self.data.workContentList.forEach(objective_item => {
 
    //處理新增objective資料
    if( objective_item.objective_id === -1){
      if (!objective_item.objective_description) {
        bl = true;
      };

      let create_wc = {};   

      objective_item.pervMonthList.forEach(item => {
        self.processWorkNote(item,create_wc);
      });
      
      objective_item.thisMonthList.forEach(item => {
        self.processWorkNote(item,create_wc);
      });

      let create_obj = {
        objective_description : objective_item.objective_description,
      };
      if(Object.keys(create_wc).length > 0){
        create_obj.work_note = create_wc;
      }
      create_list.push(create_obj);

    }else{ //處理更新資料(包含新增的work_note)
      let update_wc = {};
      
      objective_item.pervMonthList.forEach(item => {
        self.processWorkNote(item,update_wc);
      });
      
      objective_item.thisMonthList.forEach(item => {
        self.processWorkNote(item,update_wc);
      });

      if(Object.keys(update_wc).length > 0){
        let update_obj = {
          objective_id : objective_item.objective_id,
          objective_description : objective_item.objective_description,
          work_note : update_wc
        };
        update_list.push(update_obj);
      }
    }
    
  });

  if (bl) {
    that.$message({
      message: 'Objective can not empty！',
      type: 'warning'
    });
    return;
  };

  if (create_list.length <= 0 && update_list.length <= 0 && delete_list.length <= 0) {
    that.$message({
      message: 'Nothing to change！',
      type: 'warning'
    });
    return;
  };
  
  let postbody = {
    "time_id": self.data.timerObj.time_id,
    "user_id": self.data.loginUserInfo.usercode,
    "pbu_id": self.data.current_pbu_id,
    "objectives" : {
      "create" : create_list,
      "update" : update_list,
      "delete" : delete_list
    }
  };
  console.log("(time_id,user_id,pbu_id)===>" , postbody.time_id , postbody.user_id , postbody.pbu_id);
  console.log("create===>" , postbody.objectives.create);
  console.log("update===>" , postbody.objectives.update);
  console.log("delete===>" , postbody.objectives.delete);

  self.ctx.executeDataSources({label: 'performance_objectives_save', body: postbody}).then(function (ret) {
    console.log("api result===>" , ret);
    if (ret && ret.objectives.create.isFail === false &&  ret.objectives.update.isFail === false) {
      that.$message({
        message: 'Save OK！',
        type: 'success'
      });
    } else {
      that.$message({
        message: 'Save Error！',
        type: 'warning'
      });
    };

  }).catch(function (e) {
    console.log("performance_objectives_save error!");
    console.log(e);
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.pbuValueChange = function (obj) {
  //console.log(self.data.currentPBU);
  //self.data.currentPBU = _.find(self.data.pbu_list, ['pbu_id', query_pub_id]);
  self.monthValueChange(self.data.monthValue);
};

self.monthValueChange = function (val) {
  console.log("monthValueChange val=",val.getFullYear(),val.getMonth()+1);
  self.getRecentMonth(val);
  let t = setTimeout(() => {
    //self.getGmoReportObjective();
    self.getPSReportObjective();
  }, 10);
  t = null;
};

self.changeColumns = function () {
  self.data.columns[1].label = (self.data.timerObj.month - 1) + " keypoint";
  self.data.columns[2].label = (self.data.timerObj.month) + " keypoint";
};
self.initCarousel = function () {
  let clientW = document.body.clientWidth;
  // let maxDisplayW = self.data.personnelList.length * self.data.carouselW;
  let maxDisplayW = 0;
  self.data.showRArrow = clientW + Math.abs(self.data.carouselP) <= maxDisplayW;
};
self.addObjective = function () {
  let uuid = self.generateUUID().replace(/-/g, "");
  //let monthId = "" + self.data.timerObj.year + self.data.timerObj.month;
  let time_id = parseInt(self.data.timerObj.time_id);
  let jobItem = self.createJobItem(time_id, new Date().getTime() + 10);
  let jobItem1 = self.createJobItem(time_id - 1);
  let item = {
    objective_id: -1,
    objective_description: "",
    modifiable: 1,
    isAdd: true,
    isChange: false,
    id: uuid,
    thisMonthList: [jobItem],
    pervMonthList: [jobItem1]
  };
  self.data.workContentList.push(item);
};
self.showUrlTagDialog = function (id) {
  console.log("will add tag into :" , id);

  self.data.url_form.name = "";
  self.data.url_form.url = "";
  self.data.url_form.item_id = id;
  
  self.data.dialogVisible = true;
};
self.addUrlTag = function () {
  var $ = self.ctx.$;
  // console.log("id = " , self.data.url_form.item_id);
  // console.log("name = " , self.data.url_form.name);
  // console.log("url = " , self.data.url_form.url);
  // console.log("postition = " , self.data.url_form.postition);
  let url = self.data.url_form.url;
  url = url.indexOf("http") >= 0 ? url : "http://" + url;
  let name = self.data.url_form.name;
  let id = self.data.url_form.item_id;
  if (url !== "" && name !== ""){
    // let id_content = self.ctx.$('#'+self.data.url_form.item_id).val();
    let url_tag = "<a target='blank' href='" + url +"'>" + name + "</a>";
    
    //開始塞資料到該textarea上
    let isMac = /macintosh|mac os x/i.test(navigator.userAgent);
    let reg = /[0-9]+\）/g;
    let oldS = self.ctx.$('#'+id).val();
    let cursurPosition = oldS.length; //固定都放在最後
    let cursurBefore = oldS.slice(0, cursurPosition);
    let cursurAfter = oldS.slice(cursurPosition);
    let arr = cursurBefore.match(reg);

    let num = "";
    if (arr && arr.length > 0) {
      num = "\n" + (arr[arr.length - 1].replace("）", "") * 1 + 1 + "）");
    };
    if (oldS.trim() === "" || arr === null) {
      num = "1）";
    };
    //如果在 ) 後面就不增加一行
    if (oldS.lastIndexOf("）") === cursurPosition-1){
      num = "";
    }

    let newS = (cursurBefore + num + url_tag + cursurAfter);
    $('#'+id).val(newS)[0].dispatchEvent(new Event('input'));
  }
  
  self.data.dialogVisible = false;
};


self.toJobItem = function (work_note) {
  let uuid = self.generateUUID().replace(/-/g, "");
  let item = {
    id: uuid,
    job_name: work_note.note,
    isAdd: false,
    isChange: false,
    month_id: work_note.time_id
  };
  self.loadAutoTextarea();
  return item;
};

self.createJobItem = function (monthId, jobOrder) {
  let uuid = self.generateUUID().replace(/-/g, "");
  let item = {
    id: uuid,
    job_name: "1）",
    isAdd: true,
    isChange: false,
    month_id: "" + monthId
  };
  self.loadAutoTextarea();
  return item;
};

self.deleteWork = function (list, index, item) {
  this.$confirm('Confirm to delete？', '', {
    confirmButtonText: 'Yes',
    cancelButtonText: 'Cancel',
    center: true
  }).then(() => {
    list.splice(index, 1);
    list.map((el, i) => {
      el.objective_id = i + 1;
    });
    if (!item.isAdd) {
      if (item.thisMonthList === undefined) {
        self.data.deleteList.push(item.id);
      } else {
        self.data.deleteList.push(item.id);
        item.thisMonthList.forEach(el => {
          self.data.deleteList.push(el.id);
        });
        item.pervMonthList.forEach(el => {
          self.data.deleteList.push(el.id);
        });
      };
    };
    list.forEach((el, i) => {
      el.objective_id = i + 1;
    });
  }).catch(() => {});
};

/**** 編輯Edit相關Event *****/
self.transformJobName = function (jobName) {
  return jobName === "1）" ? "" : jobName;
};

self.transitionContent = function (str, comparisonStr1) {
  let reg = /([\s]+)([0-9]+)(\）)/g;
  let reg1 = /([\s]+)([0-9]{1,2}\.[0-9]{1,2}\s)/g;
  let newHtml = str.replace(reg, "<br />" + "$1" + "<span class='jobName-No'>" + "$2" + "$3" + "</span>");
  newHtml = newHtml.replace(reg1, "<br />" + "$1" + "<span class='jobName-subNo'>" + "$2" + "</span>");
  arr = newHtml.split("<br />");
  let str1NewHtml = comparisonStr1.replace(reg, "<br />" + "$1" + "<span class='jobName-No'>" + "$2" +
    "$3" +
    "</span>");
  str1NewHtml = str1NewHtml.replace(reg1, "<br />" + "$1" + "<span class='jobName-subNo'>" + "$2" +
    "</span>");
  arr1 = str1NewHtml.split("<br />");
  let supplement = "";
  if (arr1 && arr) {
    let difference = arr1.length - arr.length;
    if (difference > 0) {
      for (let index = 0; index < difference; index++) {
        supplement += `<tr><td style="padding: 8px;"></td></tr>`;
      };
    };
  };
  newHtml = arr.map((el, i) => {
    return `<tr><td style="padding: 8px;">${el}</td></tr>`;
  }).join("");
  newHtml = "<table border='1' cellspacing='0'>" + newHtml + supplement + "</table>";
  if (newHtml.slice(0, 2) === '1）') {
    newHtml = "<span class='jobName-No'>1）</span>" + newHtml.slice(2);
  };
  return newHtml;
};

self.transitionViewContent = function (str) {
  str = self.transformJobName(str);
  // console.log("transitionContent - str : ", str);
  let reg = /([\s]+)([0-9]+)(\）)/g;
  let reg1 = /([\s]+)([0-9]{1,2}\.[0-9]{1,2}\s)/g;
  let newHtml = str.replace(reg, "$1" + "<span class='jobName-No'>" + "$2" + "</span>");
  // console.log("newHtml : ", newHtml);
  newHtml = newHtml.replace(reg1, "$1" + "<span class='jobName-subNo'>" + "$2" + "</span>");
  if (newHtml.slice(0, 2) === '1）') {
    // newHtml = "<span class='jobName-No'>1</span>" + newHtml.slice(2);
    newHtml = "<span class='jobName-No'>1</span>" + newHtml.slice(2);
  };
  return newHtml;
};

self.btnClick = function (type) {
  console.log("btn+" + type);
  let that = this;
  switch (type) {
    case 'view':
      if(self.data.editVisible){
        self.data.mode_msg = "Edit Mode";
      }
      else{
        self.data.mode_msg = "View Mode";
      }
      self.data.editVisible = !self.data.editVisible;
      break;
    case 'save':
      self.savePSWeekService(that);
      break;
    case 'return':
      this.$confirm('Confirm to exit？', '', {
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        center: true
      }).then(() => {
        let timestring = self.data.timerObj.year + "-" +(self.data.timerObj.month).toString().padStart(2, '0');
        let url = self.data.review_url + "?time_id=" + timestring + "&pbu_id=" + self.data.current_pbu_id;
        console.log("go to reviwe =" + url);
        window.location.href = url;
      }).catch(() => {});
      break;
    case 'cancel':
      this.$confirm('Confirm to cancel？', '', {
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        center: true
      }).then(() => {
        self.getPSReportObjective();
        //self.getGmoEmpBasicInfo();
      }).catch(() => {});
      break;
    default:
      break;
  };
};

self.deleteWorkNote = function (objective_id,item){
  self.ctx.$('#'+item.id).parent().hide();
  //self.ctx.$('#'+item.id).parents("div:first").attr('class','background-color:#DCDFE6');
  if (objective_id >= 0){
    self.data.deleteList.push({
      "time_id": item.month_id,
      "objective_id": objective_id
    });
  }
};

self.getMonthStartAndMonthEnd = function (d) {
    let year = d.getFullYear();
    let month = parseInt(d.getMonth()) + 1;
    let monthStart = 1;
    let monthEnd = new Date(year, month, 0).getDate();

    return {
        monthStart,
        monthEnd,
        year: year,
        month: month,
        name: self.data.month_names[month]   
    };
  };

self.getRecentMonth = function (date) {
    let currentT = date.getTime();
    //let dt = 86400000; //一天豪秒數
    // let getRecentMonthList = [
    //   new Date(date.getFullYear(), date.getMonth() - 3, 1).getTime(),
    //   new Date(date.getFullYear(), date.getMonth() - 2, 1).getTime(),
    //   new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime(),
    //   currentT,
    //   new Date(date.getFullYear(), date.getMonth() + 1, 1).getTime(),
    //   new Date(date.getFullYear(), date.getMonth() + 2, 1).getTime()
    // ];
    let getRecentMonthList = [
        new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime(),
        currentT
      ];
    let req = getRecentMonthList.map(el => {
      let r = self.getMonthStartAndMonthEnd(new Date(el));
      let s = {
        type: 'Month',
        value: r.month,
        name : r.name,
        time_id : r.year + "-" + r.month.toString().padStart(2, '0'),
        timeQuantum: `(${r.monthStart} ~ ${r.monthEnd})`
      };
      // console.log("s : ", s);
      return s;
    });
    self.data.timerList = req;
    self.data.timerObj = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      time_id: "" + date.getFullYear() + (date.getMonth() + 1).toString().padStart(2, '0')
    };
    self.changeColumns();
  };

self.getTimerObj = function name(date) {
  date = date ? new Date(date) : new Date();
  return {
    year: date.getFullYear(),
    month: date.getMonth() + 1
  };
};

self.setObjectiveVal = function (el) {
  let allChinese = /^[\u4e00-\u9fa5]+$/i.test(el.objective_description);
  if (allChinese) {
    el.objective_description.length > 20 && (el.objective_description = el.objective_description.substr(0, 20));
  } else {
    el.objective_description.length > 100 && (el.objective_description = el.objective_description.substr(0, 100));
  };
};

self.generateUUID = function () {
  var d = new Date().getTime();
  if (window.performance && typeof window.performance.now === "function") {
    d += performance.now();
  };
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

self.isChangeObjOrJobName = function (params) {
  params.isChange = true;
};

self.defaultPrevent = function (e) {
  let keyCode = e.keyCode || e.which || e.charCode;
  if (keyCode === 9 || keyCode === 13) {
    e.preventDefault();
  };
};

self.inpKeyup = function (list, key, e) {
  let isMac = /macintosh|mac os x/i.test(navigator.userAgent);
  let keyCode = e.keyCode || e.which || e.charCode;
  let ctrlKey = e.ctrlKey;
  let combinationList = [65, 66, 67, 86, 88];

  if (isMac && keyCode === 13 && list[key - 1].inTheInput === -1) {
    list[key - 1].inTheInput = 1;
    return;
  };

  let el = e.target;
  let reg = /[0-9]+\）/g;
  let oldS = list[key - 1].job_name;
  let num = "";
  let subNum = "";
  let cursurPosition = 0;
  cursurPosition = el.selectionStart;
  let cursurBefore = oldS.slice(0, cursurPosition);
  let cursurAfter = oldS.slice(cursurPosition);
  let arr = cursurBefore.match(reg);

  if (keyCode === 9) {
    let reg1 = /[0-9]+\.[0-9]+/g;
    if (arr && arr.length > 0) {
      let lastNumStr = arr[arr.length - 1];
      let lastNum = lastNumStr.replace("）", "");
      let subCursurBefore = cursurBefore.slice(cursurBefore.lastIndexOf(lastNumStr));
      let arr1 = subCursurBefore.match(reg1);
      if (arr1 && arr1.length > 0) {
        subNum = "\n " + lastNum + '.' + (arr1[arr1.length - 1].split(".")[1] * 1 + 1) + " ";
      } else {
        subNum = "\n " + lastNum + ".1 ";
      };
    };
    list[key - 1].job_name = cursurBefore + subNum + cursurAfter;
  } else if (keyCode === 13) {
    if (arr && arr.length > 0) {
      num = "\n" + (arr[arr.length - 1].replace("）", "") * 1 + 1 + "）");
    };
    if (oldS.trim() === "" || arr === null) {
      num = "1）";
    };
    list[key - 1].job_name = cursurBefore + num + cursurAfter;
  };

  setTimeout(() => {
    setCursorPosition(el, cursurPosition + num.length + subNum.length);
    self.autoTextarea(el);
  }, 0);

  function setCursorPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);
    } else if (ctrl.createTextRange) {
      let range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    };
  };
};

self.autoTextarea = function (elem, extra, maxHeight) {
  extra = extra || 0;
  var isFirefox = !!document.getBoxObjectFor || 'mozInnerScreenX' in window,
    isOpera = !!window.opera && !!window.opera.toString().indexOf('Opera'),
    addEvent = function (type, callback) {
      elem.addEventListener ?
        elem.addEventListener(type, callback, false) :
        elem.attachEvent('on' + type, callback);
    },
    getStyle = elem.currentStyle ? function (name) {
      var val = elem.currentStyle[name];
      if (name === 'height' && val.search(/px/i) !== 1) {
        var rect = elem.getBoundingClientRect();
        return rect.bottom - rect.top -
          parseFloat(getStyle('paddingTop')) -
          parseFloat(getStyle('paddingBottom')) + 'px';
      };
      return val;
    } : function (name) {
      return getComputedStyle(elem, null)[name];
    },
    minHeight = parseFloat(getStyle('height'));
  elem.style.resize = 'none';
  var change = function () {
    var scrollTop, height,
      padding = 0,
      style = elem.style;
    if (elem._length === elem.value.length) return;
    elem._length = elem.value.length;
    if (!isFirefox && !isOpera) {
      padding = parseInt(getStyle('paddingTop')) + parseInt(getStyle('paddingBottom'));
    };
    scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    elem.style.height = minHeight + 'px';
    if (elem.scrollHeight > minHeight) {
      if (maxHeight && elem.scrollHeight > maxHeight) {
        height = maxHeight - padding;
        style.overflowY = 'auto';
      } else {
        height = elem.scrollHeight - padding;
        style.overflowY = 'hidden';
      };
      style.height = height + extra + 'px';
      scrollTop += parseInt(style.height) - elem.currHeight;
      document.body.scrollTop = scrollTop;
      document.documentElement.scrollTop = scrollTop;
      elem.currHeight = parseInt(style.height);
    };
  };
  addEvent('propertychange', change);
  addEvent('input', change);
  addEvent('focus', change);
  change();
};

self.loadAutoTextarea = function () {
  setTimeout(() => {
    let textareaList = document.querySelectorAll("textarea");
    textareaList.forEach(el => {
      self.autoTextarea(el);
    });
  }, 10);
};

self.currentTenantValueChange = function (params) {
  self.data.currentTenant = self.data.tenantList.find(el => {
    return el.tenant_code === params;
  });
};

self.onCompositionStart = function (params) {
  params.inTheInput = -1;
};

self.onCompositionEnd = function (params) {
  //有onCompositionStart
};
