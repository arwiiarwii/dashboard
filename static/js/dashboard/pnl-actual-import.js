// 整個檔案內容複製到 https://javascriptcompressor.com/ 進行壓縮
// 貼到deployment/{env}/widget/{page}.json的controllerScript

// use _promisify to make function not only response by callback function, but also return promise.
// ctx.getDataSources = self.promisify(ctx.getDataSources, function() {});
var _promisify = function (_function, middleware) {
  var _middleware = middleware || function (config) {return config;};

  return function (config, callback) {
    return new Promise(function (resolve, reject) {
      config = _middleware(config);
      _function(config, function (data) {
        //error handling
        try {
          if (data.error) {
            throw {
              code: data.error.code,
              message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
            };
          } else if (data instanceof Error) {
            console.error(data);
            throw {
              code: -1,
              message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
            };
          }
          callback && callback(data);
          resolve(data);
        } catch (e) {
          callback && callback(null, e);
          reject(e);
        }
      });
    });
  };
};

self.toFixedFloatCurrency = function (value, fix) {
  return isNaN(value)? value: Number(value).toLocaleString(undefined, {
    minimumFractionDigits: fix,
    maximumFractionDigits: fix
  });
};

self.toggleItemGroup = function (collapse_index) {
  self.ctx.self.$set(self.data.is_open_group, collapse_index, !self.data.is_open_group[collapse_index]);
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

self.tbodyRowClass = function (item) {
  var item_type = item.item_type;
  var is_required = item.is_required;
  var clazz = [];

  return item_type.split('').reduce(function(clazz, type) {
    (is_required === 'Y') && clazz.push('required');
    (!is_required) && clazz.push('disabled');

    switch (type) {
      case 'C': //（分類/縮合，灰底粗字）
        clazz.push('category');
        break;
      case 'E': // emphasize（粗體/字體16）
        clazz.push('emphasize');
        break;
      case 'M': //粗體/水藍背景
        clazz.push('mark');
        break;
      // case 'U':
      //   clazz.push('underline');
      //   break;
      // case 'P':
      //   clazz.push('percentage');
    }
    return clazz;
  }, clazz);
  // return {
  //   'important': item_type.indexOf('S') >= 0 || item_type === 'T',
  //   'summary': item_type === 'S',
  //   'margin-summary': item_type === 'MS',
  //   'margin': item_type === 'M',
  //   'row-collapse': item_type === 'T',
  //   'required': is_required === 'Y',
  //   'disabled': !is_required
  // }
};

self.progressStepPointCss = function (myStep) {
  return {
    done: self.data.progress_step > myStep,
    ongoing: self.data.progress_step === myStep,
    pending: self.data.progress_step < myStep
  }
};

self.progressStepConnectCss = function (myStep) {
  return {
    done: self.data.progress_step >= myStep,
    pending: self.data.progress_step < myStep
  }
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.contentDataPreprocess = function (result) {

  var collapse_count = 0;
  var current_collapse_index = -1;

  // console.log('result.status: ', result.status);
  result.status = _.isEmpty(result.status)? 0: parseInt(result.status);
  // console.log('result.status: ', result.status);
  result.items.forEach(function (item) {
    if (item.item_type === 'C') {
      collapse_count += 1;
      item.collapse_index = ++current_collapse_index;
    } else {
      item.collapse_index = current_collapse_index;
    }
  });

  self.data.criteria = {
    time_id: result.time_id,
    pbu_id: result.pbu_id,
    subpbu_id: result.subpbu_id,
    actual_version_id: result.actual_version_id
  };

  self.data.actual_history = result;
  self.data.products = (result.status === 0)? Array(10).fill({}): result.products;
  self.data.history_items = result.items;
  self.data.is_open_group = Array(collapse_count).fill(true);
  self.data.caption = _.find(self.data.pbu_table_raw, {subpbu_id: result.subpbu_id}).subpbu_name + ' ' +
    moment(result.time_id, 'YYYYMM').format('MMM YYYY') + ' P&L Input';
};

self.searchHandler = function () {
  self.getActualExpense(self.data.filter);
};

self.getActualExpense = function (criteria) {
  // console.log(criteria);
  // console.log(self.data.filter);
  var params = JSON.parse(JSON.stringify(criteria));
  //get data
  self.data.actual_history = null;
  return self.ctx.executeDataSources({label: 'actual_application_history', body: params}).then(function (result) {
    self.contentDataPreprocess(result);
    return result;
  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.lockHandler = function () {
  var params = JSON.parse(JSON.stringify(self.data.criteria));
  self.ctx.executeDataSources({label: 'actual_publish', body: params}).then(function (response) {
    return self.getActualExpense(self.data.criteria);
  }).then(function () {
    self.ctx.self.$message({
      message: 'Lock successfully.',
      type: 'success'
    });
  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Lock failed.',
      type: 'error'
    });
  });
};

self.unlockHandler = function () {
  var params = JSON.parse(JSON.stringify(self.data.criteria));
  self.ctx.executeDataSources({label: 'actual_recall', body: params}).then(function (response) {
    return self.getActualExpense(self.data.criteria);
  }).then(function () {
    self.ctx.self.$message({
      message: 'Unlock successfully.',
      type: 'success'
    });
  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Unlock failed.',
      type: 'error'
    });
  });
};

self.resetImportStep = function () {
  self.data.import_step = 1;
  self.data.import_message = null;
};

self.interruptImportStep = function (message) {
  self.data.import_step = 4;
  self.data.import_message = message;
};

self.openImportDialog = function () {
  self.data.is_show_import = true;
};

self.validateImportCriteria = function (sheet) {
  var time_id = sheet[0][1].toString().trim();
  var pbu_name = sheet[1][1].trim();
  var subpbu_name = sheet[2][1].trim();
  var version_name = sheet[3][1].toString().trim();

  var template_pbu = _.find(self.data.pbu_table_raw, {
    pbu_id: self.data.criteria.pbu_id,
    subpbu_id: self.data.criteria.subpbu_id
  });

  var template_version = self.data.actual_version_table.find(function (version) {
    return version.version_id === self.data.criteria.actual_version_id;
  });

  return (self.data.criteria.time_id.toString() === time_id)  //equal value, not equal type.
    && (template_pbu.pbu_name === pbu_name)
    && (template_pbu.subpbu_name === subpbu_name)
    && (template_version.version_name.toString() === version_name); //equal value, not equal type.
};

self.getUploadFile = function (e) {
  self.data.import_step = 2;
  var files = e.target.files;

  self.ctx.file.parseToJson(files[0]).then(function (val) {
    var sheets = val.results;
    // sheet count
    var key = Object.keys(sheets).find(function (key) {
      return key.trim() === 'PBU P&L Input';
    });
    var sheet = sheets[key];
    if (!sheet)
      throw 'No sheet available.';

    //validate criteria
    if (!self.validateImportCriteria(sheet)) {
      throw 'Please check YearMonth, PBU, SubPBU and Version are match to your target.';
    }

    //get products
    var valid_product_index = [];
    var products = _.map(sheet[4].slice(1, 11), _.toString).filter(function (product, index) {
      var valid = !_.isEmpty(product);
      valid && valid_product_index.push(index);
      return valid;
    });
    // console.log('products: ', products);
    var product_count = products.length;

    if (product_count === 0) {
      throw 'At least one product required.';
    }

    var items = self.data.history_items.filter(function (item) {
      return !!item.is_required;
    }).map(function (item) {

      var is_required = ('Y' === item.is_required);
      var sheet_row = sheet[(item.row - 1)];

      if (is_required && !sheet_row)
        throw 'Item(:' + item.item_name + ') is not exist.';

      return {
        line_item: item.line_item,
        products: products.map(function (product, index) {

          var data_index = valid_product_index[index];
          var value = sheet_row[(item.col - 1 + data_index)];
          var log = {
            item: item.item_name,
            product: product,
            value: value
          };

          if (isNaN(value)) {
            throw 'Unsupported data format(' + JSON.stringify(log) + ').';
          } else if ('Y' === item.is_required && value.length <= 0) {
            throw 'Please fill in necessary field(' + JSON.stringify(log) + ').';
          }
          return {
            product_name: product,
            value: value
          };
        })
      };
    }); // end return

    var foreign_data = JSON.parse(window.foreigndata);
    var payload = Object.assign({products: products, items: items, user_id:  foreign_data.usercode}, self.data.criteria);
    // console.log('payload: ', payload);
    self.storeActualExpense(payload);

  }).catch(function (e) {
    console.error(e);
    self.interruptImportStep(e);
  });

};

self.storeActualExpense = function (payload) {
  self.ctx.executeDataSources({label: 'actual_store', body: payload}).then(function () {
    self.data.import_step = 3;
    self.getActualExpense(self.data.criteria);
  }).catch(function (e) {
    self.data.import_message = e.message;
    self.data.import_step = 4;
  });
};

self.openUploadDialog = function () {
  // this.$refs.uploadButton
  self.ctx.self.$refs.file_input.click();
};

self.dataComputed = {
  cellAmount: function () {
    var solid = 1;
    return solid + self.data.products.length
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  }
};

self.dataWatch = {
  "filter.pbu_id": {
    handler: function (newVal, oldVal) {
      self.data.sub_pbu_table_distinct = self.data.pbu_table_raw.filter(function (pbu) {
        return (pbu.pbu_id === newVal);
      });
      self.data.filter.subpbu_id = (self.data.sub_pbu_table_distinct.length > 0) ?
        self.data.sub_pbu_table_distinct[0].subpbu_id : null;
    }
  },
  import_step: {
    handler: function (newVal, oldVal) {
      if (newVal !== 4)
        self.data.progress_step = newVal;
    }
  }
};

self.onInit = function () {
  self.data.init = true;
  var _ctx = self.ctx;

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  //init date-picker default value, which is previous month.
  var dd = new Date();
  ctx.self.filter.time_id = dd.getFullYear() + ('' + dd.getMonth()).padStart(2, '0');

  //use promise.all to get all necessary filter options concurrently.
  var foreign_data = JSON.parse(window.foreigndata);
  Promise.all([
    _ctx.getDataSources({label: 'common_user_role', params: {user_id: foreign_data.usercode}}),
    _ctx.getDataSources({label: 'actual_pbu_list', params: {user_id: foreign_data.usercode}}),
    _ctx.getDataSources({label: 'actual_version_list'})
  ]).then(function (results) {
    var profile = results[0];
    var pbu_table = results[1];
    var actual_version_table = results[2];
    var pbu_table_distinct = _.uniqBy(pbu_table, 'pbu_id');

    self.data.pbu_table_raw = pbu_table;
    self.data.pbu_table_distinct = pbu_table_distinct;
    self.data.filter.pbu_id = pbu_table_distinct.length > 0? pbu_table_distinct[0].pbu_id: null;
    self.data.profile = profile;
    self.data.is_cm_admin = (_.findIndex(profile, {role_name: 'CM_Admin'}) >= 0);
    self.data.actual_version_table = actual_version_table;
    self.data.filter.actual_version_id = (actual_version_table.length > 0)? actual_version_table[0].version_id: null;

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.onDataUpdated = function () {
};
self.onResize = function () {
};
self.onDestroy = function () {
};