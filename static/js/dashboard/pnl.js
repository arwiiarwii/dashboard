// 整個檔案內容複製到 https://javascriptcompressor.com/ 進行壓縮
// 貼到deployment/{env}/widget/{page}.json的controllerScript

var top_popover_elm = 0;
var ytm_style_sheet = null;
var ytm_hide_keyframes = 'slide';

// use _promisify to make function not only response by callback function, but also return promise.
// ctx.getDataSources = self.promisify(ctx.getDataSources, function() {});
var _promisify = function (_function, middleware) {
  var _middleware = middleware || function (config) {return config;};

  return function (config, callback) {
    return new Promise(function (resolve, reject) {
      config = _middleware(config);
      _function(config, function (data) {
        //error handling
        try {
          if (data.error) {
            throw {
              code: data.error.code,
              message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
            };
          } else if (data instanceof Error) {
            console.error(data);
            throw {
              code: -1,
              message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
            };
          }
          callback && callback(data);
          resolve(data);
        } catch (e) {
          callback && callback(null, e);
          reject(e);
        }
      });
    });
  };
};

self.data.popover_editor = {

  template:
    '<template id="popover-editor-template"> ' +
    '<el-popover trigger="manual" placement="right" width="340" @after-enter="show" ref="popper" :popper-options="popperOptions" popper-class="editable-popper" v-model="isOpen"> ' +
    '<el-card class="edit-card" @click.native="focus"> ' +
    '<div slot="header" class="clearfix header">' +
    '<span>{{caption}}</span> ' +
    '<i class="close-btn el-icon-close" @click="close"> </i>' +
    '</div> ' +
    '<div ref="content" contenteditable="true" class="editor outline-none" v-html="inner_html" @keypress="keypressHandler" @compositionend="compositionendHandler" @paste="pasteHandler"/> ' +
    '</el-card> <div class="el-popover-footer" @click="focus"> ' +
    '<el-button class="save-btn" @click="save">Save</el-button> ' +
    '</div> ' +
    '<el-tooltip slot="reference" popper-class="tooltip" placement="right" :open-delay="500">' +
    '<div slot="content" v-html="inner_html"/>' +
    '<div class="outline-none overflow-hidden" v-html="inner_inline_html"></div> ' +
    '</el-tooltip>' +
    '</el-popover>' +
    '</template>',

  props: ['text', 'rich_text', 'isOpen', 'caption', 'popperOptions', 'resizeFlag', 'updater'],

  data: function () {
    return {
      inner_inline_html: '',
      inner_html: '',
      observer: null
    };
  },

  created: function () {
    this._rich_text = this.rich_text || this.text.replace(/\n/, '<br>') || '';
  },

  mounted: function () {
    var _this = this;
    this.observer = new MutationObserver(mutationRecords => {
      // console.log(mutationRecords); // console.log(the changes)
      var is_number_removed = mutationRecords.some(function (record) {
        return Array.from(record.removedNodes).some(function (node) {
          return ('list-number' === node.className);
        });
      });

      is_number_removed && _this.arrangeLineNumber();
    });

    this.observer.observe(this.$refs.content, {
      childList: true, // 观察直接子节点
      // subtree: true, // 及其更低的后代节点
      // characterDataOldValue: true // 将旧的数据传递给回调
    });
  },

  destroyed: function () {
    this.observer.disconnect();
  },

  methods: {
    compositionendHandler: function (e) {
      var range = window.getSelection().getRangeAt(0);
      var anchor = range.commonAncestorContainer;

      if ("" === e.data && anchor.parentElement.getAttribute('contenteditable') === 'false') {

        var newEle = document.createTextNode("");
        anchor.parentNode.parentNode.insertBefore(newEle, anchor.parentNode.nextSibling);

        range = document.createRange();
        range.setStartAfter(newEle);
        range.collapse(true);
        //
        // //make the cursor there
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);

        // e.preventDefault();
        return false;
      }
    },

    pasteHandler: function (e) {
      var paste = (e.clipboardData)
        ? (e.originalEvent || e).clipboardData.getData('text/plain')
        // For IE
        : (window.clipboardData ? window.clipboardData.getData('Text') : '');
      // console.log(e.clipboardData.getData('text/html'));

      var selection = window.getSelection();
      if (!selection.rangeCount) return false;
      selection.deleteFromDocument();
      var range = selection.getRangeAt(0);
      var docFragment = document.createDocumentFragment();

      paste.split('\n').forEach(function (text, index) {
        if (index > 0) {
          docFragment.appendChild(document.createElement('br'));
        }
        docFragment.appendChild(document.createTextNode(text));
      });
      range.deleteContents();
      range.insertNode(docFragment);
      range.collapse(false);
      //
      selection.removeAllRanges();
      selection.addRange(range);

      this.arrangeLineNumber();

      e.preventDefault();
      // return false;
    },

    keypressHandler: function (e) {
      //the following are chrome only.
      if (e.which !== 13)
        return true;
      //
      // var current = this.$refs.content.querySelectorAll('span.list-number').length;

      var docFragment = document.createDocumentFragment();
      // //add a new line
      var newEle = null;
      newEle = document.createTextNode('\n');
      docFragment.appendChild(newEle);
      newEle =  document.createElement('br');
      docFragment.appendChild(newEle);
      //
      // for chrome: put a span before contenteditable=false to make backspace works!
      newEle = document.createElement('span');
      docFragment.appendChild(newEle);
      //line number span
      newEle = document.createElement('span');
      newEle.className = 'list-number';
      newEle.setAttribute('contenteditable', 'false');
      // var numElm = document.createTextNode(++current + '. ');
      // newEle.append(numElm);
      docFragment.appendChild(newEle);

      newEle = document.createTextNode("");
      docFragment.appendChild(newEle);

      //
      // //make the br replace selection
      var range = window.getSelection().getRangeAt(0);
      range.deleteContents();
      range.insertNode(docFragment);
      //
      // //create a new range
      range = document.createRange();
      range.setStartAfter(newEle);
      range.collapse(true);
      //
      // //make the cursor there
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);

      //re-arrange line number
      this.arrangeLineNumber();
      //
      // return false;
      e.preventDefault();
      // ================

      // console.log('press enter');
      // var sel, range, textNode;
      // if (window.getSelection) {
      //   sel = window.getSelection();
      //   if (sel.getRangeAt && sel.rangeCount) {
      //     range = sel.getRangeAt(0);
      //     range.deleteContents();
      //     textNode = document.createTextNode(text);
      //     range.insertNode(textNode);
      //
      //     // Move caret to the end of the newly inserted text node
      //     range.setStart(textNode, textNode.length);
      //     range.setEnd(textNode, textNode.length);
      //     sel.removeAllRanges();
      //     sel.addRange(range);
      //   }
      // } else if (document.selection && document.selection.createRange) {
      //   range = document.selection.createRange();
      //   range.pasteHTML(text);
      // }

    },

    arrangeLineNumber: function () {
      var $ = self.ctx.$;
      $(this.$refs.content).find('span.list-number').each(function (index) {
        $(this).text((index + 1).toString().padStart(2, 0) + '.')
      });
    },

    close: function () {
      this.$emit('popover-close');
      this.$refs.content.innerHTML = this._rich_text;
      // this.$refs.popper.updatePopper();
    },

    save: function () {
      let inner_html = this.$refs.content.innerHTML;
      let plain_text = this.$refs.content.innerText;
      var param = {
        note: plain_text,
        note_rich_text: inner_html
      };
      this.updater(param);
      this._rich_text = inner_html;
    },

    focus: function (e) {
      var my_popper = e.currentTarget.closest('.el-popover');
      if (my_popper !== top_popover_elm) {
        var myZIndex = my_popper.style.zIndex;
        my_popper.style.zIndex = top_popover_elm.style.zIndex;
        top_popover_elm.style.zIndex = myZIndex;
        top_popover_elm = my_popper;
      }
    },

    show: function () {
      // console.log(this.$refs.popper);
      top_popover_elm = this.$refs.popper.popperElm;
    }
  },

  computed: {
    _rich_text: {
      get() {
        return this.inner_html;
      },
      set(value) {
        this.inner_html = value;
        this.inner_inline_html = value.replace(/<\W*br\W*>/g, '\n');
      }
    }
  },

  watch: {
    resizeFlag: {
      handler: function (newVal, oldVal) {
        // console.log('resizeFlag watcher');
        if (this.isOpen && newVal !== oldVal) {
          // console.log('resizeFlag: ', newVal);
          this.$refs.popper.updatePopper();
        }
      }
    }
  }
};

self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

self.toggleItemGroup = function (collapse_index) {
  self.data.report_items.forEach(function (item, index) {
    if (item.collapse_index === collapse_index) {
      self.toggleFlagInSet(self.data.is_open_deviation_explanation, index, false);
      self.toggleFlagInSet(self.data.is_open_action_item, index, false);
    }
  });
  self.toggleFlagInSet(self.data.is_open_group, collapse_index);
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
  // self.toggleResizeFlag();
};

self.toggleYTM = function () {
  var is_show = !self.data.is_show_YTM;
  self.data.is_show_YTM = is_show;
  var $ = self.ctx.$;
  var tail_class_name = null;
  var anchor_class_name = null;
  var target_event = null;
  var tail_event = null;
  var my_app = $(self.ctx.$element);
  if (is_show) {
    anchor_class_name = 'cell-slide-enter';
    tail_class_name = 'cell-tail-slide-enter';
    target_event = 'animationend';
    tail_event = 'animationend';
  } else {
    anchor_class_name = 'cell-slide-leave';
    tail_class_name = 'cell-tail-slide-leave';
    target_event = 'transitionend';
    tail_event = 'animationend';
  }

  var target = my_app.find('td:first-child ~ td:not(:nth-child(n+6))')
    .add(my_app.find('thead tr:first-child th:nth-child(2)'))
    .add(my_app.find('thead tr:nth-child(2) th:not(:nth-child(n+5))'))
    .addClass(anchor_class_name);
  // console.log('target.length: ', target.length);

  var tail = my_app.find(`.${anchor_class_name} ~ *`)
    .addClass(tail_class_name);
  // console.log('tail.length: ', tail.length);

  var animate_promises = target.map(function (index, dom) {
    return new Promise(function (resolve) {
      $(this).on(target_event, function () {
        $(this).off(target_event);
        resolve(dom);
      });
    });
  }).add(
    tail.map(function (index, dom) {
      return new Promise(function (resolve) {
        $(this).on(tail_event, function () {
          $(this).off(tail_event);
          resolve(dom);
        });
      });
    })
  ).get();
  // console.log('animate_promises: ', animate_promises.length);
  Promise.all(animate_promises).then(function () {
    is_show? target.removeClass('display-none'): target.addClass('display-none');
    target.removeClass(anchor_class_name);
    tail.removeClass(tail_class_name);
    self.toggleResizeFlag();
  });
};

self.toggleProducts = function () {
  self.data.is_show_products = !self.data.is_show_products;
  self.toggleResizeFlag();
};

self.toggleResizeFlag = function () {
  self.data.resize_fired_flag = !self.data.resize_fired_flag;
};

self.tbodyRowClass = function (item, index) {
  var item_type = item.item_type;
  var clazz = [];

  if (self.data.is_highlight_row[index]) {
    clazz.push('highlight');
  }

  return item_type.split('').reduce(function(clazz, type) {
    switch (type) {
      case 'C': //（分類/縮合，灰底粗字）
        clazz.push('category');
        break;
      case 'E': // emphasize（粗體/字體16）
        clazz.push('emphasize');
        break;
      case 'M': //粗體/水藍背景
        clazz.push('mark');
        break;
      case 'U':
        clazz.push('underline');
        break;
      case 'P':
        clazz.push('percentage');
    }
    return clazz;
  }, clazz);

  // return {
  //   'important': item_type.indexOf('S') >= 0 || item_type === 'T',
  //   'summary': item_type === 'S',
  //   'margin-summary': item_type === 'MS',
  //   'margin': item_type === 'M',
  //   'row-collapse': item_type === 'T',
  //   'highlight': self.data.is_highlight_row[index]
  // }
};

self.tbodyCellSignClass = function (value, flag) {
  var _class = null;
  // var _value = Number(value);
  if (value === 0) { // 0 == 0.0
    _class = 'deuce';
  } else if (flag === 'positive') {
    _class = (value > 0)? 'success': 'alert';
  } else if (flag === 'negative') {
    _class = (value < 0)? 'success': 'alert';
  }
  return {
    [_class]: _class,
    [flag]: flag
  };
};

self.theadCollapseIconSrc = function (isShow) {
  return  self.data.assets_path + 'static/media/' + (isShow? 'minus-box.svg': 'plus-box.svg');
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.contentDataPreprocess = function (result) {
  var collapse_count = _.filter(result.items, {item_type: 'C'}).length;


  result.items = self.ctx.lineItemProcessor(result.items);
  self.data.criteria = {
    time_id: result.time_id,
    pbu_id: result.pbu_id,
    subpbu_id: result.subpbu_id,
    actual_version_id: result.actual_version_id,
    budget_version_id: result.budget_version_id,
    currency_id: result.currency_id
  };

  // self.data.pnl_report_raw = result;
  self.data.products = result.products || [];
  // self.data.report_items = result.items;
  self.data.is_open_group = Array(collapse_count).fill(true);
  self.data.is_open_deviation_explanation = Array(result.items.length).fill(false);
  self.data.is_open_action_item = Array(result.items.length).fill(false);
  self.data.is_highlight_row = Array(result.items.length).fill(false);
  self.data.caption = _.find(self.data.pbu_table_raw, {subpbu_id: result.subpbu_id}).subpbu_name + ' ' +
    moment(result.time_id, 'YYYYMM').format('MMM YYYY') + ' P&L';
  self.data.currency = _.find(self.data.currency_table, {currency_id: result.currency_id}).currency_name;
  self.data.pnl_report_raw = result;
  self.data.report_items = result.items;

  // calculate YTM width
  self.data.is_show_YTM = true;
  self.ctx.self.$nextTick()
    .then(function () {
      var ytm_width = self.ctx.$element
        .querySelector('.pnl-table > thead > tr:first-child > th:nth-child(2)')
        .getBoundingClientRect().width;

      self.createKeyframes(ytm_style_sheet, ytm_hide_keyframes, ytm_width);

    });
};

self.createKeyframes = function (style_sheet, name, width) {
  if (style_sheet) {
    document.head.removeChild(style_sheet);
  }
  // console.log(width);
  // console.log(name);
  style_sheet = document.createElement('style');
  style_sheet.type = 'text/css';
  document.head.appendChild(style_sheet);

  // Adding The Keyframes
  style_sheet.sheet.insertRule(`@keyframes ${name}-leave {
        from {
          transform: translateX(0);
        }
        to {
          transform: translateX(-${width}px);
        }
      }`,
    style_sheet.length
  );

  style_sheet.sheet.insertRule(`@keyframes ${name}-enter {
        from {
          transform: translateX(-${width}px);
        }
        to {
          transform: translateX(0);
        }
      }`,
    style_sheet.length
  );
};

self.getVersionTable = function () {
  if (!self.data.filter.subpbu_id)
    return ;

  var ctx = self.ctx;
  var params = JSON.parse(JSON.stringify(self.data.filter));
  params.time_id_s = params.time_id_e;
  Promise.all([
    ctx.getDataSources({label: 'pnl_version_list_actual', params: params}),
    ctx.getDataSources({label: 'pnl_version_list_budget', params: params}),
  ]).then(function ([actual_version_table, budget_version_table]) {
    self.data.actual_version_table = actual_version_table;
    (actual_version_table.length > 0)?
      (self.data.filter.actual_version_id = actual_version_table[0].version_id): null;

    self.data.budget_version_table = budget_version_table;
    (budget_version_table.length > 0)?
      (self.data.filter.budget_version_id = budget_version_table[0].version_id): null;
  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.searchReport = function () {
  if (self.data.is_demo)
    return self.contentDataPreprocess(self.data.pbu_table_mock.data);

  //get data
  self.data.pnl_report_raw = null;
  var params = JSON.parse(JSON.stringify(self.data.filter));
  self.ctx.executeDataSources({label: 'pnl_report', body: params}).then(function (result) {
    self.contentDataPreprocess(result);
  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.itemNoteUpdater = function (line_item, service_label) {

  return function (param) {
    var body = Object.assign(param, self.data.criteria, {
      line_item: line_item
    });

    return self.ctx.executeDataSources({label: service_label, body: body}).then(function (response) {
      self.ctx.self.$message({
        message: 'Update success.',
        type: 'success'
      });
      return response;
    }).catch(function (e) {
      self.ctx.self.$message({
        message: 'Something wrong.',
        type: 'error'
      });
    });

  };
};

self.actionItemUpdater = function (line_item) {
  var service_label = 'pnl_action_item_save';
  return self.itemNoteUpdater(line_item, service_label);
};

self.deviationExplanationUpdater = function (line_item) {
  var service_label = 'pnl_deviation_explanation_save';
  return self.itemNoteUpdater(line_item, service_label);
};

self.exportToExcel = function () {
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.category img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px');//.html('Granularity: ' + self.data.fraction.label);
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.category td').css('background', '#F2F2F2');
  table_box.find('tbody tr.mark td').css('background', '#C9F0FF');
  table_box.find('tbody tr.emphasize td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td.alert').css('color', '#FF4F4F').css('textAlign', 'right');
  table_box.find('tbody td.success, tbody td.deuce').css('textAlign', 'right')
    .find('span:first-child').css('color', '#ABDB77');
  // replace newline to <br> in editor cell
  table_box.find('tbody td.editable').each(function (index, el) {
    var node = $(this).find('.el-popover__reference').eq(0);
    $(this).html(node.text().replace(/\n/, '<br>'));
  });

  table_box.find('span.percentage').each(function () {
    $(this).html($(this).html() + '%');
  });

  var all_ms = table_box.find('.margin-summary:not(:last-child)');
  all_ms.each(function () {
    $(this).find('td:first-child ~ td:not(:nth-last-child(-n+2))').each(function () {
      $(this).css('borderBottom', '#000000 1px solid')
        .css('borderLeft', '#FFFFFF 4px solid')
        .css('borderRight', '#FFFFFF 4px solid')
        .find('span:last-child')
        .html(function (index, oldHtml) {
          $(this).html(oldHtml + '%')
        });
    });
  });

  var ms_end = table_box.find('.margin-summary:last-child td:first-child ~ td:not(:nth-last-child(-n+2))')
    .each(function () {
      $(this).css('borderBottom', '#000000 1px double')
        .css('borderLeft', '#FFFFFF 4px double')
        .css('borderRight', '#FFFFFF 4px double')
        .find('span:last-child')
        .html(function (index, oldHtml) {
          $(this).html(oldHtml + '%')
        });
  });

  var exportData = [{
    sheetname: self.data.caption,
    data: table_box.html()
  }];
  self.ctx.file.exportDataByHtml(exportData, {
    filename: self.data.caption.replace(/([\s\.])+/g, '_'),
    style: style
  });
};

self.decimalPlaceChangeHandler = function (e) {
  self.data.report_items = self.ctx.lineItemProcessor(self.data.report_items);
};

self.dataComputed = {
  inMonthColspan: function () {
    var solid = 4;
    return self.data.is_show_products? (solid + self.data.products.length): solid;
  },
  cellAmount: function () {
    var solid = 11;
    return self.data.is_show_products? (solid + self.data.products.length): solid;
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  },
  numberScaleConfigs: function () {
    return {
      raw_data: {
        label: 'Raw Data',
        number_scale: 1,
      },
      thousand: {
        label: 'Thousand',
        number_scale: 1000,
      },
      million: {
        label: 'Million',
        number_scale: 1000000,
      }
    };
  }
};

self.dataWatch = {
  time_id: {
    handler: function (newVal, oldVal) {
      self.data.filter.time_id_s = newVal.substring(0, 4) + '01';
      self.data.filter.time_id_e = newVal;
      self.getVersionTable();
    }
  },
  "filter.pbu_id": {
    handler: function (newVal, oldVal) {
      self.data.sub_pbu_table_distinct = self.data.pbu_table_raw.filter(function (pbu) {
        return (pbu.pbu_id === newVal);
      });
      self.data.filter.subpbu_id = (self.data.sub_pbu_table_distinct.length > 0)?
        self.data.sub_pbu_table_distinct[0].subpbu_id: null;
    }
  },
  "filter.subpbu_id": {
    handler: function (newVal, oldVal) {
      self.getVersionTable();
    }
  },
  actual_version_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.actual_version_id = newVal.length > 0? newVal[0].version_id: null;
    }
  },
  budget_version_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.budget_version_id = newVal.length > 0? newVal[0].version_id: null;
    }
  },
  currency_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.currency_id = newVal.length > 0? newVal[0].currency_id: null;
    }
  },
  number_scale: {
    handler: function (newVal, oldVal) {
      self.ctx.lineItemProcessor(self.data.report_items);
    }
  }
  // decimal_place: {
  //   handler: function (newVal, oldVal) {
  //     self.ctx.lineItemProcessor(self.data.report_items);
  //   }
  // }
};

self.onInit = function () {
  self.data.init = true;
  var _ctx = self.ctx;

  // self.data.fraction = self.ctx.self.granularityConfigs.thousand;

  _ctx.toCurrency = function (value, fixed) {
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: fixed,
      maximumFractionDigits: (fixed === undefined)? 20: fixed
    });
  };

  _ctx.getGranularityConfig = function (item_type) {
    if (item_type.indexOf('S') >= 0) {
      if (item_type.indexOf('P') >= 0) { // Percentage
        return { decimal_place: 2, number_scale: 1 };
      } else if (item_type.indexOf('Q') >= 0) { // Quantity
        return { decimal_place: 0, number_scale: 1 };
      } else if (item_type.indexOf('N') >= 0) { // Number
        return { decimal_place: 2, number_scale: 1 };
      }
    } else {
      return {
        decimal_place: self.data.decimal_place,
        number_scale: _ctx.self.numberScaleConfigs[self.data.number_scale].number_scale
      };
    }
  };

  _ctx.getItemValueHandlers = function (item_type) {
    if (item_type.indexOf('P') >= 0) {  // 整個row都是百分比，例如margin
      return {
        percentageHandler: function (value, config = {}) {
          return _ctx.toCurrency(value * 100, config.decimal_place);
        },
        currencyHandler: function (value, config = {}) {
          return _ctx.toCurrency(value * 100, config.decimal_place);
        }
      };
    } else {
      return {
        percentageHandler: function (value, config = {}) {
          return _ctx.toCurrency(value * 100, 2);
        },
        currencyHandler: function (value, config = {}) {
          return _ctx.toCurrency((value / (config.number_scale || 1)), config.decimal_place);
        }
      };
    }
  };

  _ctx.lineItemProcessor = function (items) {
    var current_collapse_index = -1;
    return items.map(function (item) {
      if (item.item_type === 'C') {
        item.collapse_index = ++current_collapse_index;
        return item;
      }

      item.collapse_index = current_collapse_index;

      var config = _ctx.getGranularityConfig(item.item_type);
      var handlers = _ctx.getItemValueHandlers(item.item_type);

      item.ytm = _ctx._lineItemProcessor(item.ytm, handlers, config);
      item.in_month = _ctx._lineItemProcessor(item.in_month, handlers, config);

      return item;
    });
  };

  _ctx._lineItemProcessor = function (obj, handlers, config) {
    var percentageHandler = handlers.percentageHandler;
    var currencyHandler = handlers.currencyHandler;

    obj.percentage_fixed = percentageHandler(obj.percentage, config);
    obj.percentage_text = percentageHandler(obj.percentage);
    obj.actual_fixed = currencyHandler(obj.actual, config);
    obj.actual_text = currencyHandler(obj.actual);
    obj.budget_fixed = currencyHandler(obj.budget, config);
    obj.budget_text = currencyHandler(obj.budget);
    obj.abs_fixed = currencyHandler(obj.abs, config);
    obj.abs_text = currencyHandler(obj.abs);

    if (obj.products) {
      obj.products_fixed = _.map(obj.products, function (value) {
        return currencyHandler(value, config);
      });
      obj.products_text = _.map(obj.products, function (value) {
        return currencyHandler(value);
      });
    }
    return obj;
  };

  self.toggleYTM = _.throttle(self.toggleYTM, 500, {leading: true, trailing: false});

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  //init date-picker default value, which is previous month.
  var dd = new Date();
  _ctx.self.time_id = dd.getFullYear() + ('' + dd.getMonth()).padStart(2, '0');

  // use promise.all to get all necessary filter options concurrently.
  var foreign_data = JSON.parse(window.foreigndata);
  // console.log('before promise all.');
  Promise.all([
    _ctx.getDataSources({label: 'pnl_pbu_list', params: {user_id: foreign_data.usercode}}),
    _ctx.getDataSources({label: 'common_currency_list'})
  ]).then(function (results) {
    var pbu_table = results[0];
    var currency_table = results[1];
    var pbu_table_distinct = _.uniqBy(pbu_table, 'pbu_id');

    self.data.pbu_table_raw = pbu_table;
    self.data.pbu_table_distinct = pbu_table_distinct;
    self.data.filter.pbu_id = pbu_table_distinct.length > 0? pbu_table_distinct[0].pbu_id: null;
    self.data.currency_table = currency_table;

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.onDataUpdated = function () {};
self.onResize = function () {};
self.onDestroy = function () {};