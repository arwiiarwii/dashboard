
self.onInit = function () {
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};


// self.onInit = function () {
self.myInit = function () {
  var ctx = self.ctx;
  self.getIPAddr();
  self.data.clientW = document.body.clientWidth;
  self.data.weekValue = new Date();
  self.initData();
  self.initCarousel();
  self.getRecentWeek(new Date());
  self.test001();
};

// test by Ida
self.test001 = function () {
  // alert('test001');
  // Promise.resolve(
  //   ctx.getDataSources({label: 'GetGmoReportByWeek', params: ''})
  //   ).then(function (results) {
  //     console.log(results); // "Success"
  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
  //   });
  // });

  // Promise.all([
  //   ctx.getDataSources({label: 'GetGmoReportByWeek', params: ''})    
  // ]).then(function (results) {
  //   console.log("GetGmoReportByWeek : ", results);
  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
  //   });
  // });
};

// ooo
self.initData = function (params) {
  self.data.loginUserInfo = window.foreigndata ? JSON.parse(window.foreigndata) : {
    "usercode": "MW00086",
  };
  self.data.currentDate = new Date();
  self.data.canvas = {
    w: 60,
    h: 60
  };
  self.getGmoEmpTenantCode(self.data.loginUserInfo.usercode);
};

// modify by Ida
self.getGmoEmpBasicInfo = function () {
  // alert("GetGmoEmpBasicInfoV2");
  let url = self.data.baseurl + '/api/datacapture/v3/GetGmoEmpBasicInfoV2/kpi/' +
    `?params=tenant_code='${self.data.currentTenant.tenant_code}';day`;

  Promise.resolve(
    ctx.getDataSources({label: 'GetGmoEmpBasicInfoV2', params: ''}) ).then(function (ret) {

    // console.log(ret); // "Success"

    console.log("GetGmoEmpBasicInfoV2 : ", ret);
    let currentPerson = {};
    let sortP = ret[0].map(el => {
      if (el.emp_id === self.data.loginUserInfo.usercode) {
        self.data.loginPerson = el;
        if (el.main_reporter !== 'R') {
          currentPerson = el;
        };
      };
      return {
        ...el,
        emp_order: el.emp_order * 1,
        platform_order: el.platform_order * 1,
        subjectAndSite: el.subject ? el.subject + ' / ' + el.platform_name : el.platform_name
      };
    }).sort((a, b) => {
      return a.emp_order - b.emp_order;
    }) || [];

    self.data.personnelList = sortP;
    if (JSON.stringify(currentPerson) !== "{}") {
      self.data.currentPerson = currentPerson;
      self.data.showGoKeyPointWork = true;
    } else {
      firstPerson = sortP.filter(el => {
        return el.main_reporter !== "R";
      })[0];
      currentPerson = firstPerson;
      self.data.currentPerson = firstPerson;
      self.data.showGoKeyPointWork = false;
    };

    self.data.personnelListCopy = JSON.parse(JSON.stringify(sortP));
    self.data.mainReporterList = self.data.personnelListCopy.filter(el => {
      return el.main_reporter === "Y";
    });
    self.data.firstSpeaker = self.data.personnelListCopy.find(el => el.main_reporter === 'Y');
    let siteList = [],
      currentSite = {},
      siteNameList = [];
    self.data.personnelList.forEach((el, i) => {
      if (siteNameList.indexOf(el.platform_name) < 0) {
        let s = {
          ...el,
          key: siteList.length
        };
        siteNameList.push(el.platform_name);
        siteList.push(s);
        if (el.platform_name === currentPerson.platform_name) {
          currentSite = s;
        };
      };
    });
    let sortSiteList = siteList.sort((a, b) => {
      return a.platform_order - b.platform_order;
    });
    self.data.siteList = sortSiteList;
    self.data.currentSite = JSON.stringify(currentSite) !== "{}" ? currentSite : siteList[0];
    self.toggleSiteClick(self.data.currentSite, currentPerson, 1);


  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
  //   });
  });

};


self.getGmoReportObjective = function (loginPerson) {
  self.data.timerNumber = 90;
  clearInterval(self.data.timerInterval);
  let emp_id = loginPerson ? loginPerson.emp_id : self.data.currentPerson.emp_id,
    week_id = "" + self.data.timerObj.year + self.data.timerObj.week,
    week_id1 = ("" + self.data.timerObj.year + self.data.timerObj.week) * 1 - 1;
  // console.log("emp_id : ",);

  let url = self.data.baseurl + '/api/datacapture/v3/GetGmoReportObjectiveV2/kpi/' +
    `?params=emp_id='${emp_id}' and week_id='${week_id}' and week_id2='${week_id1}' and select_type='show' and tenant_code='${self.data.currentTenant.tenant_code}';day`;

    var params = {
      "emp_id": emp_id,
      "week_id": week_id,
      "week_id1": week_id1
    };

    ctx.getDataSources({label: 'GetGmoReportObjectiveV2', params: params}).then(function (ret) {
      console.log("getGmoReportObjective - ret" , ret); // "Success"

      let objectiveOrderList = [];
      let objectiveList = [];
      let objective_order = ret[0].map(el => {
        let objectiveKey = el.objective_order +
          el.objective_name;
        let color;
        if (el.month_id.slice(4) * 1 < self.data.timerObj.month && el.objective_progress * 1 < 100) {
          color = self.data.colorObj.OFFTRACK;
        } else if ((el.month_id.slice(4) * 1 > self.data.timerObj.month)) {
          color = self.data.colorObj.ONTRACK;
        } else {
          let req = self.monthWeekRelation(self.data.weekValue);
          let weekSchedule = 100 / req.monthWeekNum;
          color = el.objective_progress * 1 >= (req.realityMW - 1) * weekSchedule ? self.data.colorObj
            .ONTRACK :
            self.data.colorObj
            .OFFTRACK;
        };
        if (objectiveOrderList.indexOf(objectiveKey) < 0) {
          objectiveOrderList.push(objectiveKey);
          objectiveList.push({
            ...el,
            color
          });
        } else {
          objectiveList.forEach(item => {
            let objectiveKey1 = item.objective_order + item.objective_name;
            if (objectiveKey === objectiveKey1) {
              item.objective_progress = item.objective_progress * 1 + el.objective_progress * 1;
            };
          });
        };
        return el.objective_order;
      }).join(",");
      if (!objectiveList || objectiveList.length <= 0) {
        self.data.workContentList = [];
      } else {
        objectiveList.sort((a, b) => {
          return a.objective_code - b.objective_code;
        });
        self.getGmoReportByWeek(objective_order, emp_id, week_id, week_id1, objectiveList);
      };

  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
    // });
  });

};

// modify by Ida
self.getGmoReportByWeek = function (objective_order, emp_id, week_id, week_id1, workContentList) {
  // let url = self.data.baseurl + "/api/datacapture/v3/GetGmoReportByWeek/kpi/" +
  //   `?params=emp_id = '${emp_id}' AND objective_order IN(${objective_order}) AND week_id IN('${week_id}','${week_id1}') AND job_name IS NOT NULL AND job_name <> ''`;

  console.log("\t objective_order : ", objective_order, " \n\t emp_id : ", emp_id, " \n\t week_id : ", week_id, " \n\t week_id1 : ", week_id1, " \n\t workContentList : ", workContentList);

  ctx.getDataSources({label: 'GetGmoReportByWeek', params: ""}).then(function (ret) {

  // Promise.resolve(
  //   ctx.getDataSources({label: 'GetGmoReportByWeek', params: ''})
    // ).then(function (ret) {
      console.log("GetGmoReportByWeek - ret : ", ret[0]); // "Success"
      console.log("GetGmoReportByWeek - workContentList :  ", workContentList ); // "Success"

      
      let rq = ret[0] || [];

      // el : from workContentList
      self.data.workContentList = workContentList.map(el => {
        let thisWeekList = [],
          PervWeekList = [];

        console.log("rq :  ", rq ); // "Success"
        rq.forEach(item => {
          console.log("item : ", item);
          console.log("el.objective_name : ", el.objective_name);
          console.log("item.objective_name : ", item.objective_name);
          console.log("el.objective_order : ", el.objective_order);
          console.log("item.objective_order : ", item.objective_order);
          console.log("item.week_id : ", item.week_id);
          console.log("week_id : ", week_id);

          // el : 
          // item : 
          if (el.objective_name === item.objective_name && el.objective_order === item.objective_order) {
            if (item.week_id * 1 === week_id * 1) {
              thisWeekList.push(item);
            } else {
              PervWeekList.push(item);
            };
          };
        });
        el.thisWeekList = thisWeekList;
        el.PervWeekList = PervWeekList;
        el.objective_progress = el.objective_progress * 1;
        console.log("el : ", el);
        return el;
      });
      let t = setTimeout(() => {
        let canvasList = [...document.querySelectorAll("canvas[id^='canvas_']")];
        canvasList.forEach(el => {
          self.createPie(el.id);
        });
      }, 10);
      t = null;
      self.timerNumberCountDown();


  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
  //   });
  });
};

self.toggleSiteClick = function (el, person, type) {
  self.data.currentSite = el;
  let newPersonnelList = self.data.personnelListCopy.filter(item => {
    return el.platform_name === item.platform_name && item.main_reporter !== 'R';
  });
  self.data.personnelList = newPersonnelList;
  self.data.carouselP = 0;
  self.initCarousel();
  let p = person && JSON.stringify(person) !== "{}" ? person : newPersonnelList[0];
  self.togglePersonClick(p, type);
};
self.goToWorkBodyTop = function () {
  document.getElementById("workBody").scrollTop = 0;
};
self.togglePersonClick = function (el, type) {
  self.data.currentTenant.tenant_code && self.shouldRecordConference(self.data.currentPerson);
  self.data.showSinglePerson = type ? false : true;
  self.goToWorkBodyTop();
  let systemAccount = self.data.systemAccount.indexOf(self.data.loginUserInfo.usercode);
  if (systemAccount === -1 && self.data.loginPerson.main_reporter !== 'Y' && self.data.loginPerson
    .main_reporter !== 'R' && self.data.loginPerson
    .main_reporter !== 'NR') {
    self.getGmoReportObjective(self.data.loginPerson);
    return;
  };
  self.data.currentPerson = el;
  if (el.emp_id === self.data.mainReporterList[self.data.mainReporterList.length - 1].emp_id || el.main_reporter !==
    'Y') {
    self.data.currentPersonNext = {};
  } else {
    for (let i = 0; i < self.data.mainReporterList.length; i++) {
      if (self.data.mainReporterList[i].emp_id === el.emp_id &&
        self.data.mainReporterList[i + 1] && self.data.mainReporterList[i + 1].main_reporter === 'Y') {
        self.data.currentPersonNext = self.data.mainReporterList[i + 1];
      };
    };
  };

  let t = setTimeout(() => {
    self.getGmoReportObjective();
  }, 10);
  t = null;
};
self.nextPersonClick = function () {
  self.data.siteList.forEach(el => {
    if (el.platform_name === self.data.currentPersonNext.platform_name) {
      self.data.currentSite = el;
    };
  });
  self.togglePersonClick(self.data.currentPersonNext, 1);
};
self.toggleDateBtnClick = function (item) {
  self.data.currentDateUnit = item.key;
};
self.carouselToggle = function (type) {
  if (type === 0) {
    self.data.carouselP = self.data.carouselP + self.data.carouselW;
  } else {
    self.data.carouselP = self.data.carouselP - self.data.carouselW;
  };
  self.initCarousel();
};
self.weekValueChange = function (val) {
  self.monthWeekRelation(self.data.weekValue);
  self.getRecentWeek(val);
  let t = setTimeout(() => {
    self.getGmoReportObjective();
  }, 10);
  t = null;
};

self.initCarousel = function () {
  let clientW = document.body.clientWidth;
  let maxDisplayW = self.data.personnelList.length * self.data.carouselW;
  self.data.showRArrow = clientW + Math.abs(self.data.carouselP) <= maxDisplayW;
};

self.createPie = function (elId) {
  let pList = elId.split("_"),
    val = pList[3] * 1 || 0,
    color = pList[4],
    canvas = document.getElementById(elId),
    ctx = canvas.getContext("2d"),
    centerX = canvas.width / 2,
    centerY = canvas.height / 2,
    rad = Math.PI * 2 / 100,
    radius = (canvas.width / 2) - 10,
    speed = 1,
    x = val >= 100 ? 17.5 : val < 10 ? 9 : 12;

  function blueCircle(n) {
    ctx.save();
    ctx.strokeStyle = color;
    ctx.lineWidth = 5;
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, -Math.PI / 2, -Math.PI / 2 + n * rad,
      false);
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
  };

  function whiteCircle() {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.strokeStyle = "#D8D8D8";
    ctx.arc(centerX, centerY, radius, 0, Math.PI * 2, false);
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
  };

  function text(n) {
    ctx.save();
    ctx.fillStyle = color;
    ctx.font = "bold 14px Arial";
    ctx.fillText(n.toFixed(0) + "%", centerX - x, centerY + 5);
    ctx.restore();
  };

  function drawFrame(now) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    whiteCircle();
    text(now);
    blueCircle(now);
  };

  function loadCanvas(now) {
    let timer = setInterval(function () {
      if (speed > now) {
        drawFrame(now);
        clearInterval(timer);
        timer = null;
      } else {
        drawFrame(speed);
        speed += 6;
      };
    }, 20);
  };
  loadCanvas(val);
};
self.goKeyPointWork = function () {
  let url =
    'http://tm.iisd.efoxconn.com:803/dashboard.html#/iframeDashboards/detail/3a844154-e465-4a3e-88ac-e3a4cea50154/pc/1.0.0';
  window.location.href = url;
};

self.getWeekStartAndWeekEnd = function (d) {
  let year = d.getFullYear();
  let month = parseInt(d.getMonth()) + 1;
  let weekStartFullDate = self.getWeekStart(d).fullDate;
  let weekStart = self.getWeekStart(d).portionDate;
  let newD = new Date(weekStartFullDate);
  newD.setDate(newD.getDate() + 6);
  let monthSunday = parseInt(newD.getMonth()) + 1;
  let rqD = new Date(newD.getTime());
  let weekEnd = parseInt(rqD.getMonth()) + 1 + "/" + rqD.getDate();
  return {
    weekStart,
    weekEnd,
    week: self.getWeek(d)
  };
};

self.getWeekStart = function (date) {
  let week = date.getDay();
  let minus = week ? week - 1 : 6;
  date.setDate(date.getDate() - minus);
  let y = date.getFullYear();
  let m = date.getMonth() + 1;
  let d = date.getDate();
  let fullDate = y + "-" + m + "-" + d;
  let rqD = new Date(date.getTime());
  let portionDate = (rqD.getMonth() + 1) + "/" + rqD.getDate();
  return {
    fullDate,
    portionDate
  };
};

self.getWeek = function (date) {
  let day11 = Date.parse(date);
  day11 = new Date(day11);
  day11.setMonth(0);
  day11.setDate(1);
  day11.setHours(0);
  day11.setMinutes(0);
  day11.setSeconds(0);

  let day11mill = day11.getTime();
  let ori_day = day11.getDay();
  let fill1 = 0;
  if (ori_day !== 0) {
    fill1 = ori_day * 60 * 60 * 24 * 1000;
  };

  let now = Date.parse(date);
  now = new Date(now);
  now.setHours(0);
  now.setMinutes(0);
  now.setSeconds(0);
  let nowmill = now.getTime();
  let now_day = now.getDay();
  let fill2 = 0;
  if (now_day !== 0) {
    fill2 = (7 - now_day) * 60 * 60 * 24 * 1000;
  };

  let cha2 = (nowmill - day11mill + fill1 + fill2) / (60 * 60 * 24 * 1000);
  let week = Math.ceil(cha2 / 7);
  if (week < 10) {
    week = "0" + week;
  };
  let year = now.getFullYear().toString();
  year = year.substring(2);
  return week;
};

self.getRecentWeek = function (date) {
  let currentT = date.getTime();
  let dt = 86400000;
  let getRecentWeekList = [
    currentT - dt * 7 * 3,
    currentT - dt * 7 * 2,
    currentT - dt * 7 * 1,
    currentT,
    currentT + dt * 7 * 1,
    currentT + dt * 7 * 2
  ];
  let req = getRecentWeekList.map(el => {
    let r = self.getWeekStartAndWeekEnd(new Date(el));
    let s = {
      type: 'Week',
      value: r.week,
      timeQuantum: `(${r.weekStart} ~ ${r.weekEnd})`
    };
    return s;
  });
  self.data.timerList = req;
  self.data.timerObj = {
    year: date.getFullYear(),
    week: self.data.timerList[3].value,
    month: date.getMonth() + 1
  };
};

self.dateFormat = function (fmt, date) {
  let ret;
  const opt = {
    "Y+": date.getFullYear().toString(),
    "m+": (date.getMonth() + 1).toString(),
    "d+": date.getDate().toString(),
    "H+": date.getHours().toString(),
    "M+": date.getMinutes().toString(),
    "S+": date.getSeconds().toString()
  };
  for (let k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
    };
  };
  return fmt;
};
self.setWeekInputFocus = function (params) {
  document.getElementById("week_value_input").focus();
};

self.monthWeekRelation = function (params) {
  function getDays(year, month) {
    const date = new Date(year, month, 0);
    return date.getDate();
  };

  function getMonthStartAndEnd(d) {
    let firstDate = new Date(d),
      startDate = firstDate.getFullYear() + "-" + ((firstDate.getMonth() + 1) < 10 ? "0" : "") + (firstDate
        .getMonth() +
        1) + "-" + "01",
      date = new Date(d),
      currentMonth = date.getMonth(),
      nextMonth = ++currentMonth,
      nextMonthFirstDay = new
    Date(date.getFullYear(), nextMonth, 1), oneDay = 1000 * 60 * 60 * 24, lastDate = new Date(nextMonthFirstDay -
        oneDay),
      endDate = lastDate.getFullYear() + "-" + ((lastDate.getMonth() + 1) < 10 ? "0" : "") + (lastDate.getMonth() +
        1) +
      "-" + (lastDate.getDate() < 10 ? "0" : "") + lastDate.getDate();
    return [startDate, endDate];
  };

  function getMonthWeek(a, b, c) {
    let date = new Date(a, parseInt(b) - 1, c),
      w = date.getDay(),
      d = date.getDate();
    if (w == 0) {
      w = 7;
    };
    let config = {
      getMonth: date.getMonth() + 1,
      getYear: date.getFullYear(),
      getWeek: Math.ceil((d + 6 - w) / 7)
    };
    return config;
  };
  let copyDate = params ? params : new Date(),
    Y = copyDate.getFullYear(),
    M = copyDate.getMonth() + 1,
    D = copyDate.getDate(),
    monthNum = getDays(Y, M),
    MStartAndEnd = getMonthStartAndEnd(copyDate),
    startGetDays = new Date(MStartAndEnd[0]).getDay() === 0 ? 7 : new Date(MStartAndEnd[0]).getDay(),
    endGetDays = new Date(MStartAndEnd[1]).getDay() === 0 ? 7 : new Date(MStartAndEnd[1]).getDay(),
    startBelongTo = startGetDays < 5 ? 1 : 0,
    endBelongTo = endGetDays > 3 ? 1 : 0,
    wholeW = (monthNum - (7 - startGetDays + 1) - endGetDays) / 7,
    monthWeekNum = wholeW + startBelongTo + endBelongTo,
    MW = getMonthWeek(Y, M, D).getWeek,
    realityMW = startBelongTo === 0 ? MW - 1 : MW;
  return {
    Y,
    M,
    D,
    monthNum,
    MStartAndEnd,
    startGetDays,
    endGetDays,
    startBelongTo,
    endBelongTo,
    wholeW,
    monthWeekNum,
    MW,
    realityMW
  };
};

self.transitionContent = function (str) {
  // console.log("transitionContent - str : ", str);
  let reg = /([\s]+)([0-9]+)(\）)/g;
  let reg1 = /([\s]+)([0-9]{1,2}\.[0-9]{1,2}\s)/g;
  let newHtml = str.replace(reg, "$1" + "<span class='jobName-No'>" + "$2" + "</span>");
  // console.log("newHtml : ", newHtml);
  newHtml = newHtml.replace(reg1, "$1" + "<span class='jobName-subNo'>" + "$2" + "</span>");
  if (newHtml.slice(0, 2) === '1）') {
    // newHtml = "<span class='jobName-No'>1</span>" + newHtml.slice(2);
    newHtml = "<span class='jobName-No'>123</span>" + newHtml.slice(2);
  };
  return newHtml;
};
self.timerNumberCountDown = function () {
  clearInterval(self.data.timerInterval);
  self.data.timerInterval = setInterval(() => {
    self.data.timerNumber = self.data.timerNumber - 1;
  }, 1000);
};
self.workContentCilck = function (params) {
  self.data.currentWorkContent = params;
};


// modify by Ida
self.postGmoLogService = function (timeDifference, ctimer, person) {
  let url = self.data.baseurl + "/api/datacapture/v1/GmoLogService/service/",
    data = [{
      "entityList": [{
        duration: timeDifference,
        emp_id: person.emp_id,
        week_id: "" + self.data.timerObj.year + self.data.timerObj.week,
        report_start: ctimer.getTime(),
        report_end: ctimer.getTime() + timeDifference * 1000,
        tenant_code: self.data.currentTenant.tenant_code,
        ip_address: self.data.IPAddr
      }],
      "opType": "add"
    }];


  self.ctx.executeDataSources({label: 'GmoLogService', body: {}}).then(function (ret) {
    console.log("GmoLogService : ", ret);

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });


};


self.shouldRecordConference = function (person) {
  if (self.data.loginUserInfo.usercode === "GMO") {
    let ctimer = new Date();

    function currentGetDay(pDate) {
      return pDate ? pDate.getDay() : new Date(ctimer).getDay();
    };

    function isDuringDate(beginTime, endTime, pDate) {
      let strb = beginTime.split(":");
      if (strb.length != 2) {
        return false;
      };
      let stre = endTime.split(":");
      if (stre.length != 2) {
        return false;
      };
      let b = new Date(ctimer);
      let e = new Date(ctimer);
      let n = new Date(ctimer);
      b.setHours(strb[0]);
      b.setMinutes(strb[1]);
      e.setHours(stre[0]);
      e.setMinutes(stre[1]);
      if (n.getTime() - b.getTime() > 0 && n.getTime() - e.getTime() < 0) {
        return true;
      } else {
        return false;
      };
    };
    let Day = currentGetDay();
    let bl = isDuringDate("9:00", "12:30");
    let timeDifference = 90 - self.data.timerNumber;
    console.log(Day, bl, timeDifference);
    if (Day > 0 && bl && timeDifference > 20) {
      console.log("达到记录标准");
      self.postGmoLogService(timeDifference, ctimer, person);
      return timeDifference;
    } else {
      console.log("未达到记录标准");
      return false;
    };
  };
};

self.resetConferenceTimer = function () {
  self.data.timerNumber = 90;
  self.timerNumberCountDown();
};



// modify by Ida
self.getGmoEmpTenantCode = function (emp_id) {
  console.log("getGmoEmpTenantCode : " + emp_id);

  Promise.resolve(
    ctx.getDataSources({label: 'GetGmoEmpTenantCode', params: ''})
    ).then(function (ret) {
      console.log(ret); // "Success"
      // console.log("ret___: " , ret[0][0]);
      console.log("ret___: " , ret[0]);
      let no = ret[0].length;
      self.data.tenantList = ret[0].filter(el => {
        if (no <= 1) {
          return el;
        } else {
          return el.tenant_code === 'GMO';
        };
      });
      self.data.currentTenant = self.data.tenantList[0];
      console.log("self.data.tenantList : ", self.data.tenantList);
      self.data.tenantSelectValue = self.data.tenantList[0].tenant_code;
      if (self.data.tenantList.length === 1) {
        self.getGmoEmpBasicInfo();
      } else {
        self.data.dialogVisible = true;
      };

  // }).catch(function (err) {
  //   self.ctx.self.$message({
  //     message: 'Something wrong.',
  //     type: 'error'
  //   });
  });
};


self.confirmTenant = function () {
  self.data.dialogVisible = false;
  self.getGmoEmpBasicInfo();
};


self.currentTenantValueChange = function (params) {
  self.data.currentTenant = self.data.tenantList.find(el => {
    console.log("[currentTenantValueChange] el.tenant_code : ", el.tenant_code);
    return el.tenant_code === params;
  });
};



// xxx
self.getIPAddr = function () {
  let url = `http://10.124.160.3:9106/api/insight/v1/ip`;
  let token = sessionStorage.getItem('token');
  let Authorization = `Bearer ${token}`;
  self.ctx.$.ajax({
    url: url,
    type: "get",
    dataType: "json",
    async: true,
    xhrFields: {
      withCredentials: true
    },
    beforeSend: function (request) {
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.setRequestHeader("X-Authorization", Authorization);
    },
    success: (ret) => {
      console.log("ip - s : ", ret);
      self.data.IPAddr = ret;
    },
    error: (error) => {
      console.log("ip - e : ", ret);
      console.log("获取数据失败", error);
    }
  });
};


