
// ooo-1
self.onInit = function () {
  // alert("onInit");
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};

// ooo-2
self.myInit = function () {
  var ctx = self.ctx;
  let nextWeek = new Date(new Date().getTime() + 604800000);
  self.data.weekValue = nextWeek;
  self.initData();
  self.initCarousel();
  self.getRecentWeek(nextWeek);
  self.loadJS("../static/js/html2canvas.js", function () {});
  self.loadJS("../static/js/jspdf.debug.js", function () {});
  // console.log(self);
};


// ooo-3
/* add for keyPointWork 20210625*/
self.initData = function (params) {
  self.data.loginUserInfo = window.foreigndata ? JSON.parse(window.foreigndata) : {
    "usercode": "GMO"
  };
  self.data.currentDate = new Date();
  self.getGmoEmpTenantCode(self.data.loginUserInfo.usercode);
};


// ooo-4
self.getGmoEmpTenantCode = function (emp_id) {

  // let url = self.data.baseurl + "/api/datacapture/v3/GetGmoEmpTenantCode/kpi/" +
  //   `?params=emp_id='${emp_id}';day`;
  ctx.getDataSources({label: 'GetGmoEmpTenantCode', params: ""}).then(function (ret) {
    let no = ret[0].length;
    self.data.tenantList = ret[0].filter(el => {
      if (no <= 1) {
        return el;
      } else {
        return el.tenant_code === 'GMO';
      };
    });
    self.data.currentTenant = self.data.tenantList[0];
    self.data.tenantSelectValue = self.data.tenantList[0].tenant_code;
    if (self.data.tenantList.length === 1) {
      self.getGmoEmpBasicInfo();
    } else {
      self.data.dialogVisible = true;
    };

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};


// ooo-5
self.getGmoEmpBasicInfo = function () {
  // let url = self.data.baseurl + '/api/datacapture/v3/GetGmoEmpBasicInfoV2/kpi/' +
  //   `?params=tenant_code='${self.data.currentTenant.tenant_code}';day`;
  console.log("params : ", `tenant_code='${self.data.currentTenant.tenant_code}';day`);
  
  ctx.getDataSources({label: 'GetGmoEmpBasicInfoV2', params: `tenant_code='${self.data.currentTenant.tenant_code}';day`}).then(function (ret) {
    console.log("GetGmoEmpBasicInfoV2 : ", GetGmoEmpBasicInfoV2);
    let currentPerson = {};
    let personnelList = ret[0].map(el => {
      if (el.emp_id === self.data.loginUserInfo.usercode) {
        self.data.currentPerson = el;
        self.data.personnelList = [el];
        currentPerson = el;
      };
      return {
        ...el,
        emp_order: el.emp_order * 1,
        platform_order: el.platform_order * 1,
        subjectAndSite: el.subject ? el.subject + ' / ' + el.platform_name : el.platform_name
      };
    }).sort((a, b) => {
      return a.emp_order - b.emp_order;
    }) || [];

    self.data.personnelListCopy = JSON.parse(JSON.stringify(personnelList));
    let siteList = [];
    personnelList.forEach((el, i) => {
      if (el.platform_name == currentPerson.platform_name) {
        self.data.currentSite = {
          ...el,
          key: i
        };
      };
      if (siteList.indexOf(el.platform_name) < 0) {
        siteList.push({
          ...el,
          key: i
        });
      };
    });
    self.data.siteList = siteList.sort((a, b) => {
      return a.platform_order - b.platform_order;
    });

    self.data.hiddenBtn = (JSON.stringify(currentPerson) === '{}' || currentPerson
        .main_reporter === 'R') ?
      true : false;
    self.getGmoReportObjective();


  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};

// ooo-6
self.getGmoReportObjective = function () {
  self.data.workContentList = [];
  // let emp_id = self.data.currentPerson.emp_id,
  let emp_id = "GMO",
    week_id = "" + self.data.timerObj.year + self.data.timerObj.week,
    week_id1 = ("" + self.data.timerObj.year + self.data.timerObj.week) * 1 - 1;
  // let url = self.data.baseurl + '/api/datacapture/v3/GetGmoReportObjectiveV2/kpi/' +
  //   `?params=emp_id='${emp_id}' and week_id='${week_id}' and week_id2='${week_id1}'and tenant_code='${self.data.currentTenant.tenant_code}' and select_type='maintenance';day`;
  if (!emp_id) {
    return;
  };

  ctx.getDataSources({label: 'GetGmoReportObjectiveV2', params: ''}).then(function (ret) {
    console.log("GetGmoReportObjectiveV2 : ", ret);
    let objectiveOrderList = [];
    let objectiveList = [];
    let objective_order = ret[0].map(el => {
      let objectiveKey = el.objective_order + el.objective_name;
      let color;
      if (el.month_id.slice(4) * 1 < self.data.timerObj.month && el.objective_progress * 1 <
        100) {
        color = self.data.colorObj.OFFTRACK;
      } else if ((el.month_id.slice(4) * 1 > self.data.timerObj.month)) {
        color = self.data.colorObj.ONTRACK;
      } else {
        let req = self.monthWeekRelation(self.data.weekValue);
        let weekSchedule = 100 / req.monthWeekNum;
        color = el.objective_progress * 1 + el.current_progress * 1 >= (req.realityMW - 1) *
          weekSchedule ?
          self
          .data.colorObj
          .ONTRACK :
          self.data.colorObj
          .OFFTRACK;
      };
      if (objectiveOrderList.indexOf(objectiveKey) < 0) {
        objectiveOrderList.push(objectiveKey);
        objectiveList.push({
          ...el,
          thisSchedule: el.current_progress,
          color
        });
      } else {
        objectiveList.forEach(item => {
          let objectiveKey1 = item.objective_order + item.objective_name;
          if (objectiveKey === objectiveKey1) {
            item.objective_progress = item.objective_progress * 1 + el
              .objective_progress * 1;
          };
        });
      };
      return el.objective_order;
    }).join(",");

    if (!objectiveList || objectiveList.length <= 0) {
      self.data.workContentList = [];
      self.data.objectiveList = [];
    } else {
      objectiveList.sort((a, b) => {
        return a.objective_code - b.objective_code;
      });
      self.data.objectiveList = JSON.parse(JSON.stringify(objectiveList));
      self.getGmoReportByWeek(objective_order, emp_id, week_id, week_id1, objectiveList);
    };

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};

// ooo-7
self.getGmoReportByWeek = function (objective_order, emp_id, week_id, week_id1, objectiveList) {
  // let url = self.data.baseurl + "/api/datacapture/v3/GetGmoReportByWeek/kpi/" +
  //   `?params=emp_id = '${emp_id}' AND objective_order IN(${objective_order}) AND week_id <= '${week_id}' AND job_name IS NOT NULL AND job_name <> ''`;

  ctx.getDataSources({label: 'GetGmoReportByWeek', params: ''}).then(function (ret) {
    

    let rq = ret[0] || [];
    let workContentList = objectiveList.map(el => {
      let obj = {
        ...el,
        objective_progress: el.objective_progress * 1,
        showView: el.objective_progress * 1 + el.current_progress * 1 >= 100,
        thisWeekList: [],
        pervWeekList: []
      };

      rq.forEach(item => {
        if (el.objective_name === item.objective_name && el.objective_order === item
          .objective_order) {
          if (item.week_id * 1 === week_id * 1) {
            obj.thisWeekList.push(item);
          } else if (item.week_id * 1 === week_id - 1) {
            obj.pervWeekList.push(item);
          };
        };
      });
      if (obj.thisWeekList.length <= 0) {
        let weekId = "" + self.data.timerObj.year + self.data.timerObj.week;
        let newItem = self.createJobItem(weekId);
        obj.thisWeekList.push(newItem);
      };
      if (obj.pervWeekList.length <= 0) {
        let weekId = "" + self.data.timerObj.year + self.data.timerObj.week - 1;
        let newItem = self.createJobItem(weekId);
        obj.pervWeekList.push(newItem);
      };
      self.data.workContentList.push(obj);
      return el;
    });
    let tableData = self.data.workContentList.map(el => {
      let obj = {
        objective_name: el.objective_name,
        objective_progressob: el.objective_progress * 1 + el.current_progress * 1 + "%",
        pervWeekJobName: self.transformJobName(el.pervWeekList[0].job_name),
        job_completion: el.pervWeekList[0].job_completion === 'Y' ? "是" : "否",
        thisWeekJobName: self.transformJobName(el.thisWeekList[0].job_name),
        color: el.color
      };
      return obj;
    });
    let tableDataXLS = self.data.workContentList.map(el => {
      let obj = {
        objective_name: el.objective_name,
        objective_progressob: el.objective_progress * 1 + el.current_progress * 1 + "%",
        pervWeekJobName: self.transitionContent(self.transformJobName(el.pervWeekList[0]
            .job_name),
          self.transformJobName(el.thisWeekList[0].job_name)),
        job_completion: el.pervWeekList[0].job_completion,
        thisWeekJobName: self.transitionContent(self.transformJobName(el.thisWeekList[0]
            .job_name),
          self.transformJobName(el.pervWeekList[0].job_name)),
        color: el.color
      };
      return obj;
    });
    self.data.tableData = tableData;
    self.data.tableDataXLS = tableDataXLS;
    self.GetGmoExportByWeek();
    self.loadAutoTextarea();

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};


// [xxx]-8
self.GetGmoExportByWeek = function (that) {
  let weekList = [1, 2, 3, 4, 5].map((el, i) => {
    let tObj = self.getTimerObj(new Date(self.data.weekValue.getTime() - 604800000 * i));
    return "" + tObj.year + tObj.week;
  });
  let url = self.data.baseurl + "/api/datacapture/v3/GetGmoExportByWeek/kpi/" +
    `?params=emp_id='${self.data.loginUserInfo.usercode}' and week_id1='${weekList[0]}' and week_id2='${weekList[1]}' and week_id3='${weekList[2]}' and week_id4='${weekList[3]}' and week_id5='${weekList[4]}' and select_type='maintenance' and tenant_code='${self.data.currentTenant.tenant_code}';day`;

  ctx.getDataSources({label: 'GetGmoExportByWeek', params: ''}).then(function (ret) {



    let data = ret[0],
      exportXLSList = [],
      sheet1 = [],
      sheet2 = [],
      sheet3 = [],
      sheet4 = [];
    for (let i = 0; i < data.length; i++) {
      switch (data[i].sheet_name) {
        case "sheet1":
          sheet1.push(data[i]);
          break;
        case "sheet2":
          sheet2.push(data[i]);
          break;
        case "sheet3":
          sheet3.push(data[i]);
          break;
        case "sheet4":
          sheet4.push(data[i]);
          break;

        default:
          break;
      };
    };

    function formatSheet(list, weekDifference) {
      list.sort((a, b) => a.objective_code - b.objective_code);
      let res = classify(list).map((el, i) => {
        let pervWeekJobName = "";
        let thisWeekJobName = "";
        let pervWeekObj = {};

        if (el.data[1]) {
          if (el.data[0].job_week_id * 1 > el.data[1].job_week_id * 1) {
            thisWeekJobName = el.data[0].job_name;
            pervWeekJobName = el.data[1].job_name;
            pervWeekObj = el.data[1];
          } else {
            thisWeekJobName = el.data[1].job_name;
            pervWeekJobName = el.data[0].job_name;
            pervWeekObj = el.data[0];
          };
        } else {
          let timerObj = self.getTimerObj(self.data.weekValue - ((weekDifference - 1) * 604800000));
          let weekId = "" + timerObj.year + timerObj.week;
          if (el.data[0].job_week_id === weekId) {
            thisWeekJobName = el.data[0].job_name;
          } else {
            pervWeekJobName = el.data[0].job_name;
          };
          pervWeekObj = el.data[0];
        };
        let obj = {
          objective_name: el.data[0].objective_name,
          objective_progressob: el.data[0].objective_progress * 1 + el.data[0].current_progress * 1 +
            "%",
          pervWeekJobName: self.transitionContent(self.transformJobName(pervWeekJobName),
            self.transformJobName(thisWeekJobName)),
          job_completion: el.data[1] ? pervWeekObj.job_completion : 'N',
          thisWeekJobName: self.transitionContent(self.transformJobName(thisWeekJobName),
            self.transformJobName(pervWeekJobName)),
          color: getTextColor(pervWeekObj, weekDifference)
        };
        return obj;
      });

      function getTextColor(el, weekDifference) {
        let color;
        if ((el.objective_progress * 1 + el.current_progress * 1) >= 100) {
          color = self.data.colorObj.ONTRACK;
        } else {
          let req = self.monthWeekRelation(self.data.weekValue - ((weekDifference - 1) * 604800000));
          let year = el.month_id.slice(0, 4) * 1;
          let month = el.month_id.slice(4) * 1;
          let timeStamp = new Date(year, month, 0).getTime();
          let contrastWeek = self.data.weekValue - ((weekDifference - 1) * 604800000);
          if (timeStamp < contrastWeek) {
            color = self.data.colorObj.OFFTRACK;
          } else {
            let weekSchedule = 100 / req.monthWeekNum;
            color = el.objective_progress * 1 + el.current_progress * 1 >= (req.realityMW - 1) *
              weekSchedule ?
              self.data.colorObj.ONTRACK :
              self.data.colorObj.OFFTRACK;
          };
        };
        el.color = color;
        return color;
      };

      function classify(arr) {
        let map = {};
        let myArr = [];
        for (let i = 0; i < arr.length; i++) {
          if (!map[arr[i].objective_order]) {
            myArr.push({
              objective_order: arr[i].objective_order,
              data: [arr[i]]
            });
            map[arr[i].objective_order] = arr[i];
          } else {
            for (let j = 0; j < myArr.length; j++) {
              if (arr[i].objective_order === myArr[j].objective_order) {
                myArr[j].data.push(arr[i]);
                break;
              };
            };
          };
        };
        return myArr;
      };
      exportXLSList.push(res);
    };
    formatSheet(sheet1, 1);
    formatSheet(sheet2, 2);
    formatSheet(sheet3, 3);
    formatSheet(sheet4, 4);
    self.data.exportXLSList = exportXLSList;




  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });


};





// ================================================

// self.document = document;
// self.URL = window.URL;

self.loadJS = function (url, callback) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  if (typeof (callback) != "undefined") {
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (script.readyState == "loaded" || script.readyState == "complete") {
          script.onreadystatechange = null;
          callback();
        };
      };
    } else {
      script.onload = function () {
        callback();
      };
    };
  };
  script.src = url;
  document.body.appendChild(script);
};






// ooo-?
self.GmoUpdateHide = function (type, obj, that) {
  let emp_id = self.data.currentPerson.emp_id,
    hide_week = obj.hide_week,
    id = obj.id;
  let url = self.data.baseurl + "/api/datacapture/v3/GmoUpdateHide/kpi/" +
    `?params=emp_id='${emp_id}' and hide_week='${hide_week}' and id='${id}';day`;
    params

  ctx.getDataSources({label: 'GmoUpdateHide', params: ''}).then(function (ret) {
    // to do 
  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};


// ooo-?
self.saveGmoWeekService = function (that) {
  self.postGmoOrderService(that);
  let bl = false,
    bl1 = false,
    url = self.data.baseurl + "/api/datacapture/v1/GmoWeekService/service/",
    data = [],
    addObj = {
      "entityList": [],
      "opType": "add"
    },
    updateObj = {
      "entityList": [],
      "opType": "update"
    },
    deleteObj = {
      "entityList": self.data.deleteList.map(el => {
        return {
          id: el
        };
      }),
      "opType": "delete"
    };

  self.data.workContentList.forEach(item => {
    if (item.isAdd === true && self.transformJobName(item.thisWeekList[0].job_name) === "" &&
      self.transformJobName(item.pervWeekList[0].job_name) === "") {
      bl1 = true;
    };
    if (!item.objective_name) {
      bl = true;
    };
    self.data.objectiveList.forEach(el => {
      if (item.isChange && el.id === item.id && el.objective_name === item.objective_name && item
        .thisSchedule * 1 > 0
      ) {
        let uuid = self.generateUUID().replace(/-/g, "");
        addObj.entityList.push({
          tenant_code: self.data.currentTenant.tenant_code,
          emp_id: self.data.loginUserInfo.usercode,
          id: uuid,
          objective_name: item.objective_name,
          objective_order: item.objective_order + "",
          objective_progress: item.thisSchedule,
          month_id: "" + self.data.timerObj.year + self.data.timerObj.month,
          week_id: ((self.data.timerObj.year + "" + self.data.timerObj.week * 1) - 1) + ""
        });
      };
    });
    if (item.isAdd) {
      addObj.entityList.push({
        tenant_code: self.data.currentTenant.tenant_code,
        emp_id: self.data.loginUserInfo.usercode,
        id: item.id,
        objective_name: item.objective_name,
        objective_order: item.objective_order + "",
        objective_progress: item.thisSchedule,
        objective_code: item.objective_code,
        month_id: "" + self.data.timerObj.year + self.data.timerObj.month,
        week_id: "0"
      });
      addObj.entityList.push({
        tenant_code: self.data.currentTenant.tenant_code,
        emp_id: self.data.loginUserInfo.usercode,
        id: self.generateUUID().replace(/-/g, ""),
        objective_name: item.objective_name,
        objective_order: item.objective_order + "",
        objective_progress: item.thisSchedule,
        objective_code: item.objective_code,
        month_id: "" + self.data.timerObj.year + self.data.timerObj.month,
        week_id: ((self.data.timerObj.year + "" + self.data.timerObj.week * 1) - 1) + ""
      });
    };
    item.thisWeekList.concat(item.pervWeekList).forEach(el => {
      if (el.isChange) {
        if (el.isAdd) {
          addObj.entityList.push({
            tenant_code: self.data.currentTenant.tenant_code,
            emp_id: self.data.loginUserInfo.usercode,
            id: el.id,
            job_completion: el.job_completion,
            job_name: self.transformJobName(el.job_name),
            job_order: el.job_order + "",
            objective_name: item.objective_name,
            objective_order: item.objective_order + "",
            week_id: el.week_id
          });
        } else {
          updateObj.entityList.push({
            tenant_code: self.data.currentTenant.tenant_code,
            emp_id: self.data.loginUserInfo.usercode,
            id: el.id,
            job_completion: el.job_completion,
            job_name: self.transformJobName(el.job_name),
            job_order: el.job_order + "",
            objective_name: item.objective_name,
            objective_order: item.objective_order + "",
            week_id: el.week_id
          });
        };
      };
    });
  });
  if (bl) {
    that.$message({
      message: 'Objective 不能为空！',
      type: 'warning'
    });
    return;
  };
  if (bl1) {
    that.$message({
      message: '新建 Objective 必须有一项工作重点！',
      type: 'warning'
    });
    return;
  };
  data = [addObj, updateObj, deleteObj];
  if (data[0].entityList.length <= 0 && data[1].entityList.length <= 0 && data[2].entityList.length <=
    0) {
    return;
  };

  self.ctx.executeDataSources({label: 'GmoWeekService', body: data}).then(function (ret) {
    // if (ret.data && ret.data.successed === true) {
    console.log("data : ", data);
    alert("GmoWeekService");
    console.log("ret.data : ", ret.data);
    console.log("ret.successed : ", ret.successed);
    if (ret.data && ret.successed === true) {
      that.$message({
        message: '保存成功！',
        type: 'success'
      });
      self.getGmoEmpBasicInfo();
    } else {
      that.$message({
        message: '保存失败！',
        type: 'warning'
      });
    };

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};



self.toggleDateBtnClick = function (item) {
  // 
  self.data.currentDateUnit = item.key;
};

self.carouselToggle = function (type) {
  if (type === 0) {
    self.data.carouselP = self.data.carouselP + self.data.carouselW;
  } else {
    self.data.carouselP = self.data.carouselP - self.data.carouselW;
  };
  self.initCarousel();
};

self.weekValueChange = function (val) {
  self.getRecentWeek(val);
  let t = setTimeout(() => {
    self.getGmoReportObjective();
  }, 10);
  t = null;
};
self.changeColumns = function () {
  self.data.columns[2].label = "Week" + (self.data.timerObj.week - 1) + "重点工作";
  self.data.columns[4].label = "Week" + (self.data.timerObj.week) + "重点工作";
};
self.initCarousel = function () {
  let clientW = document.body.clientWidth;
  let maxDisplayW = self.data.personnelList.length * self.data.carouselW;
  self.data.showRArrow = clientW + Math.abs(self.data.carouselP) <= maxDisplayW;
};
self.addObjective = function () {
  let uuid = self.generateUUID().replace(/-/g, "");
  let weekId = "" + self.data.timerObj.year + self.data.timerObj.week;
  let jobItem = self.createJobItem(weekId, new Date().getTime() + 10);
  let jobItem1 = self.createJobItem(weekId - 1);
  let item = {
    objective_name: "",
    objective_progress: 0,
    thisSchedule: "",
    modifiable: 1,
    isAdd: true,
    isChange: false,
    objective_code: self.data.workContentList[self.data.workContentList.length -
      1] ? self.data.workContentList[self.data.workContentList.length - 1].objective_code * 1 + 1 : 1,
    objective_order: new Date().getTime(),
    id: uuid,
    thisWeekList: [jobItem],
    pervWeekList: [jobItem1]
  };
  self.data.workContentList.push(item);
};
self.addPervWeek = function (params, index) {
  let weekId = "" + self.data.timerObj.year + self.data.timerObj.week - 1;
  let item = self.createJobItem(weekId);
  params.push(item);
  self.setInputFocus(item.id);
};
self.addThisWeek = function (params, index) {
  let weekId = "" + self.data.timerObj.year + self.data.timerObj.week;
  let item = self.createJobItem(weekId);
  params.push(item);
  self.setInputFocus(item.id);
};
self.createJobItem = function (weekId, jobOrder) {
  let uuid = self.generateUUID().replace(/-/g, "");
  let item = {
    job_order: jobOrder ? jobOrder : new Date().getTime(),
    id: uuid,
    job_name: "1）",
    isAdd: true,
    isChange: false,
    job_completion: "N",
    week_id: "" + weekId
  };
  self.loadAutoTextarea();
  return item;
};

self.setInputFocus = function (elId) {
  setTimeout(() => {
    let el = document.getElementById(elId);
    el.focus();
  }, 10);
};
self.changeAccomplishStatus = function (params) {
  params.isChange = true;
  params.job_completion = params.job_completion === 'Y' ? 'N' : 'Y';
};
self.deleteWork = function (list, index, item) {
  this.$confirm('確定要刪除工作？', '提示', {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    center: true
  }).then(() => {
    list.splice(index, 1);
    list.map((el, i) => {
      el.objective_code = i + 1;
    });
    if (!item.isAdd) {
      if (item.thisWeekList === undefined) {
        self.data.deleteList.push(item.id);
      } else {
        self.data.deleteList.push(item.id);
        item.thisWeekList.forEach(el => {
          self.data.deleteList.push(el.id);
        });
        item.pervWeekList.forEach(el => {
          self.data.deleteList.push(el.id);
        });
      };
    };
    list.forEach((el, i) => {
      el.objective_code = i + 1;
    });
  }).catch(() => {});
};
self.thisScheduleChange = function (el) {
  if ((el.objective_progress * 1 + el.thisSchedule * 1) > 100) {
    el.thisSchedule = 100 - el.objective_progress * 1;
  };
};
self.transformJobName = function (jobName) {

  return jobName === "1）" ? "" : jobName;
};
self.transitionContent = function (str, comparisonStr1) {
  let reg = /([\s]+)([0-9]+)(\）)/g;
  let reg1 = /([\s]+)([0-9]{1,2}\.[0-9]{1,2}\s)/g;
  let newHtml = str.replace(reg, "<br />" + "$1" + "<span class='jobName-No'>" + "$2" + "$3" + "</span>");
  newHtml = newHtml.replace(reg1, "<br />" + "$1" + "<span class='jobName-subNo'>" + "$2" + "</span>");
  arr = newHtml.split("<br />");
  let str1NewHtml = comparisonStr1.replace(reg, "<br />" + "$1" + "<span class='jobName-No'>" + "$2" +
    "$3" +
    "</span>");
  str1NewHtml = str1NewHtml.replace(reg1, "<br />" + "$1" + "<span class='jobName-subNo'>" + "$2" +
    "</span>");
  arr1 = str1NewHtml.split("<br />");
  let supplement = "";
  if (arr1 && arr) {
    let difference = arr1.length - arr.length;
    if (difference > 0) {
      for (let index = 0; index < difference; index++) {
        supplement += `<tr><td style="padding: 8px; font-family: '宋体';"></td></tr>`;
      };
    };
  };
  newHtml = arr.map((el, i) => {
    return `<tr><td style="padding: 8px; font-family: '宋体';">${el}</td></tr>`;
  }).join("");
  newHtml = "<table border='1' cellspacing='0'>" + newHtml + supplement + "</table>";
  if (newHtml.slice(0, 2) === '1）') {
    newHtml = "<span class='jobName-No'>1）</span>" + newHtml.slice(2);
  };
  return newHtml;
};
self.btnClick = function (type, obj) {
  let that = this;
  switch (type) {
    case 'changeShowObject':
      if (obj.hide_week === '') {
        obj.hide_week = "" + self.data.timerObj.year + self.data.timerObj.week - 1;
        self.GmoUpdateHide(0, obj, that);
      } else {
        obj.hide_week = "";
        self.GmoUpdateHide(1, obj, that);
      };
      break;
    case 'save':
      self.saveGmoWeekService(that);
      break;
    case 'return':
      this.$confirm('你确定要返回周会看板吗？请确定您有保存内容。', '提示', {
        confirmButtonText: '前往',
        cancelButtonText: '停留',
        center: true
      }).then(() => {
        let url =
          'http://tm.iisd.efoxconn.com:803/dashboard.html#/iframeDashboards/detail/d53185a2-65f4-415e-a314-9d904f7a1f22/pc/1.0.0';
        window.location.href = url;
      }).catch(() => {});
      break;
    case 'cancel':
      this.$confirm('確定要取消当前修改吗？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        center: true
      }).then(() => {
        self.getGmoEmpBasicInfo();
      }).catch(() => {});
      break;
    case 'exportExcel':
      let style = ``;
      const table = self.ctx.$element.querySelector('#exportWrapXLS0');
      const tables = [...document.querySelectorAll("div[id^='exportWrapXLS']")];
      tables.map((el, i) => {
        let WInfo = self.getWeekInfo(new Date(self.data.weekValue.getTime() - 604800000 * i));
        let WObj = self.getTimerObj(new Date(self.data.weekValue.getTime() - 604800000 * i));
        let sheetName =
          `${self.data.currentPerson.emp_name}${WObj.year}年第${WObj.week}周${WInfo.WeekFirstDayMonth}/${WInfo.WeekFirstDay}-${WInfo.WeekLastDayMonth}/${WInfo.WeekLastDay}周报`;
        self.data.exportXLSTitleList.push(sheetName);
      });
      let startWeek = self.getTimerObj(self.data.weekValue.getTime() - 604800000 * 3);
      let excelName =
        `${self.data.currentPerson.emp_name}${startWeek.year}年第${startWeek.week}周-${self.data.timerObj.week}周周报`;

      setTimeout(() => {
        let exportData = tables.map((el, i) => {
          let sheetName =
            `${self.data.currentPerson.emp_name}${self.data.timerObj.year}年第${self.data.timerObj.week-i}周周报`;
          return {
            sheetname: sheetName,
            data: el.innerHTML
          };
        });

        self.ctx.file.exportDataByHtml(exportData, {
          filename: excelName
        });
      }, 1);
      break;
    case 'exportPDF':
      let WInfo = self.getWeekInfo(new Date(self.data.weekValue));
      self.data.exportTitle =
        `${self.data.currentPerson.emp_name}${self.data.timerObj.year}年第${self.data.timerObj.week}周${WInfo.WeekFirstDayMonth}/${WInfo.WeekFirstDay}-${WInfo.WeekLastDayMonth}/${WInfo.WeekLastDay}周报`;
      let filename = `${self.data.currentPerson.emp_name}${self.data.timerObj.year}年第${self.data.timerObj.week}周周报`;

      setTimeout(() => {
        document.getElementById("exportWrapPDF").scrollTop = 0;
        document.getElementById("exportWrapPDF").scrollIntoView();
        let exportEl = document.getElementById("exportWrapPDF");
        html2canvas(exportEl, {
          allowTaint: true,
          scale: 2
        }).then(function (canvas) {
          let contentWidth = canvas.width;
          let contentHeight = canvas.height;
          let pageData = canvas.toDataURL('image/jpeg', 1.0);
          let pdfX = (contentWidth + 10) / 2 * 0.75;
          let pdfY = (contentHeight + 500) / 2 * 0.75;
          let imgX = pdfX;
          let imgY = (contentHeight / 2 * 0.75);
          let PDF = new jspdf('', 'pt', [pdfX, pdfY]);
          PDF.addImage(pageData, 'jpeg', 0, 0, imgX, imgY);
          PDF.save(filename);
        });
      }, 1);
      break;
    default:
      break;
  };
};
self.getWeekInfo = function (Nowdate) {
  let WeekFirstDay = new Date(Nowdate - (Nowdate.getDay() - 1) * 86400000);
  let WeekLastDay = new Date((WeekFirstDay / 1000 + 6 * 86400) * 1000);
  let obj = {
    WeekFirstDay: WeekFirstDay.getDate(),
    WeekLastDay: WeekLastDay.getDate(),
    WeekFirstDayMonth: WeekFirstDay.getMonth() + 1,
    WeekLastDayMonth: WeekLastDay.getMonth() + 1
  };
  return obj;
};

self.getWeekStartAndWeekEnd = function (d) {
  let year = d.getFullYear();
  let month = parseInt(d.getMonth()) + 1;
  let weekStartFullDate = self.getWeekStart(d).fullDate;
  let weekStart = self.getWeekStart(d).portionDate;
  let newD = new Date(weekStartFullDate);
  newD.setDate(newD.getDate() + 6);
  let monthSunday = parseInt(newD.getMonth()) + 1;
  let rqD = new Date(newD.getTime());
  let weekEnd = parseInt(rqD.getMonth()) + 1 + "/" + rqD.getDate();
  return {
    weekStart,
    weekEnd,
    week: self.getWeek(d)
  };
};

self.getWeekStart = function (date) {
  let week = date.getDay();
  let minus = week ? week - 1 : 6;
  date.setDate(date.getDate() - minus);
  let y = date.getFullYear();
  let m = date.getMonth() + 1;
  let d = date.getDate();
  let fullDate = y + "-" + m + "-" + d;
  let rqD = new Date(date.getTime());
  let portionDate = (rqD.getMonth() + 1) + "/" + rqD.getDate();
  return {
    fullDate,
    portionDate
  };
};

self.getWeek = function (date) {
  let day11 = Date.parse(date);
  day11 = new Date(day11);
  day11.setMonth(0);
  day11.setDate(1);
  day11.setHours(0);
  day11.setMinutes(0);
  day11.setSeconds(0);

  let day11mill = day11.getTime();
  let ori_day = day11.getDay();
  let fill1 = 0;
  if (ori_day !== 0) {
    fill1 = ori_day * 60 * 60 * 24 * 1000;
  };

  let now = Date.parse(date);
  now = new Date(now);
  now.setHours(0);
  now.setMinutes(0);
  now.setSeconds(0);
  let nowmill = now.getTime();
  let now_day = now.getDay();
  let fill2 = 0;
  if (now_day !== 0) {
    fill2 = (7 - now_day) * 60 * 60 * 24 * 1000;
  };

  let cha2 = (nowmill - day11mill + fill1 + fill2) / (60 * 60 * 24 * 1000);
  let week = Math.ceil(cha2 / 7);
  if (week < 10) {
    week = "0" + week;
  };
  let year = now.getFullYear().toString();
  year = year.substring(2);
  return week;
};

self.getRecentWeek = function (date) {
  let currentT = date.getTime();
  let dt = 86400000;
  let getRecentWeekList = [
    currentT - dt * 7 * 3,
    currentT - dt * 7 * 2,
    currentT - dt * 7 * 1,
    currentT,
    currentT + dt * 7 * 1,
    currentT + dt * 7 * 2
  ];
  let req = getRecentWeekList.map(el => {
    let r = self.getWeekStartAndWeekEnd(new Date(el));
    let s = {
      type: 'Week',
      value: r.week,
      timeQuantum: `(${r.weekStart} ~ ${r.weekEnd})`
    };
    console.log("s : ", s);
    return s;
  });
  self.data.timerList = req;
  self.data.timerObj = {
    year: date.getFullYear(),
    week: self.data.timerList[3].value,
    month: date.getMonth() + 1
  };
  self.changeColumns();
};

self.getTimerObj = function name(date) {
  date = date ? new Date(date) : new Date();
  let r = self.getWeekStartAndWeekEnd(new Date(date));
  return {
    year: date.getFullYear(),
    week: r.week,
    month: date.getMonth() + 1
  };
};

self.setObjectiveVal = function (el) {
  let allChinese = /^[\u4e00-\u9fa5]+$/i.test(el.objective_name);
  if (allChinese) {
    el.objective_name.length > 20 && (el.objective_name = el.objective_name.substr(0, 20));
  } else {
    el.objective_name.length > 100 && (el.objective_name = el.objective_name.substr(0, 100));
  };
};

self.inpFocus = function (list) {
  self.data.currentOperation = list;
};

self.dateFormat = function (fmt, date) {
  let ret;
  const opt = {
    "Y+": date.getFullYear().toString(),
    "m+": (date.getMonth() + 1).toString(),
    "d+": date.getDate().toString(),
    "H+": date.getHours().toString(),
    "M+": date.getMinutes().toString(),
    "S+": date.getSeconds().toString()
  };
  for (let k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
    };
  };
  return fmt;
};

self.generateUUID = function () {
  var d = new Date().getTime();
  if (window.performance && typeof window.performance.now === "function") {
    d += performance.now();
  };
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

self.setWeekInputFocus = function (params) {

  document.getElementById("week_value_input").focus();
};

self.isChangeObjOrJobName = function (params) {

  params.isChange = true;
};

self.defaultPrevent = function (e) {
  let keyCode = e.keyCode || e.which || e.charCode;
  if (keyCode === 9 || keyCode === 13) {
    e.preventDefault();
  };
};

self.inpKeyup = function (list, key, e) {
  let isMac = /macintosh|mac os x/i.test(navigator.userAgent);
  let keyCode = e.keyCode || e.which || e.charCode;
  let ctrlKey = e.ctrlKey;
  let combinationList = [65, 66, 67, 86, 88];

  if (isMac && keyCode === 13 && list[key - 1].inTheInput === -1) {
    list[key - 1].inTheInput = 1;
    return;
  };

  let el = e.target;
  let reg = /[0-9]+\）/g;
  let oldS = list[key - 1].job_name;
  let num = "";
  let subNum = "";
  let cursurPosition = 0;
  cursurPosition = el.selectionStart;
  let cursurBefore = oldS.slice(0, cursurPosition);
  let cursurAfter = oldS.slice(cursurPosition);
  let arr = cursurBefore.match(reg);

  if (keyCode === 9) {
    let reg1 = /[0-9]+\.[0-9]+/g;
    if (arr && arr.length > 0) {
      let lastNumStr = arr[arr.length - 1];
      let lastNum = lastNumStr.replace("）", "");
      let subCursurBefore = cursurBefore.slice(cursurBefore.lastIndexOf(lastNumStr));
      let arr1 = subCursurBefore.match(reg1);
      if (arr1 && arr1.length > 0) {
        subNum = "\n " + lastNum + '.' + (arr1[arr1.length - 1].split(".")[1] * 1 + 1) + " ";
      } else {
        subNum = "\n " + lastNum + ".1 ";
      };
    };
    list[key - 1].job_name = cursurBefore + subNum + cursurAfter;
  } else if (keyCode === 13) {
    if (arr && arr.length > 0) {
      num = "\n" + (arr[arr.length - 1].replace("）", "") * 1 + 1 + "）");
    };
    if (oldS.trim() === "" || arr === null) {
      num = "1）";
    };
    list[key - 1].job_name = cursurBefore + num + cursurAfter;
  };

  setTimeout(() => {
    setCursorPosition(el, cursurPosition + num.length + subNum.length);
    self.autoTextarea(el);
  }, 0);

  function setCursorPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);
    } else if (ctrl.createTextRange) {
      let range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    };
  };
};

self.autoTextarea = function (elem, extra, maxHeight) {
  extra = extra || 0;
  var isFirefox = !!document.getBoxObjectFor || 'mozInnerScreenX' in window,
    isOpera = !!window.opera && !!window.opera.toString().indexOf('Opera'),
    addEvent = function (type, callback) {
      elem.addEventListener ?
        elem.addEventListener(type, callback, false) :
        elem.attachEvent('on' + type, callback);
    },
    getStyle = elem.currentStyle ? function (name) {
      var val = elem.currentStyle[name];
      if (name === 'height' && val.search(/px/i) !== 1) {
        var rect = elem.getBoundingClientRect();
        return rect.bottom - rect.top -
          parseFloat(getStyle('paddingTop')) -
          parseFloat(getStyle('paddingBottom')) + 'px';
      };
      return val;
    } : function (name) {
      return getComputedStyle(elem, null)[name];
    },
    minHeight = parseFloat(getStyle('height'));
  elem.style.resize = 'none';
  var change = function () {
    var scrollTop, height,
      padding = 0,
      style = elem.style;
    if (elem._length === elem.value.length) return;
    elem._length = elem.value.length;
    if (!isFirefox && !isOpera) {
      padding = parseInt(getStyle('paddingTop')) + parseInt(getStyle('paddingBottom'));
    };
    scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    elem.style.height = minHeight + 'px';
    if (elem.scrollHeight > minHeight) {
      if (maxHeight && elem.scrollHeight > maxHeight) {
        height = maxHeight - padding;
        style.overflowY = 'auto';
      } else {
        height = elem.scrollHeight - padding;
        style.overflowY = 'hidden';
      };
      style.height = height + extra + 'px';
      scrollTop += parseInt(style.height) - elem.currHeight;
      document.body.scrollTop = scrollTop;
      document.documentElement.scrollTop = scrollTop;
      elem.currHeight = parseInt(style.height);
    };
  };
  addEvent('propertychange', change);
  addEvent('input', change);
  addEvent('focus', change);
  change();
};

self.loadAutoTextarea = function () {
  setTimeout(() => {
    let textareaList = document.querySelectorAll("textarea");
    textareaList.forEach(el => {
      self.autoTextarea(el);
    });
  }, 10);
};

self.monthWeekRelation = function (params) {
  params = params ? new Date(params) : new Date();

  function getDays(year, month) {
    const date = new Date(year, month, 0);
    return date.getDate();
  };

  function getMonthStartAndEnd(d) {
    let firstDate = new Date(d),
      startDate = firstDate.getFullYear() + "-" + ((firstDate.getMonth() + 1) < 10 ? "0" : "") + (
        firstDate
        .getMonth() +
        1) + "-" + "01",
      date = new Date(d),
      currentMonth = date.getMonth(),
      nextMonth = ++currentMonth,
      nextMonthFirstDay = new
    Date(date.getFullYear(), nextMonth, 1), oneDay = 1000 * 60 * 60 * 24, lastDate = new Date(
        nextMonthFirstDay -
        oneDay),
      endDate = lastDate.getFullYear() + "-" + ((lastDate.getMonth() + 1) < 10 ? "0" : "") + (lastDate
        .getMonth() +
        1) +
      "-" + (lastDate.getDate() < 10 ? "0" : "") + lastDate.getDate();
    return [startDate, endDate];
  };

  function getMonthWeek(a, b, c) {
    let date = new Date(a, parseInt(b) - 1, c),
      w = date.getDay(),
      d = date.getDate();
    if (w == 0) {
      w = 7;
    };
    let config = {
      getMonth: date.getMonth() + 1,
      getYear: date.getFullYear(),
      getWeek: Math.ceil((d + 6 - w) / 7)
    };
    return config;
  };
  let copyDate = params ? params : new Date(),
    Y = copyDate.getFullYear(),
    M = copyDate.getMonth() + 1,
    D = copyDate.getDate(),
    monthNum = getDays(Y, M),
    MStartAndEnd = getMonthStartAndEnd(copyDate),
    startGetDays = new
  Date(MStartAndEnd[0]).getDay() === 0 ? 7 : new Date(MStartAndEnd[0]).getDay(), endGetDays = new
  Date(MStartAndEnd[1]).getDay() === 0 ? 7 : new Date(MStartAndEnd[1]).getDay(), startBelongTo =
    startGetDays < 5 ?
    1 : 0,
    endBelongTo = endGetDays > 3 ? 1 : 0,
    wholeW = (monthNum - (7 - startGetDays + 1) - endGetDays) / 7,
    monthWeekNum = wholeW + startBelongTo + endBelongTo,
    MW = getMonthWeek(Y, M, D).getWeek,
    realityMW = startBelongTo === 0 ? MW - 1 : MW;
  return {
    Y,
    M,
    D,
    monthNum,
    MStartAndEnd,
    startGetDays,
    endGetDays,
    startBelongTo,
    endBelongTo,
    wholeW,
    monthWeekNum,
    MW,
    realityMW
  };
};


// ooo-?
self.changeObjectiveSort = function (params) {
  let arr = self.data.workContentList.sort((a, b) => {
    return a.objective_code - b.objective_code;
  });
  let bl = false;
  let res = arr.findIndex((el, index, arr) => {
    return el.id === params.id;
  });
  if (arr[res - 1] && arr[res - 1].objective_code * 1 === arr[res].objective_code * 1) {
    [arr[res], arr[res - 1]] = [arr[res - 1], arr[res]];
  };
  self.data.workContentList.map((el, i) => {
    el.objective_code = i + 1;
  });
};



self.postGmoOrderService = function (that) {
  if (self.data.workContentList.length <= 0) {
    return;
  };
  let url = self.data.baseurl + "/api/datacapture/v1/GmoOrderService/service/",
    data = [{
      "entityList": self.data.workContentList.map(el => {
        return {
          objective_code: el.objective_code * 1,
          id: el.id
        };
      }),
      "opType": "update"
    }];


  self.ctx.executeDataSources({label: 'GmoOrderService', body: data}).then(function (ret) {
    console.log("that.tableData : ", that.tableData);
    // if (ret.data && ret.data.successed === true) {
    if (ret && ret.successed === true) {
      that.$message({
        message: '保存OK！',
        type: 'success'
      });
    } else {
      that.$message({
        message: '排序保存失败！',
        type: 'warning'
      });
    };

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });

};




self.confirmTenant = function () {
  self.data.dialogVisible = false;
  self.getGmoEmpBasicInfo();
};

self.currentTenantValueChange = function (params) {
  self.data.currentTenant = self.data.tenantList.find(el => {
    return el.tenant_code === params;
  });
};



self.onCompositionStart = function (params) {

  params.inTheInput = -1;
};

self.onCompositionEnd = function (params) {
  // 
  // 
};





