
self.myInit = function () {
  var ctx = self.ctx;

  // ctx.toPbuTreeData = function (pbu_list, parent_id) {
  //   return _.sortBy(
  //     _.filter(pbu_list, function (sub_pbu) {
  //       return (sub_pbu.parent_id === parent_id);
  //     }),
  //     ['order']
  //   ).map(function (node) {
  //     return {
  //       id: node.subpbu_id,
  //       label: node.subpbu_name,
  //       //label: node.parent_id ? node.subpbu_name: node.pbu_name,
  //       order: node.order,
  //       children: ctx.toPbuTreeData(pbu_list, node.subpbu_id)
  //     }
  //   });
  // };

  

  //init date-picker default value, 預設開始為當年度的一月
  var dd = new Date();
  self.data.filter.time_id_s = dd.getFullYear() + '01';
  self.data.filter.time_id_e = dd.getFullYear() + dd.getMonth().toString().padStart(2, '0');

  //初始選單資料
  var foreign_data = JSON.parse(window.foreigndata);
  Promise.all([
    ctx.getDataSources({label: 'overall_overview_pbu_list', params: {user_id: foreign_data.usercode}}),
    ctx.getDataSources({label: 'common_currency_list'}),
    ctx.getDataSources({label: 'overall_overview_version_list_actual'}),
    ctx.getDataSources({label: 'overall_overview_version_list_budget'})
  ]).then(function (results) {

    var tt = results[0];
    // console.log(self.toPbuTreeData_TEST(tt));
    //ctx.toPbuTreeData(tt)
    //將資料加入Root結點
    var all_pbu_list = [{
      id : 0,
      label : "Overall",
      order : 0,
      children : self.toPbuTreeData(results[0])
    }];//因為Tree的Data輸入需要Array，Object會出錯
    var currency_table = results[1];
    var actual_version_table = results[2];
    var budget_version_table = results[3];

    self.data.all_pbu_list = all_pbu_list;
    self.data.currency_table = currency_table;
    self.data.actual_version_table = actual_version_table;
    self.data.budget_version_table = budget_version_table;

    //self.searchHandler();

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

/* =============================================  filter使用的 ===========================begin=========================*/

self.toPbuTreeData = function (pbulist) {
  var pbu_lv1 = [];
  var subpbu_map = {};
  // console.log("toPbuTreeData");

  pbulist.forEach(function (sub_pbu) {
    if (_.find(pbu_lv1 , { "pbu_id" : sub_pbu.pbu_id}) === undefined) {
      pbu_lv1.push(sub_pbu);
      _.set(subpbu_map, sub_pbu.pbu_id, []);
    }
    var tmp_ary = _.get(subpbu_map, sub_pbu.pbu_id);
    // console.log(tmp_ary)
    tmp_ary.push(sub_pbu);
    _.set(subpbu_map, sub_pbu.pbu_id, tmp_ary);
  });

  // console.log("=================== LV1");
  // console.log(pbu_lv1);
  // console.log("=================== MAP");
  // console.log(subpbu_map);

  var treeobj = [];
  pbu_lv1.forEach(function (pbu){
    var childs = [];
    var lv1_id = "";
    
    _.get(subpbu_map,pbu.pbu_id).forEach(function (sub_pbu) {
      var node = {
        id: sub_pbu.subpbu_id,
        label: sub_pbu.subpbu_name,
        order: sub_pbu.order
      };
      lv1_id = lv1_id + sub_pbu.subpbu_id;
      childs.push(node);
    });
    //console.log("toPbuTreeData_TEST_3:" + childs.length + ",pbu=" + pbu.pbu_id);

    if (childs.length === 1) { // 表示自己為母節點
      treeobj.push(childs[0]);
    } else { //表示需要產生一個母節點
      var node = {
        id: lv1_id,
        label: pbu.pbu_name,
        order: pbu.order,
        children: childs
      };
      treeobj.push(node);
    }
  });
  //console.log(treeobj);
  return treeobj;
};

self.searchHandler = function () {
  var params =  self.data.filter;
  params["subpbu_id_list"] = _.map(self.data.pickup_pbu_list,'id');
  
  console.log(JSON.stringify(params));

  self.searchOverallData(params);
};

//===========================   自訂的一些 Methods  @Kenny ===========================
//回傳依據Order排序挑選的Pbu
self.orderedPickupPbuList = function () {
  if(self.data.pickup_pbu_list.length > 0){
    return _.sortBy(Object.values(self.data.pickup_pbu_list), ['order']);
  }else
    return [];
};

//移除Tag使用
self.pbuTagRemove = function (tag) {
  //去出選的ID，將該ID移除，再塞回Tree當中
  self.ctx.self.$refs.tree.setCheckedKeys(_.without(_.map(self.data.pickup_pbu_list,'id'),tag.id));
};

//Tree過濾用
self.pbuFilterBy = function (keyword, item) {
  if (!keyword) return true;
  return item.label.toLowerCase().indexOf(keyword.toLowerCase()) >= 0;
};

//Tree的Checkbox Check用
self.treeNodeCheckChange = function (item, isChecked, haveChildSelected) {
  if (!item.children || item.children.length === 0) {
    if (isChecked) {
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat(item);
    } else {
      self.data.pickup_pbu_list.splice(_.findIndex(self.data.pickup_pbu_list,{id:item.id}),1);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat();
    }
  }
};
/* =============================================  filter使用的 ===========================stop=========================*/

/* =============================================  content使用的 ===========================begin=========================*/

self.searchOverallData = function (params) {
  //取回查詢結果資料
  self.ctx.executeDataSources({label: 'overall_overview_report', body: params}).then(function (results) {

    console.log("================================API回傳結果=========================================");
    self.contentDataPreprocess(results);

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

self.contentDataPreprocess = function (results) {
 
  var columns = [];//self.data.overview_result.columns;
  var items = [];//_.map(columns, 'items');
  var sub_items = [];// _.map(items.sub_items);
  var sub_items_map = {};//_.map(items, 'sub_items');

  results.columns.forEach(function (column) {
    var col_colspan = 0;
    //console.log(column);
    column.items.forEach(function (item) {
      //console.log("item",item);
      var item_colspan = 0;    
      item.sub_items.forEach(function (sub_item) {
        const id = _.keys(sub_item)[0];
        const tmp_sub_item = sub_item[id];

        if(tmp_sub_item.css_type !== undefined){
          //console.log("sub_item-css",tmp_sub_item.css_type);
          if(tmp_sub_item.css_type.indexOf("B") >= 0){//標題藍底色
            self.data.baby_blue_100.push(tmp_sub_item.sub_item_name);
          }
          if(tmp_sub_item.css_type.indexOf("G") >= 0){//標題字灰色
            self.data.text_gray.push(tmp_sub_item.sub_item_name);
          }
          if(tmp_sub_item.css_type.indexOf("P") >= 0){//加正號
            self.data.text_plus.push(tmp_sub_item.sub_item_name);
          }
          if(tmp_sub_item.css_type.indexOf("S") >= 0){//加燈號
            self.data.red_green_sign.push(tmp_sub_item.sub_item_name);
          }
        }
        
        // console.log("tmp_sub_item",tmp_sub_item);
        tmp_sub_item["items_name"] = item.items_name;
        sub_items.push(id);
        _.set(sub_items_map, id, tmp_sub_item);
      });

      item_colspan = item.sub_items.length;
      item["colspan"] = item_colspan;
      items.push(item);
      col_colspan += item_colspan;
    });
    column["colspan"] = col_colspan;
    columns.push(column);
  });
  // console.log("================columns");
  // console.log(columns);
  // console.log("================items");
  // console.log(items);
  // console.log("================sub_items");
  // console.log(sub_items);
  // console.log("================sub_items_map");
  // console.log(sub_items_map);

  self.data.overview_head_r1 = columns;
  self.data.overview_head_r2 = items;
  self.data.overview_head_r3 = sub_items;
  self.data.overview_head_map = sub_items_map;
  self.data.overview_data = results.subpbu_list;
  self.data.caption = 'PBU ' + results.time_id_e.toString().substring(0, 4) + ' Overview Dashboard';

};

// self.contentDataPreprocess = function (results) {
 
//   var columns = [];//self.data.overview_result.columns;
//   var items = [];//_.map(columns, 'items');
//   var sub_items = [];// _.map(items.sub_items);
//   var sub_items_map = {};//_.map(items, 'sub_items');

//   results.columns.forEach(function (column) {
//     var col_colspan = 0;
//     column.items.forEach(function (item) {
//       var item_colspan = 0;    
//       _.keys(item.sub_items).forEach(function (id) {
//         const tmp_sub_item = item.sub_items[id];
//         tmp_sub_item["items_name"] = item.items_name;
//         sub_items.push(id);
//         //console.log(item.sub_items[id]);
//         _.set(sub_items_map, id, tmp_sub_item);
//         //sub_items_map.push(id,item.sub_items.get)
//       });

//       item_colspan = _.keys(item.sub_items).length;
//       item["colspan"] = item_colspan;
//       items.push(item);
//       col_colspan += item_colspan;
//     });
//     column["colspan"] = col_colspan;
//     columns.push(column);
//   });
//   // console.log("================columns");
//   // console.log(columns);
//   // console.log("================items");
//   // console.log(items);
//   // console.log("================sub_items");
//   // console.log(sub_items);
//   // console.log("================sub_items_map");
//   // console.log(sub_items_map);

//   self.data.overview_head_r1 = columns;
//   self.data.overview_head_r2 = items;
//   self.data.overview_head_r3 = sub_items;
//   self.data.overview_head_map = sub_items_map;
//   self.data.overview_data = results.subpbu_list;
//   self.data.caption = 'PBU ' + results.time_id_e.toString().substring(0, 4) + ' Overview Dashboard';

// };

self.fixedFloatChange = function (ff) {
  console.log("fixed_float change to=" + self.data.tmp_fixed_float);
  self.data.fixed_float = self.data.tmp_fixed_float;
};

// self.currencyCheckChange = function (item) {
//   console.log("change to=" + item);
// };

// self.toFixedFloatValue = function (value,fixed) {
//   //console.log(value +";" + (""+value).length);
//   return isNaN(value)? value: Number(value).toLocaleString(undefined, {
//     minimumFractionDigits: fixed,
//     maximumFractionDigits: fixed
//   });
// };

self.toSalesVolValue = function (value) {
  let float_digits = self.data.fixed_float;
  return isNaN(value)? value: Number(value).toLocaleString(undefined, {
    minimumFractionDigits: float_digits,
    maximumFractionDigits: float_digits
  });
};

self.toFixedFloatPercentage = function (value,fix) {
  value *= 100;//Sever給的值為原始小數點，需要再乘100
  
  /* 百分比顯示固定到小數點兩位 */
  return isNaN(value)? value: Number(value).toLocaleString(undefined, {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });
};

self.toFixedFloatCurrency = function (value) {
  /* Raw Data(USD)   不四捨五入，完整帶出 */
  /* Thousand(KUSD)  四捨五入到小數點兩位*/
  /* Million(MUSD)   四捨五入到小數點一位 */
  let float_digits = self.data.fixed_float;

  if( self.data.fixed_type  === "Thousand"){
    value /= 1000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: float_digits,
      maximumFractionDigits: float_digits
    });
  }else if( self.data.fixed_type  === "Million"){
    value /= 1000000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: float_digits,
      maximumFractionDigits: float_digits
    });
  }else{
    //return value;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: float_digits,
      maximumFractionDigits: float_digits
    });
  }
};

self.exportToExcel = function () {
  
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.row-collapse img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //remove not aria-checked radion
  table_box.find('.granularity label[tabindex=-1]').remove();
  table_box.find('.granularity input[type=radio]').remove();
  
  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px');
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.row-collapse td').css('background', '#F2F2F2');
  table_box.find('tbody tr.summary td').css('background', '#C9F0FF');
  table_box.find('tbody tr.important td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td span.alert').css('color', '#FF4F4F');
  table_box.find('tbody td span.success').css('color', '#ABDB77');
  table_box.find('tbody td:not(:first-child) ').css('textAlign', 'right');

  table_box.find('tbody td span.postfix-percent').each(function () {
    $(this).html($(this).html()+ '%');
  });

  // table_box.find('tbody td span.prefix-plus').each(function () {
  //   console.log($(this).html());
  //   $(this).html('\+' + $(this).html());
  // });

  //解決匯出%加sign會有亂碼的問題
  table_box.find('tbody td span.sign').each(function () {
    $(this).html($(this).html()+ '&nbsp');
  });

  console.log("Excel table_box 處理完成");
  
  // //本機Work 舊寫法
  // self.ctx.file.exportDataByHtml(table_box.html(), {
  //   filename: self.data.caption,
  //   sheetName: self.data.caption,
  //   style: style
  // });

  // // 上龍宮的新寫法(ref:龍宮文件的)
  exportData = [{
    sheetname : self.data.caption, 
    data: table_box.html()
  }];

  self.ctx.file.exportDataByHtml(exportData, {
    filename: self.data.caption,
    style: style
  });
  
};

self.tbodySpanClass = function (item , isPercent , isTextPlus){
  return {
    'postfix-percent': isPercent && !isNaN(item),
    'alert': 0 > item,
    'prefix-plus' : isTextPlus && item >= 0
  };
};

self.tbodyRowClass = function (item, index , all_count) {
  return {
    'important row-collapse': item.parent_id === null || item.consolidated ,
    'important summary' :  all_count - 1 === index,
    'highlight': self.data.is_highlight_row[index]
  }
};

self.theadClass = function (itemname) {
  let rclass = "";
  // console.log(self.data.baby_blue_100.indexOf(itemname));
  // console.log(itemname,itemname.substring(0,2),itemname.substring(4,6), itemname.length);  
  if(self.data.baby_blue_100.indexOf(itemname) >= 0){
    rclass = "background-baby-blue-100";
  }else if(self.data.text_gray.indexOf(itemname) >= 0){
    rclass = "table-separator text-gray";
  } 
  // }else if( itemname.length >= 6 &&  itemname.length <= 8 &&
  //   itemname.substring(0,2) === '20' && itemname.substring(4,6) === 'FY'){
  //   rclass = "table-separator text-gray";
  // } 
  return rclass;
};

self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined) ? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

self.testConsole = function (outstring) {
  console.log(outstring);
};

/* =============================================  content使用的 ===========================stop=========================*/


self.dataComputed = {
  time_id: {
    // getter
    get: function () {
      return self.data.filter.time_id_e;
    },
    set: function (value) {
      self.data.filter.time_id_e = value;
    }
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  }
};

self.dataWatch = {
  pbu_text: {
    handler : function (keyword) {
      self.ctx.self.$refs.tree.filter(keyword);
    }
  },
  budget_version_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.budget_version_id = newVal.length > 0? newVal[0].version_id: null;
    }
  },
  currency_table: {
    handler: function (newVal, oldVal) {
      self.data.filter.currency_id = newVal.length > 0? newVal[0].currency_id: null;
    }
  }
};


self.onInit = function () {
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};
self.onDataUpdated = function () {};
self.onResize = function () {};
self.onDestroy = function () {};