self.searchOverallData = function (params) {
  var params = {
    "time_id_s": "202101",
    "time_id_e": "202103",
    "subpbu_id_list": [1,2,3,4,5],
    "actual_version_id": 1,
    "budget_version_id": 1,
    "currency_id": 1
  };

  //TODO: fetch data from service api
  //取回查詢結果資料
  ctx.executeDataSources({label: 'overall_overview_report', params: params}).then(function (results) {
    
    console.log("================================API回傳結果=========================================");
    self.contentDataPreprocess(results);

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

self.contentDataPreprocess = function (results) {
 //self.data.overview_result = results;

  var columns = [];//self.data.overview_result.columns;
  var items = [];//_.map(columns, 'items');
  var sub_items = [];// _.map(items.sub_items);
  var sub_items_map = {};//_.map(items, 'sub_items');

  results.columns.forEach(function (column) {
    var col_colspan = 0;
    column.items.forEach(function (item) {
      var item_colspan = 0;
      _.keys(item.sub_items).forEach(function (id){
        sub_items.push(id);
        //console.log(item.sub_items[id]);
        _.set(sub_items_map, id, item.sub_items[id])
        //sub_items_map.push(id,item.sub_items.get)
      });
      
      item_colspan = _.keys(item.sub_items).length
      item["colspan"] = item_colspan;
      items.push(item);
      col_colspan += item_colspan;
    });
    column["colspan"] = col_colspan;
    columns.push(column);
  });
  // console.log("================columns");
  // console.log(columns);
  // console.log("================items");
  // console.log(items);
  // console.log("================sub_items");
  // console.log(sub_items);
  // console.log("================sub_items_map");
  // console.log(sub_items_map);
  
  self.data.overview_head_r1 = columns;
  self.data.overview_head_r2 = items;
  self.data.overview_head_r3 = sub_items;
  self.data.overview_head_map = sub_items_map;
  self.data.overview_data = results.pbu_list;
 

};

// self.roundDecimal = function (val) {
//   var precision = 2;//0取整數,> 1保留n位
//   return  _.isNumber(val) ? Math.round(Math.round(val * Math.pow(10, (precision || 0) + 1)) / 10) / Math.pow(10, (precision || 0)) : "";
// };

self.currencyCheckChange = function (item) {
  console.log("change to=" + item);
  self.data.fixed_type = item;
};

self.toFixedFloatPercentage = function (value,fix) {
  /* 百分比顯示固定到小數點兩位 */
  return isNaN(value)? value: Number(value).toLocaleString(undefined, {
    minimumFractionDigits: fix,
    maximumFractionDigits: fix
  });
};

self.toFixedFloatCurrency = function (value) {
  /* Raw Data(USD)   不四捨五入，完整帶出 */
  /* Thousand(KUSD)  四捨五入到小數點兩位*/
  /* Million(MUSD)   四捨五入到小數點一位 */
  if( self.data.fixed_type  === "KUSD"){
    value = value/1000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })+'K';
  }else if( self.data.fixed_type  === "MUSD"){
    value = value/1000000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: 1,
      maximumFractionDigits: 1
    })+'M';
  }else{
    return isNaN(value)? value: Number(value).toLocaleString();
  }
};

self.exportToExcel = function () {
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.row-collapse img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //remove not aria-checked radion
  table_box.find('.granularity label[tabindex=-1]').remove();

  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px');
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.row-collapse td').css('background', '#F2F2F2');
  table_box.find('tbody tr.summary td').css('background', '#C9F0FF');
  table_box.find('tbody tr.important td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td span.alert').css('color', '#FF4F4F');
  table_box.find('tbody td span.success').css('color', '#ABDB77');
  table_box.find('tbody td:not(:first-child) ').css('textAlign', 'right');

  // table_box.find('tbody td.success, tbody td.deuce').css('textAlign', 'right')
  //   .find('span:first-child').css('color', '#ABDB77');
  // replace newline to <br> in editor cell
  table_box.find('tbody td.editable').each(function (index, el) {
    var node = $(this).find('.el-popover__reference').eq(0);
    $(this).html(node.text().replace(/\n/, '<br>'));
  });

  table_box.find('span.postfix-percent').each(function () {
    $(this).html($(this).html() + '%');
  });

  var all_ms = table_box.find('.margin-summary:not(:last-child)');
  all_ms.each(function () {
    $(this).find('td:first-child ~ td:not(:nth-last-child(-n+2))').each(function () {
      $(this).css('borderBottom', '#000000 1px solid')
        .css('borderLeft', '#FFFFFF 4px solid')
        .css('borderRight', '#FFFFFF 4px solid')
        .find('span:last-child')
        .html(function (index, oldHtml) {
          $(this).html(oldHtml + '%')
        });
    });
  });

  var ms_end = table_box.find('.margin-summary:last-child td:first-child ~ td:not(:nth-last-child(-n+2))')
    .each(function () {
      $(this).css('borderBottom', '#000000 1px double')
        .css('borderLeft', '#FFFFFF 4px double')
        .css('borderRight', '#FFFFFF 4px double')
        .find('span:last-child')
        .html(function (index, oldHtml) {
          $(this).html(oldHtml + '%')
        });
  });

  self.ctx.file.exportDataByHtml(table_box.html(), {
    filename: self.data.caption,
    sheetName: self.data.caption,
    style: style
  });
};


self.tbodyRowClass = function (item, index , all_count) {
  //:class="[{'important row-collapse': row.pbu_id === row.subpbu_id},{'important summary': overview_data.length-1 === idx}]" 
  return {
    'important row-collapse': item.pbu_id === item.subpbu_id,
    'important summary' :  all_count-1 === index,
    'highlight': self.data.is_highlight_row[index]
  }
};

self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

self.dataComputed = {
  
};

self.dataWatch = {

};