
self.searchOverallData = function (params) {
  var params = {
    "time_id_s": "202101",
    "time_id_e": "202103",
    "subpbu_id_list": [1,2,3,4,5],
    "actual_version_id": 1,
    "currency_id": 1
  };

  //TODO: fetch data from service api
  console.log(params);
  // self.ctx.executeDataSources({label: 'actuals_report', body: params}).then(function (result) {
  self.ctx.executeDataSources({label: 'overall_actual_report', body: params}).then(function (result) {
    // console.log(result);
    var collapse_count = 0;
    var current_collapse_index = -1;

    // generate pbu tree structure for UI, table header.
    var pbu_group_list = _.values(_.groupBy(_.values(result.subpbu_list), 'pbu_id'));
    pbu_group_list = _.map(pbu_group_list, function (sub_pbu_list) {
      return _.sortBy(sub_pbu_list, ['subpbu_order'])
    });
    pbu_group_list = _.sortBy(pbu_group_list, [function (pbu) { return pbu[0].subpbu_order; }]);

    // pre-process item values
    result.items.forEach(function (item) {
      if (item.item_type === 'T') {
        collapse_count += 1;
        item.collapse_index = ++current_collapse_index;
      } else {
        item.collapse_index = current_collapse_index;
      }

      if (item.values) {
        //loop in pbu of ui table header
        var item_values = pbu_group_list.map(function (sub_pbus) {
          // console.log('sub_pbus: ', sub_pbus);
          return sub_pbus.map(function (sub_pbu) { // [[x,x,x],[y,y,y],[z,z,z]]
            // console.log('sub_pbu: ', sub_pbu);
            var last_index = sub_pbu.categories.length - 1;
            return sub_pbu.categories.map(function (category, index) { // [x,x,x]
              // console.log('sub_pbu.subpbu_id: ', sub_pbu.subpbu_id);
              // console.log('category: ', category);
              // console.log('item.values: ', item.values);
              // console.log('item.values[sub_pbu.subpbu_id]: ', item.values[sub_pbu.subpbu_id]);
              // console.log(item.values[sub_pbu.subpbu_id + ""][category]);
              return {
                value: item.values[sub_pbu.subpbu_id][category],
                style: {
                  'table-separator': index === last_index
                }
              };
            });
          });
        });
        // console.log('item_values 1: ', item_values);
        item.item_fields = _.flattenDeep(item_values);
        // console.log('item_values 2: ', item_values);
      }
    });

    self.data.table_header_tree = pbu_group_list;
    self.data.actual_report = result;
    // self.data.inMonthColspan=params.time_id_e-params.time_id_s+1+1;
    // self.data.volume=result.items[0].item_name;
    // console.log("self.data.inMonthColspan : ", self.data.inMonthColspan);
    // console.log("self.data.volume : ", self.data.volume);
    // console.log(self.data.actual_report);
    // console.log(result.subpbu_list);
    self.data.column_length = _.reduce(result.subpbu_list, function(column_length, subpbu){
      return column_length + subpbu.categories.length;
    }, 1);
    // console.log("colspan : ", colspan);
    // self.data.column_length = column_length;

    self.data.is_open_group = Array(collapse_count).fill(true);
    self.data.is_highlight_row = Array(result.items.length).fill(false);

    self.data.fraction = self.ctx.self.granularityConfigs.thousand;



  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

self.toggleItemGroup = function (collapse_index) {
  self.toggleFlagInSet(self.data.is_open_group, collapse_index);
};

self.tbodyCellSignClass = function (value) {
  var _class = null;
  // var _value = Number(value);
  if (value === 0) { // 0 == 0.0
    _class = 'deuce';   // 把字 color 設定為 initial
  } else {
    _class = (value > 0)? 'success': 'alert';
  }
  return {
    [_class]: _class
  };
};

self.tableHeaderPbuColspan = function (sub_pbus) {
  return _.reduce(sub_pbus, function (colspan, sub_pbu) {
    return colspan + sub_pbu.categories.length;
  }, 0);
}

self.tbodyRowClass = function (item, index) {
  var item_type = item.item_type;
  return {
    'important': ["M", "V"].indexOf(item_type) < 0,
    'summary': item_type === 'S',
    'margin-summary': item_type === 'MS',
    'margin': item_type === 'M',
    'row-collapse': item_type === 'T',
    'highlight': self.data.is_highlight_row[index]
  }
};

self.tipCurrency = function (value) {
  return self.ctx.self.toCurrency(value, self.ctx.self.granularityConfigs.raw_data);
};

self.tipPercentage = function (value) {
  var config = self.ctx.self.granularityConfigs.raw_data;
  config.digits_of_percentage = undefined;
  return self.ctx.self.toPercentage(value, config) + '%';
};

self.toCurrency = function (value, config) {
  var fraction = config || self.data.fraction;
  return isNaN(value)? value: Number(value / fraction.base).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_currency,
    maximumFractionDigits: (fraction.digits_of_currency === undefined)? 20: fraction.digits_of_currency
  });
};

self.toPercentage = function (value, config) {
  var fraction = config || self.data.fraction;
  return isNaN(value)? value: Number(value * 100).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_percentage,
    maximumFractionDigits: (fraction.digits_of_percentage === undefined)? 20: fraction.digits_of_percentage
  });
};

self.granularityHandler = function (config) {
  self.data.fraction = config;
};

self.dataComputed = {
  granularityConfigs: function () {
    return {
      raw_data: {
        label: 'Raw Data',
        value: 'raw_data',
        base: 1,
        digits_of_currency: undefined,
        digits_of_percentage: 2
      },
      thousand: {
        label: 'Thousand',
        value: 'thousand',
        base: 1000,
        digits_of_currency: 2,
        digits_of_percentage: 2
      },
      million: {
        label: 'Million',
        value: 'million',
        base: 1000000,
        digits_of_currency: 1,
        digits_of_percentage: 2
      }
    };
  }
};

// self.dataComputed = {
//   inMonthColspan: function () {
//     var solid = 4;
//     // return self.data.is_show_products? (solid + self.data.products.length): solid;
//     console.log(" end time : ", self.searchOverallData.params)
//     var duration=self.data.time_id_e-self.data.time_id_s;
//     console.log("duration : ", duration);
//     return duration;
//   }
// };

// self.dataComputed = {
//   inMonthColspan: function () {
//     var solid = 4;
//     return self.data.is_show_products? (solid + self.data.products.length): solid;
//   },
//   cellAmount: function () {
//     var solid = 11;
//     return self.data.is_show_products? (solid + self.data.products.length): solid;
//   },
//   disableSearch: function () {
//     return _.reduce(self.data.filter, function (result, value) {
//       return result || !(_.isNumber(value) || !_.isEmpty(value));
//     }, false);
//   }
// };


self.dataWatch = {};