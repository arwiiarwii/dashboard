
self.myInit = function () {
  var ctx = self.ctx;

  ctx.toPbuTreeData = function (pbu_list, parent_id) {
    return _.sortBy(
      _.filter(pbu_list, function (sub_pbu) {
        return (sub_pbu.parent_id === parent_id);
      }),
      ['order']
    ).map(function (node) {
      return {
        id: node.subpbu_id,
        label: node.parent_id? node.subpbu_name: node.pbu_name,
        order: node.order,
        children: ctx.toPbuTreeData(pbu_list, node.subpbu_id)
      }
    });
  };

  //init date-picker default value, which is previous month.
  var dd = new Date();
  self.data.filter.time_id_s = dd.getFullYear() + Math.max((dd.getMonth() - 3), 1).toString().padStart(2, '0');
  self.data.filter.time_id_e = dd.getFullYear() + dd.getMonth().toString().padStart(2, '0');
  
  //初始選單資料
  var foreign_data = JSON.parse(window.foreigndata);
  Promise.all([
    ctx.getDataSources({label: 'overall_actual_pbu_list', params: {user_id: foreign_data.usercode}}),
    ctx.getDataSources({label: 'common_currency_list'}),
    ctx.getDataSources({label: 'overall_actual_version_list'}),
  ]).then(function (results) {
    
    //將資料加入Root結點
    var all_pbu_list = [{
      id : 0,
      label : "Overall",
      order : 0,
      children : ctx.toPbuTreeData(results[0])
    }];//因為Tree的Data輸入需要Array，Object會出錯
    var currency_table = results[1];
    var actual_version_table = results[2];
    // console.log("===============================1===============================");
    // console.log(all_pbu_list);
    // console.log("===============================2===============================");
    
    self.data.all_pbu_list = all_pbu_list;
    self.data.currency_table = currency_table;
    self.data.actual_version_table = actual_version_table;
    self.data.fraction = self.ctx.self.granularityConfigs.thousand;
        
  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.searchHandler = function () {
  var params =  self.data.filter;
  params["subpbu_id_list"] = _.map(self.data.pickup_pbu_list,'id');
  
  console.log(JSON.stringify(params));

  //TODO: compose search criteria
  self.searchOverallData(params);
};


//===========================   自訂的一些 Methods  @Kenny ===========================
//回傳依據Order排序挑選的Pbu
self.orderedPickupPbuList = function () {
  return _.sortBy(Object.values(self.data.pickup_pbu_list), ['order']);
};

//移除Tag使用
self.pbuTagRemove = function (tag) {
  //去出選的ID，將該ID移除，再塞回Tree當中
  self.ctx.self.$refs.tree.setCheckedKeys(_.without(_.map(self.data.pickup_pbu_list,'id'),tag.id));
};

//Tree過濾用
self.pbuFilterBy = function (keyword, item) {
  if (!keyword) return true;
  return item.label.indexOf(keyword) >= 0;
};

//Tree的Checkbox Check用
self.treeNodeCheckChange = function (item, isChecked, haveChildSelected) {
  if (!item.children || item.children.length === 0) {
    if (isChecked) {
      // console.log(":Set=" + item.id);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat(item);
    } else {
      // console.log(":Delete=" + item.id);
      self.data.pickup_pbu_list.splice(_.findIndex(self.data.pickup_pbu_list,{id:item.id}),1);
      self.data.pickup_pbu_list = self.data.pickup_pbu_list.concat();
    }
  }
};

/*** Content相關js 開始 */

self.searchOverallData = function (params) {
  // var params = {
  //   "time_id_s": "202101",
  //   "time_id_e": "202103",
  //   "subpbu_id_list": [1,2,3,4,5],
  //   "actual_version_id": 1,
  //   "currency_id": 1
  // };

  //TODO: fetch data from service api
  console.log(params);

  self.ctx.executeDataSources({label: 'overall_actual_report', body: params}).then(function (result) {

    // console.log(result);
    var collapse_count = 0;
    var current_collapse_index = -1;

    // generate pbu tree structure for UI, table header.
    // 編排 result.pub_list 的內容
    var pbu_group_list = _.values(_.groupBy(_.values(result.subpbu_list), 'pbu_id'));

    // pbu_group_list : 將 pbu_list 與 sub_pbu_list 寫入 list
    pbu_group_list = _.map(pbu_group_list, function (sub_pbu_list) {
      return _.sortBy(sub_pbu_list, ['subpbu_order'])
    });

    // 以 pbu_group_list 排序
    pbu_group_list = _.sortBy(pbu_group_list, [function (pbu) { return pbu[0].subpbu_order; }]);
    // console.log(result.items); 

    // pre-process item values
    // 先取出 results.items 的內容
    result.items.forEach(function (item) {

      // 將每個 item 新增一個 item.collapse_index 屬性
      if (item.item_type === 'T') {
        collapse_count += 1;
        item.collapse_index = ++current_collapse_index;
      } else {
        item.collapse_index = current_collapse_index;
      }

      if (item.values) {
        //loop in pbu of ui table header
        // 將 sub_pbus mapping 進 pbu_group_list 中
        var item_values = pbu_group_list.map(function (sub_pbus) {
          // console.log('sub_pbus: ', sub_pbus);

          // 將每一組 sub_pbu mapping 進 sub_pbus 中
          return sub_pbus.map(function (sub_pbu) { // [[x,x,x],[y,y,y],[z,z,z]]
            // console.log('sub_pbu: ', sub_pbu);

            // 若 categories 的數量-1 =false 
            var last_index = sub_pbu.categories.length - 1;
            return sub_pbu.categories.map(function (category, index) { // [x,x,x]
              // console.log('sub_pbu.subpbu_id: ', sub_pbu.subpbu_id);
              // console.log('category: ', category);
              // console.log('item.values: ', item.values);
              // console.log('item.values[sub_pbu.subpbu_id]: ', item.values[sub_pbu.subpbu_id]);
              // console.log(item.values[sub_pbu.subpbu_id + ""][category]);
              return {
                // items fields.value : 每個 item 列往右的每個 value 
                value: item.values[sub_pbu.subpbu_id][category],
                style: {
                  'table-separator': index === last_index
                }
              };
            });
          });
        });
        // console.log('item_values 1: ', item_values);
        // _.flattenDeep 把 array 展平
        item.item_fields = _.flattenDeep(item_values);
        // console.log('item_values 2: ', item_values);
      }
    });

    self.data.table_header_tree = pbu_group_list;
    self.data.actual_report = result;
    // self.data.inMonthColspan=params.time_id_e-params.time_id_s+1+1;
    // self.data.volume=result.items[0].item_name;
    // console.log("self.data.inMonthColspan : ", self.data.inMonthColspan);
    // console.log("self.data.volume : ", self.data.volume);
    // console.log(self.data.actual_report);
    // console.log(result.subpbu_list);
    // console.log("self.data.table_header_tree = " + self.data.table_header_tree);

    // 把 results.subpbu_list 走一遍，計算全部 subpbu.categories 總合，self.data.column_length 為所有欄位數量
    self.data.column_length = _.reduce(result.subpbu_list, function(column_length, subpbu){
      return column_length + subpbu.categories.length;
    }, 1);
    // console.log("colspan : ", colspan);
    // self.data.column_length = column_length;

    self.data.is_open_group = Array(collapse_count).fill(true);
    self.data.is_highlight_row = Array(result.items.length).fill(false);

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};


// 點選 sidebar 時，隱藏與顯示都會更換 icon
self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

// 點選 toggleSidebar 時，把 self.data.is_hide_sidebar 選項變相反： true -> false / false -> true
self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

// 折行效果
self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

// 折行效果
self.toggleItemGroup = function (collapse_index) {
  self.toggleFlagInSet(self.data.is_open_group, collapse_index);
};

// 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
self.tbodyCellSignClass = function (value) {
  var _class = null;
  // var _value = Number(value);
  if (value === 0) { // 0 == 0.0
    _class = 'deuce';   // 把字 color 設定為 initial
  } else {
    _class = (value > 0)? 'success': 'alert';
  }
  return {
    [_class]: _class
  };
};

// // 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
//   var _class = null;
//   // var _value = Number(value);
//   if (value === 0) { // 0 == 0.0
//     _class = 'deuce';
//   } else if (flag === 'positive') {
//     _class = (value > 0)? 'success': 'alert';
//   } else if (flag === 'negative') {
//     _class = (value < 0)? 'success': 'alert';
//   }
//   return {
//     [_class]: _class,
//     [flag]: flag
//   };
// };

// 第算 title 第一列的 colspan
self.tableHeaderPbuColspan = function (sub_pbus) {
  return _.reduce(sub_pbus, function (colspan, sub_pbu) {
    return colspan + sub_pbu.categories.length;
  }, 0);
}


// 根據每一列的 item_type 決定使用的 class
self.tbodyRowClass = function (item, index) {
  var item_type = item.item_type;
  return {
    'important': ["M", "V"].indexOf(item_type) < 0,
    'summary': item_type === 'S',
    'margin-summary': item_type === 'MS',
    'margin': item_type === 'M',
    'row-collapse': item_type === 'T',
    'highlight': self.data.is_highlight_row[index]
  }
};

// tooltip 的內容要顯示 raw_data 的 value
self.tipCurrency = function (value) {
  return self.ctx.self.toCurrency(value, self.ctx.self.granularityConfigs.raw_data);
};

// tooltip 的內容要顯示 raw_data 的 value
self.tipPercentage = function (value) {
  var config = self.ctx.self.granularityConfigs.raw_data;
  config.digits_of_percentage = undefined;
  return self.ctx.self.toPercentage(value, config) + '%';
};


// 定義數值的欄位，最大最小位數
self.toCurrency = function (value, config) {
  var fraction = config || self.data.fraction;
  // return isNaN(value)? value: Number(value / self.data.base).toLocaleString(undefined, {
  return isNaN(value)? value: Number(value / fraction.base).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_currency,
    maximumFractionDigits: (fraction.digits_of_currency === undefined)? 20: fraction.digits_of_currency
  });
};

// 定義小數點的欄位，總共幾位數？ fraction.digits_of_percentage
self.toPercentage = function (value, config) {
  var fraction = config || self.data.fraction;
  return isNaN(value)? value: Number(value * 100).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_percentage,
    maximumFractionDigits: (fraction.digits_of_percentage === undefined)? 20: fraction.digits_of_percentage
  });
};

// 頁面點選 radio 取得 config 的設定
self.granularityHandler = function (config) {
  self.data.fraction = config;
};

/**** Excel Export 功能 */


self.exportToExcel = function () {
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.row-collapse img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px').html('Granularity: ' + self.data.fraction.label);
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.row-collapse td').css('background', '#F2F2F2');
  table_box.find('tbody tr.summary td').css('background', '#C9F0FF');
  table_box.find('tbody tr.important td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td.alert').css('color', '#FF4F4F').css('textAlign', 'right');
  
  table_box.find('span.percentage').each(function () {
    $(this).html($(this).html() + '%');
  });

  // self.ctx.file.exportDataByHtml(table_box.html(), {
  //   filename: self.data.caption,
  //   sheetName: self.data.caption,
  //   style: style
  // });

  // 上龍宮的新寫法(ref龍宮文件)
  exportData = [{
    sheetname : self.data.caption, 
    data: table_box.html()
  }];

  self.ctx.file.exportDataByHtml(exportData, {
    filename: self.data.caption,
    style: style
  });

};


/*** Content相關js 結束 */
self.dataComputed = {
  time_id: {
    // getter
    get: function () {
      return [self.data.filter.time_id_s, self.data.filter.time_id_e];
    },
    set: function (value) {
      self.data.filter.time_id_s = value[0];
      self.data.filter.time_id_e = value[1];
    }
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  },
  granularityConfigs: function () { // 定義顆粒度的三種資料型態 ： raw_data / thousand / millions
    return {
      raw_data: {
        label: 'Raw Data',
        value: 'raw_data',
        base: 1,
        digits_of_currency: undefined,
        digits_of_percentage: 2
      },
      thousand: {
        label: 'Thousand',
        value: 'thousand',
        base: 1000,
        digits_of_currency: 2,
        digits_of_percentage: 2
      },
      million: {
        label: 'Million',
        value: 'million',
        base: 1000000,
        digits_of_currency: 1,
        digits_of_percentage: 2
      }
    };
  }
};

self.dataWatch = {
  "pbu_text": {
    handler : function (keyword) {
      self.ctx.self.$refs.tree.filter(keyword);
    }
  }
};

self.onInit = function () {
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};
self.onDataUpdated = function () {};
self.onResize = function () {};
self.onDestroy = function () {};