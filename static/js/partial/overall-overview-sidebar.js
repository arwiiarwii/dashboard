
self.myInit = function () {
  var ctx = self.ctx;

  ctx.toPbuTreeData = function (pbu_list, parent_id) {
    return _.sortBy(
      _.filter(pbu_list, function (sub_pbu) {
        return (sub_pbu.parent_id === parent_id);
      }),
      ['order']
    ).map(function (node) {
      return {
        id: node.subpbu_id,
        label: node.parent_id? node.subpbu_name: node.pbu_name,
        order: node.order,
        children: ctx.toPbuTreeData(pbu_list, node.subpbu_id)
      }
    });
  };
  
  //init date-picker default value, 預設開始為當年度的一月
  var dd = new Date();
  self.data.filter.time_id_s = dd.getFullYear() + '01';
  self.data.filter.time_id_e = dd.getFullYear() + dd.getMonth().toString().padStart(2, '0');
  
  //初始選單資料
  var foreign_data = JSON.parse(window.foreigndata);
  Promise.all([
    ctx.getDataSources({label: 'overall_overview_pbu_list', params: {user_id: foreign_data.usercode}}),
    ctx.getDataSources({label: 'common_currency_list'}),
    ctx.getDataSources({label: 'overall_overview_version_list_actual'}),
    ctx.getDataSources({label: 'overall_overview_version_list_budget'})
  ]).then(function (results) {
  
    //將資料加入Root結點
    var all_pbu_list = [{
      id : 0,
      label : "Overall",
      order : 0,
      children : ctx.toPbuTreeData(results[0])
    }];//因為Tree的Data輸入需要Array，Object會出錯
    var currency_table = results[1];
    var actual_version_table = results[2];
    var budget_version_table = results[3];
        
    self.data.all_pbu_list = all_pbu_list;
    self.data.currency_table = currency_table;
    self.data.actual_version_table = actual_version_table;
    self.data.budget_version_table = budget_version_table;
    
  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

/* =============================================  filter使用的 ===========================begin=========================*/

self.searchHandler = function () {
  var params =  self.data.filter;
  
  console.log("========================Actual filter result==============================");
  console.log(JSON.stringify(params))

  //TODO: compose search criteria
  self.searchOverallData(params);
};

//===========================   自訂的一些 Methods  @Kenny ===========================
//回傳依據Order排序挑選的Pbu
self.orderedPickupPbuList = function () {
  return _.sortBy(Object.values(self.data.pickup_pbu_list), ['order']);
};

//移除Tag使用
self.pbuTagRemove = function (tag) {
  self.ctx.self.$delete(self.data.pickup_pbu_list, tag.id);
  self.data.filter.subpbu_id_list = _.map(self.data.pickup_pbu_list, 'id');
  self.ctx.self.$refs.tree.setCheckedKeys(self.data.filter.subpbu_id_list);
};

//Tree過濾用
self.pbuFilterBy = function (keyword, item) {
  if (!keyword) return true;
  return item.label.indexOf(keyword) >= 0;
};

//Tree的Checkbox Check用
self.treeNodeCheckChange = function (item, isChecked, haveChildSelected) {
  if (!item.children || item.children.length === 0) {
    if (isChecked) {
      self.ctx.self.$set(self.data.pickup_pbu_list, item.id, item);
    } else {
      self.ctx.self.$delete(self.data.pickup_pbu_list, item.id);
    }
  }
  
  self.data.filter.subpbu_id_list = _.map(self.data.pickup_pbu_list, 'id');
};
/* =============================================  filter使用的 ===========================stop=========================*/

/* =============================================  content使用的 ===========================begin=========================*/

self.searchOverallData = function (params) {
  // var params = {
  //   "time_id_s": "202101",
  //   "time_id_e": "202103",
  //   "subpbu_id_list": [1,2,3,4,5],
  //   "actual_version_id": 1,
  //   "budget_version_id": 1,
  //   "currency_id": 1
  // };

  //TODO: fetch data from service api
  //取回查詢結果資料
  ctx.executeDataSources({label: 'overall_overview_report', params: params}).then(function (results) {
    
    console.log("================================API回傳結果=========================================");
    self.contentDataPreprocess(results);

  }).catch(function (err) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};

self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

self.contentDataPreprocess = function (results) {
 //self.data.overview_result = results;

  var columns = [];//self.data.overview_result.columns;
  var items = [];//_.map(columns, 'items');
  var sub_items = [];// _.map(items.sub_items);
  var sub_items_map = {};//_.map(items, 'sub_items');

  results.columns.forEach(function (column) {
    var col_colspan = 0;
    column.items.forEach(function (item) {
      var item_colspan = 0;
      _.keys(item.sub_items).forEach(function (id){
        sub_items.push(id);
        //console.log(item.sub_items[id]);
        _.set(sub_items_map, id, item.sub_items[id])
        //sub_items_map.push(id,item.sub_items.get)
      });
      
      item_colspan = _.keys(item.sub_items).length
      item["colspan"] = item_colspan;
      items.push(item);
      col_colspan += item_colspan;
    });
    column["colspan"] = col_colspan;
    columns.push(column);
  });
  // console.log("================columns");
  // console.log(columns);
  // console.log("================items");
  // console.log(items);
  // console.log("================sub_items");
  // console.log(sub_items);
  // console.log("================sub_items_map");
  // console.log(sub_items_map);
  
  self.data.overview_head_r1 = columns;
  self.data.overview_head_r2 = items;
  self.data.overview_head_r3 = sub_items;
  self.data.overview_head_map = sub_items_map;
  self.data.overview_data = results.pbu_list;
 

};

self.currencyCheckChange = function (item) {
  console.log("change to=" + item);
  self.data.fixed_type = item;
};

self.toFixedFloatPercentage = function (value,fix) {
  /* 百分比顯示固定到小數點兩位 */
  return isNaN(value)? value: Number(value).toLocaleString(undefined, {
    minimumFractionDigits: fix,
    maximumFractionDigits: fix
  });
};

self.toFixedFloatCurrency = function (value) {
  /* Raw Data(USD)   不四捨五入，完整帶出 */
  /* Thousand(KUSD)  四捨五入到小數點兩位*/
  /* Million(MUSD)   四捨五入到小數點一位 */
  if( self.data.fixed_type  === "thousand"){
    value = value/1000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }else if( self.data.fixed_type  === "million"){
    value = value/1000000;
    return isNaN(value)? value: Number(value).toLocaleString(undefined, {
      minimumFractionDigits: 1,
      maximumFractionDigits: 1
    });
  }else{
    return isNaN(value)? value: Number(value).toLocaleString();
  }
};

self.exportToExcel = function () {
  var $ = self.ctx.$;
  var style = 'br {mso-data-placement:same-cell;}';
  var table_box = $('div.box').clone();
  table_box.find('table').css('fontSize', '16px').css('wordBreak', 'keep-all').css('whiteSpace', 'nowrap');
  //remove popover
  table_box.find('.el-popover').remove();
  //remove row collapse icons
  table_box.find('.row-collapse img').remove();
  //remove thead icons
  table_box.find('th .control').remove();
  //remove not aria-checked radion
  table_box.find('.granularity label[tabindex=-1]').remove();

  //get thead
  table_box.find('thead tr:first-child th:first-child').css('width', '420px');
  table_box.find('thead tr').css('color', '#FFFFFF');
  table_box.find('thead tr th').css('background', '#00ADEF').css('border', '1px #FFFFFF solid');

  //get collapse row
  table_box.find('tbody tr.row-collapse td').css('background', '#F2F2F2');
  table_box.find('tbody tr.summary td').css('background', '#C9F0FF');
  table_box.find('tbody tr.important td').css('fontWeight', 'bold').css('fontSize', '16px');
  // set indent
  table_box.find('tbody td.indent-1').css('paddingLeft', '38px');
  table_box.find('tbody td.indent-2').css('paddingLeft', '63px');
  table_box.find('tbody td.indent-3').css('paddingLeft', '93px');
  // set sign and font-color
  table_box.find('tbody td span.alert').css('color', '#FF4F4F');
  table_box.find('tbody td span.success').css('color', '#ABDB77');
  table_box.find('tbody td:not(:first-child) ').css('textAlign', 'right');

  table_box.find('tbody td span.postfix-percent').each(function () {
    $(this).html($(this).html()+ '%');    
  });

  //解決匯出%加sign會有亂碼的問題
  table_box.find('tbody td span.sign').each(function () {
    $(this).html($(this).html()+ '&nbsp');    
  });

  // table_box.find('span.sign').each(function () {
  //   $(this).html($(this).html());
  // });

  // var all_ms = table_box.find('.margin-summary:not(:last-child)');
  // all_ms.each(function () {
  //   $(this).find('td:first-child ~ td:not(:nth-last-child(-n+2))').each(function () {
  //     $(this).css('borderBottom', '#000000 1px solid')
  //       .css('borderLeft', '#FFFFFF 4px solid')
  //       .css('borderRight', '#FFFFFF 4px solid')
  //       .find('span:last-child')
  //       .html(function (index, oldHtml) {
  //         $(this).html(oldHtml + '%')
  //       });
  //   });
  // });

  // var ms_end = table_box.find('.margin-summary:last-child td:first-child ~ td:not(:nth-last-child(-n+2))')
  //   .each(function () {
  //     $(this).css('borderBottom', '#000000 1px double')
  //       .css('borderLeft', '#FFFFFF 4px double')
  //       .css('borderRight', '#FFFFFF 4px double')
  //       .find('span:last-child')
  //       .html(function (index, oldHtml) {
  //         $(this).html(oldHtml + '%')
  //       });
  // });

  self.ctx.file.exportDataByHtml(table_box.html(), {
    filename: self.data.caption,
    sheetName: self.data.caption,
    style: style
  });
};


self.tbodyRowClass = function (item, index , all_count) {
  //:class="[{'important row-collapse': row.pbu_id === row.subpbu_id},{'important summary': overview_data.length-1 === idx}]" 
  return {
    'important row-collapse': item.pbu_id === item.subpbu_id,
    'important summary' :  all_count-1 === index,
    'highlight': self.data.is_highlight_row[index]
  }
};

self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

/* =============================================  content使用的 ===========================stop=========================*/


self.dataComputed = {
  time_id: {
    // getter
    get: function () {
      return self.data.filter.time_id_e;
    },
    set: function (value) {
      self.data.filter.time_id_e = value;
    }
  },
  disableSearch: function () {
    return _.reduce(self.data.filter, function (result, value) {
      return result || !(_.isNumber(value) || !_.isEmpty(value));
    }, false);
  }

};

self.dataWatch = {
  "pbu_text": {
    handler : function (keyword) {
      self.ctx.self.$refs.tree.filter(keyword);
    }
  }
};


self.onInit = function () {
  var _ctx = self.ctx;

  // use _promisify to make function not only response by callback function, but also return promise.
  var _promisify = function (_function, middleware) {
    var _middleware = middleware || function (config) {return config;};

    return function (config, callback) {
      return new Promise(function (resolve, reject) {
        config = _middleware(config);
        _function(config, function (data) {
          //error handling
          try {
            if (data.error) {
              throw {
                code: data.error.code,
                message: `Remote Service Error[${_function.name}]: ${data.error.msg}`
              };
            } else if (data instanceof Error) {
              console.error(data);
              throw {
                code: -1,
                message: `Remote Service Error[${_function.name}]: ${data.message}. 
              Please check your network or contact developer. `
              };
            }
            callback && callback(data);
            resolve(data);
          } catch (e) {
            callback && callback(null, e);
            reject(e);
          }
        });
      });
    };
  };

  // make build-in function support promise return.
  _ctx.getDataSources = _promisify(_ctx.getDataSources.bind(_ctx), function (config) {
    config.params = {params: JSON.stringify(config.params || {})};
    return config;
  });
  _ctx.executeDataSources = _promisify(_ctx.executeDataSources.bind(_ctx), function (config) {
    config.body = config.body || {};
    return config;
  });

  self.myInit();

  self.data.init = true;
};
self.onDataUpdated = function () {};
self.onResize = function () {};
self.onDestroy = function () {};