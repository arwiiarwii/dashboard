
self.searchOverallData = function (params) {
  var params = {
    "time_id_s": "202101",
    "time_id_e": "202103",
    "subpbu_id_list": [1,2,3,4,5],
    "actual_version_id": 1,
    "currency_id": 1
  };

  //TODO: fetch data from service api
  console.log(params);

  self.ctx.executeDataSources({label: 'overall_actual_report', body: params}).then(function (result) {

    // console.log(result);
    var collapse_count = 0;
    var current_collapse_index = -1;

    // generate pbu tree structure for UI, table header.
    // 編排 result.pub_list 的內容
    var pbu_group_list = _.values(_.groupBy(_.values(result.subpbu_list), 'pbu_id'));

    // pbu_group_list : 將 pbu_list 與 sub_pbu_list 寫入 list
    pbu_group_list = _.map(pbu_group_list, function (sub_pbu_list) {
      return _.sortBy(sub_pbu_list, ['subpbu_order'])
    });

    // 以 pbu_group_list 排序
    pbu_group_list = _.sortBy(pbu_group_list, [function (pbu) { return pbu[0].subpbu_order; }]);
    // console.log(result.items); 

    // pre-process item values
    // 先取出 results.items 的內容
    result.items.forEach(function (item) {

      // 將每個 item 新增一個 item.collapse_index 屬性
      if (item.item_type === 'T') {
        collapse_count += 1;
        item.collapse_index = ++current_collapse_index;
      } else {
        item.collapse_index = current_collapse_index;
      }

      if (item.values) {
        //loop in pbu of ui table header
        // 將 sub_pbus mapping 進 pbu_group_list 中
        var item_values = pbu_group_list.map(function (sub_pbus) {
          // console.log('sub_pbus: ', sub_pbus);

          // 將每一組 sub_pbu mapping 進 sub_pbus 中
          return sub_pbus.map(function (sub_pbu) { // [[x,x,x],[y,y,y],[z,z,z]]
            // console.log('sub_pbu: ', sub_pbu);

            // 若 categories 的數量-1 =false 
            var last_index = sub_pbu.categories.length - 1;
            return sub_pbu.categories.map(function (category, index) { // [x,x,x]
              // console.log('sub_pbu.subpbu_id: ', sub_pbu.subpbu_id);
              // console.log('category: ', category);
              // console.log('item.values: ', item.values);
              // console.log('item.values[sub_pbu.subpbu_id]: ', item.values[sub_pbu.subpbu_id]);
              // console.log(item.values[sub_pbu.subpbu_id + ""][category]);
              return {
                // items fields.value : 每個 item 列往右的每個 value 
                value: item.values[sub_pbu.subpbu_id][category],
                style: {
                  'table-separator': index === last_index
                }
              };
            });
          });
        });
        // console.log('item_values 1: ', item_values);
        // _.flattenDeep 把 array 展平
        item.item_fields = _.flattenDeep(item_values);
        // console.log('item_values 2: ', item_values);
      }
    });

    self.data.table_header_tree = pbu_group_list;
    self.data.actual_report = result;
    // self.data.inMonthColspan=params.time_id_e-params.time_id_s+1+1;
    // self.data.volume=result.items[0].item_name;
    // console.log("self.data.inMonthColspan : ", self.data.inMonthColspan);
    // console.log("self.data.volume : ", self.data.volume);
    // console.log(self.data.actual_report);
    // console.log(result.subpbu_list);
    // console.log("self.data.table_header_tree = " + self.data.table_header_tree);

    // 把 results.subpbu_list 走一遍，計算全部 subpbu.categories 總合，self.data.column_length 為所有欄位數量
    self.data.column_length = _.reduce(result.subpbu_list, function(column_length, subpbu){
      return column_length + subpbu.categories.length;
    }, 1);
    // console.log("colspan : ", colspan);
    // self.data.column_length = column_length;

    self.data.is_open_group = Array(collapse_count).fill(true);
    self.data.is_highlight_row = Array(result.items.length).fill(false);

    self.data.fraction = self.ctx.self.granularityConfigs.thousand;
    console.log("self.data.fraction : " + self.data.fraction);

  }).catch(function (e) {
    self.ctx.self.$message({
      message: 'Something wrong.',
      type: 'error'
    });
  });
};


// 點選 sidebar 時，隱藏與顯示都會更換 icon
self.sidebarToggleIconSrc = function (isHideSidebar) {
  var img_name = isHideSidebar? 'sidebar-close.svg': 'sidebar-open.svg';
  return self.data.assets_path + 'static/media/' + img_name;
};

// 點選 toggleSidebar 時，把 self.data.is_hide_sidebar 選項變相反： true -> false / false -> true
self.toggleSidebar = function () {
  self.data.is_hide_sidebar = !self.data.is_hide_sidebar;
};

// 折行效果
self.toggleFlagInSet = function (store, index, isOpen) {
  var flag = (isOpen === undefined)? !store[index]: !!isOpen;
  self.ctx.self.$set(store, index, flag);
};

// 折行效果
self.toggleItemGroup = function (collapse_index) {
  self.toggleFlagInSet(self.data.is_open_group, collapse_index);
};

// 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
self.tbodyCellSignClass = function (value) {
  var _class = null;
  // var _value = Number(value);
  if (value === 0) { // 0 == 0.0
    _class = 'deuce';   // 把字 color 設定為 initial
  } else {
    _class = (value > 0)? 'success': 'alert';
  }
  return {
    [_class]: _class
  };
};

// // 根據 item.item_flag 定義數值的顏色 style
// self.tbodyCellSignClass = function (value, flag) {
//   var _class = null;
//   // var _value = Number(value);
//   if (value === 0) { // 0 == 0.0
//     _class = 'deuce';
//   } else if (flag === 'positive') {
//     _class = (value > 0)? 'success': 'alert';
//   } else if (flag === 'negative') {
//     _class = (value < 0)? 'success': 'alert';
//   }
//   return {
//     [_class]: _class,
//     [flag]: flag
//   };
// };

// 第算 title 第一列的 colspan
self.tableHeaderPbuColspan = function (sub_pbus) {
  return _.reduce(sub_pbus, function (colspan, sub_pbu) {
    return colspan + sub_pbu.categories.length;
  }, 0);
}


// 根據每一列的 item_type 決定使用的 class
self.tbodyRowClass = function (item, index) {
  var item_type = item.item_type;
  return {
    'important': ["M", "V"].indexOf(item_type) < 0,
    'summary': item_type === 'S',
    'margin-summary': item_type === 'MS',
    'margin': item_type === 'M',
    'row-collapse': item_type === 'T',
    'highlight': self.data.is_highlight_row[index]
  }
};

// tooltip 的內容要顯示 raw_data 的 value
self.tipCurrency = function (value) {
  return self.ctx.self.toCurrency(value, self.ctx.self.granularityConfigs.raw_data);
};

// tooltip 的內容要顯示 raw_data 的 value
self.tipPercentage = function (value) {
  var config = self.ctx.self.granularityConfigs.raw_data;
  config.digits_of_percentage = undefined;
  return self.ctx.self.toPercentage(value, config) + '%';
};


// 定義數值的欄位，最大最小位數
self.toCurrency = function (value, config) {
  var fraction = config || self.data.fraction;
  // return isNaN(value)? value: Number(value / self.data.base).toLocaleString(undefined, {
  return isNaN(value)? value: Number(value / fraction.base).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_currency,
    maximumFractionDigits: (fraction.digits_of_currency === undefined)? 20: fraction.digits_of_currency
  });
};

// 定義小數點的欄位，總共幾位數？ fraction.digits_of_percentage
self.toPercentage = function (value, config) {
  var fraction = config || self.data.fraction;
  return isNaN(value)? value: Number(value * 100).toLocaleString(undefined, {
    minimumFractionDigits: fraction.digits_of_percentage,
    maximumFractionDigits: (fraction.digits_of_percentage === undefined)? 20: fraction.digits_of_percentage
  });
};

// 頁面點選 radio 取得 config 的設定
self.granularityHandler = function (config) {
  self.data.fraction = config;
};


// 定義顆粒度的三種資料型態 ： raw_data / thousand / millions
self.dataComputed = {
  granularityConfigs: function () {
    return {
      raw_data: {
        label: 'Raw Data',
        value: 'raw_data',
        base: 1,
        digits_of_currency: undefined,
        digits_of_percentage: 2
      },
      thousand: {
        label: 'Thousand',
        value: 'thousand',
        base: 1000,
        digits_of_currency: 2,
        digits_of_percentage: 2
      },
      million: {
        label: 'Million',
        value: 'million',
        base: 1000000,
        digits_of_currency: 1,
        digits_of_percentage: 2
      }
    };
  }
};

// console.log("granularityConfigs = " + granularityConfigs);

self.dataWatch = {};

