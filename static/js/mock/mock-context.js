var getDataSources = function ({label, params}, callback = function () {}) {
  console.log('getDateResources: ', label, 'params: ', params);
  this.actual_pbu_list = window.actual_pbu_list;
  this.actual_version_list = window.actual_version_list;
  this.common_currency_list = window.common_currency_list;
  this.common_user_role = window.common_user_role;
  this.pnl_pbu_list = window.pnl_pbu_list;
  this.pnl_version_list_actual = window.pnl_version_list_actual;
  this.pnl_version_list_budget = window.pnl_version_list_budget;

  /* Actual使用到的API------------------BEGIN------------------ */
  this.overall_actual_pbu_list = window.overall_actual_pbu_list;
  this.overall_actual_version_list = window.overall_actual_version_list;
  /* Actual使用到的API------------------END------------------ */

  /* Overview使用到的API------------------BEGIN------------------ */
  this.overall_overview_pbu_list = window.overall_overview_pbu_list;
  this.overall_overview_version_list_budget = window.overall_overview_version_list_budget;
  this.overall_overview_version_list_actual = window.overall_overview_version_list_actual;
  /* Overview使用到的API------------------END------------------ */

  /* OKR -- start */
  this.GetGmoEmpBasicInfoV2 = window.GetGmoEmpBasicInfoV2;
  this.GetGmoReportObjectiveV2 = window.GetGmoReportObjectiveV2;
  this.GetGmoReportByWeek = window.GetGmoReportByWeek;
  this.GmoUpdateHide = window.GmoUpdateHide;
  this.GetGmoEmpTenantCode = window.GetGmoEmpTenantCode;
  this.GetGmoExportByWeek = window.GetGmoExportByWeek;
  /* OKR -- end */

  /* Performance Summary -- start */
  this.performance_pbu_list = window.performance_pbu_list;
  /* Performance Summary -- end */

  /* pnl reconciliation使用到的API------------------BEGIN------------------ */
  this.pnl_reconciliation_pbu_list = window.pnl_reconciliation_pbu_list;
  this.pnl_reconciliation_version_list_actual = window.pnl_reconciliation_version_list_actual;

  /*  pnl reconciliation使用到的API------------------END------------------ */

    
  callback(this[label].data);
};

var executeDataSources = function ({label, params, body, header}, callback = function () {}) {
  console.log('executeDataSources: ', label, 'body: ', body);
  this.actual_application_history = window.actual_application_history;
  this.actual_cache = window.actual_cache;
  this.actual_publish = window.actual_publish;
  this.actual_recall = window.actual_recall;
  this.actual_store = window.actual_store;

  this.pnl_action_item_save = window.pnl_action_item_save;
  this.pnl_deviation_explanation_save = window.pnl_deviation_explanation_save;
  this.pnl_report = window.pnl_report;

  this.overall_actual_report = window.overall_actual_report;
  this.overall_overview_report = window.overall_overview_report;

  /* OKR -- start */
  this.GmoWeekService = window.GmoWeekService;
  this.GmoLogService = window.GmoLogService;
  this.GmoOrderService = window.GmoOrderService;
  /* OKR -- end */

  /* Performance Summary -- start */
  this.performance_objectives_save = window.performance_objectives_save;
  this.performance_objectives = window.performance_objectives;
  /* Performance Summary -- end */

  /* pnl reconciliation使用到的API------------------BEGIN------------------ */
  this.pnl_reconciliation_expense_breakdown = window.pnl_reconciliation_expense_breakdown;
  this.pnl_reconciliation_set_bu_comparision_status_fail = window.pnl_reconciliation_set_bu_comparision_status_fail;
  this.pnl_reconciliation_set_bu_comparision_status_warning = window.pnl_reconciliation_set_bu_comparision_status_warning;
  this.pnl_reconciliation_mr_expense_save = window.pnl_reconciliation_mr_expense_save;
  this.pnl_reconciliation_report = window.pnl_reconciliation_report;
  /*  pnl reconciliation使用到的API------------------END------------------ */
  

  callback(this[label].data);
};

var parseToJson = function (flies) {
  var d = {
    "results": {
      "管報上傳template": [
        [
          "BU Code",
          "BU Name",
          "Month",
          "Actual Version",
          "Currency",
          "Sales revenue (net)",
          "Gross profit",
          "Operating profit B",
          "ICC",
          "Corporate Allocation",
          "Income tax (incl. Gov't subsidy)",
          "Net income"
        ],
        [
          "BU0000D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9001D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1006",
          "",
          202105,
          120,
          "NTD",
          -1046676664,
          0,
          0,
          -95664.227,
          0,
          -12879403.919,
          12975068.157
        ],
        [
          "BU1007",
          "",
          202105,
          120,
          "NTD",
          3772922336,
          33500032.154,
          23065184.45,
          13707098.951,
          475214.088,
          1968398.783,
          7681257.78
        ],
        [
          "BU1028",
          "",
          202105,
          120,
          "NTD",
          3520224971,
          49898970.238,
          18924480.023,
          12690698.411,
          314077.065,
          1286018.007,
          4658209.927
        ],
        [
          "BU1034",
          "",
          202105,
          120,
          "NTD",
          21916997,
          4450934.683,
          25576.3650000002,
          -85922.42,
          30833.964,
          -258689.936,
          1360744.025
        ],
        [
          "BU9002D",
          "",
          202105,
          120,
          "NTD",
          -0.001,
          -299092.988,
          -299665.516,
          548980.8,
          16272.691,
          2216.625,
          -867139.424
        ],
        [
          "BU1035",
          "",
          202105,
          120,
          "NTD",
          -2470279,
          0,
          0,
          -821.095,
          0,
          128979.14,
          -128158.043
        ],
        [
          "BU1036",
          "",
          202105,
          120,
          "NTD",
          261673549.767,
          3163403.516,
          -19527925.888,
          -89542.657,
          2898925.429,
          132427.406,
          -27263572.599
        ],
        [
          "BU1037",
          "",
          202105,
          120,
          "NTD",
          262644560.673,
          9705519.291,
          -7833280.547,
          277771.374,
          2701135.56,
          114692.844,
          -11495564.529
        ],
        [
          "BU9009D",
          "",
          202105,
          120,
          "NTD",
          407699610.337,
          -12658243.498,
          -55619893.755,
          2453671.86,
          3163315.041,
          -3807859.504,
          -61677340.523
        ],
        [
          "BU1038",
          "",
          202105,
          120,
          "NTD",
          -168865647,
          0,
          0,
          -31048.181,
          0,
          -3278335.32,
          3309383.504
        ],
        [
          "BU1051",
          "",
          202105,
          120,
          "NTD",
          142908877.144,
          98057043.972,
          46540060.264,
          -531131.936,
          914965.791,
          11607742.387,
          39345404.961
        ],
        [
          "BU1055",
          "",
          202105,
          120,
          "NTD",
          264896173.999,
          301713.873,
          -430060.895,
          208530.057,
          244059.936,
          -164499.683,
          -1648557.587
        ],
        [
          "BU9020D",
          "",
          202105,
          120,
          "NTD",
          2475037511.384,
          143915979.442,
          -13781738.507,
          16454298.75,
          6019893.641,
          5921512.639,
          -43470689.139
        ],
        [
          "BU1001",
          "",
          202105,
          120,
          "NTD",
          -548608953,
          0,
          0,
          -432.676,
          0,
          165483.95,
          -165051.27
        ],
        [
          "BU1008",
          "",
          202105,
          120,
          "NTD",
          2925328353.572,
          354584865.54,
          129232812.202,
          19476089.494,
          17126830.613,
          21759265.246,
          81878765.603
        ],
        [
          "BU1024",
          "",
          202105,
          120,
          "NTD",
          4964414677.587,
          47579952.78,
          2374758.612,
          28692306.854,
          2744342.18,
          -2376617.213,
          -26449418.386
        ],
        [
          "BU1033",
          "",
          202105,
          120,
          "NTD",
          550553115.356,
          101413121.913,
          61965321.213,
          -2631167.579,
          3934814.401,
          14089744.584,
          54050189.434
        ],
        [
          "BU1061",
          "",
          202105,
          120,
          "NTD",
          15758946.999,
          4312735.396,
          686992.151999999,
          -253714.484,
          201538.831,
          399577.296,
          -35610.244
        ],
        [
          "BU1096",
          "",
          202105,
          120,
          "NTD",
          2309882398.05,
          279472839.76,
          116738395.088,
          2852448.593,
          6474085.393,
          18745416.978,
          92289946.651
        ],
        [
          "BU1100",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1111",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9021D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1019",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1020",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1022",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1091",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1105",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9004D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1063",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1069_1",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1069_2",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1070",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1102",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1112",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1113",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1115",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9026D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1062",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1066",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1072",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1073",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9024D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1052",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1078",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1083",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1085",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1086",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1089",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1103",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1107",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1108",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1109",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1110",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1116",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9025D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU9029D",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "BU1090",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        [
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ]
      ]
    }
  };

  return Promise.resolve(d);
};

var exportDataByHtml = function (html, {filename, sheetName, style}) {
  // Define your style class template.
  var _style = "<style>" + style + "</style>";
  var uri = 'data:application/vnd.ms-excel;base64,'
    ,
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + _style + '</head><body>{table}</body></html>'
    , base64 = function (s) {
      return window.btoa(unescape(encodeURIComponent(s)))
    }
    , format = function (s, c) {
      return s.replace(/{(\w+)}/g, function (m, p) {
        return c[p];
      })
    }
  var ctx = {worksheet: sheetName || 'Worksheet', table: html};

  var link = document.createElement("a");
  link.download = filename + '.xls';
  link.href = uri + base64(format(template, ctx));

  document.body.appendChild(link);
  link.click();

  // Cleanup the DOM
  document.body.removeChild(link);
  delete link;
};

//以下勿動
var loadScript = function (url, name) {
  return new Promise(function (resolve, reject) {
    var script = document.createElement("script");
    script.type = "text/javascript";

    try {
      if (script.readyState) {  //IE
        script.onreadystatechange = function () {
          if (script.readyState === "loaded" || script.readyState === "complete") {
            script.onreadystatechange = null;
            resolve();
          }
        };
      } else {
        //其餘瀏覽器支援onload
        script.onload = function () {
          console.log(name, ' onload');
          resolve();
        };
      }

      script.src = url;
      document.getElementsByTagName("head")[0].appendChild(script);
    } catch (e) {
      reject(e);
    }
  })
};

var self = {
  ctx: {
    $: $,
    $element: document,
    echarts: echarts,
    file: {parseToJson, exportDataByHtml},
    getDataSources,
    executeDataSources
  },
  onCreated: function () {
  },
  created: function () {
    self.ctx.self = this;
    self.ctx.$router = this.$router;
    window.foreigndata = JSON.stringify({
      "usercode": "cm_dev",
      "accountname": "CM開發測試",
      "sessionid": "446fc705-849a-4afc-971f-b8214b9b240a",
      "userextrainfo": null,
      "ip": "172.16.43.23",
      "insighttoken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjbV9kZXYiLCJzY29wZXMiOlsiVVNFUiJdLCJ1c2VySWQiOiJjZTliMGY1Yy1jMzVmLTQwM2MtOGJjMC0zODUwZDA2YTM4MzEiLCJhY2NvdW50bmFtZSI6IkNN6ZaL55m85ris6KmmIiwiYWNjb3VudCI6ImNtX2RldiIsInVzZXJHcm91cCI6ImJhc2Vncm91cCIsImxvZ291dFRpbWVzIjowLCJuZWVkSXBQZXJtaXNzaW9uIjpmYWxzZSwiaXVncm91cHMiOlsiR01PX0RQTSIsIkNNX0UyRSIsIldlZWtseV9PS1IiLCJjbV9kZXYiXSwiaXNzIjoibWF4aW90LmlvIiwiaWF0IjoxNjI5MTg2MzQ3LCJleHAiOjE2NjA3MjIzNDd9.3iTb6a-KcrgkaAvdBulDE6R5w1yW5Yh4Ls1pYaeZxE4Ryoy-ZJBECalwKY80xDZG2Sk9Fbs6v2oC3ayyxnZ6lw",
      "insightusergroups": [
        "GMO_DPM",
        "CM_E2E",
        "Weekly_OKR",
        "cm_dev",
        "CM_Reconciliation",
        "CM_PL_D"
      ],
      "logoutTimes": 0
    });
    this.foreigndata = window.foreigndata;
    self.data.assets_path = '../';
    self.onCreated();
  },
  mounted: function () {
    // self.onInit();

    Promise.all(
      self.dependent.map(function (dep) {
        return loadScript(self.data.assets_path + dep.url, dep.name);
      })
    )
      .then(function () {
        console.log('all dependent loaded');
        self.onInit();
      })
      .catch(console.error);
  }
};
