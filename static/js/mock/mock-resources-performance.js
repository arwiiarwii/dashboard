
// Input：
// Get http://IP:Port/api/datacapture/v4/performance_objectives/kpi/?params={"user_id":"test","pbu_id":3,"time_id":202106}
var performance_objectives = {
  "msg": null,
  "data": {
    "time_id": 202107,
    "user_id": "cm_dev",
    "user_name": "cm開發",
    "pbu_id": 1,
    "pub_name": "D WH",
    "objectives": [
      {
        "objective_id": 1,
        "objective_description": "PBU Performance Summary",
        "work_note": {
          "202106": {
            "time_id": 202106,
            "note": "1）目標6/30正式上線\n 1.1 協助前後端整合測試，問題排除\n 1.2 ooo \n2）MVP2的優化調整\n"
          },
          "202107": {
            "time_id": 202107,
            "note": "1）Performance Summary開發\n 1.1 確認Performance Summary需求\n 1.2 協助設計跟規劃\n2）MVP2的優化調整\n 2.1 P&L及actual dashboard的科目item_type重新定義，以及前端對應的css stylejavascript調整\n3）<a href='http://www.google.com'>url</a>\n"
          }
        }
      },
      {
        "objective_id": 2,
        "objective_description": "Market and Competiton Analysis",
        "work_note": {
          "202106": {
            "time_id": 202106,
            "note": "1）api規格制定與開發\n2）Pnl/Actual/Overview Dashboard科目屬性重整優化"
          },
          "202107": {
            "time_id": 202107,
            "note": "1）目標6/31正式上線\n 1.1 協助前後端整合測試，問題排除"
          }
        }
      },
      {
        "objective_id": 3,
        "objective_description": "The financial outlook for the year",
        "work_note": {
          "202106": {
            "time_id": 202106,
            "note": "1）api規格制定與開發\n2）Pnl/Actual/Overview Dashboard科目屬性重整優化"
          },
          "202107": {
            "time_id": 202107,
            "note": "1）目標6/32正式上線\n 1.1 協助前後端整合測試，問題排除"
          }
        }
      },
      {
        "objective_id": 4,
        "objective_description": "Priority for the next month",
        "work_note": {
          "202106": {
            "time_id": 202106,
            "note": "1）api規格制定與開發\n2）Pnl/Actual/Overview Dashboard科目屬性重整優化"
          },
          "202107": {
            "time_id": 202107,
            "note": "1）目標6/33正式上線\n 1.1 協助前後端整合測試，問題排除"
          }
        }
      }
    ]
  }
}


var performance_objectives_save = {
  "msg": null,
  "data": {
    "time_id": 202105,
    "user_id": "cm_dev",
    "objectives": {
      "create": {"isFail":false},
      "update": {"isFail":false},
      "delete": {"isFail":false}
    },
    "pbu_id":1
  },
  "result":true
};



// Input：
// Get http://IP:Port/api/datacapture/v4/performance_pbu_list/kpi/?params={"user_id":"test"}
var performance_pbu_list = {
  "msg": null,
  "data": [
    {
      "pbu_id": 1,
      "pbu_name": "D WH"
    },
    {
      "pbu_id": 2,
      "pbu_name": "H WH"
    },
    {
      "pbu_id": 3,
      "pbu_name": "L WH"
    },
    {
      "pbu_id": 4,
      "pbu_name": "Monitor"
    },
    {
      "pbu_id": 5,
      "pbu_name": "HW WH"
    },
    {
      "pbu_id": 6,
      "pbu_name": "Brazil"
    },
    {
      "pbu_id": 7,
      "pbu_name": "G2"
    },
    {
      "pbu_id": 8,
      "pbu_name": "JSD"
    },
    {
      "pbu_id": 9,
      "pbu_name": "FMX"
    },
    {
      "pbu_id": 10,
      "pbu_name": "Czech"
    },
    {
      "pbu_id": 11,
      "pbu_name": "I WH"
    },
    {
      "pbu_id": 12,
      "pbu_name": "GSSD Subtotal"
    },
    {
      "pbu_id": 13,
      "pbu_name": "Cartridge LH"
    },
    {
      "pbu_id": 14,
      "pbu_name": "AP5"
    },
    {
      "pbu_id": 15,
      "pbu_name": "SSBU"
    },
    {
      "pbu_id": 15,
      "pbu_name": "SSBU"
    },
    {
      "pbu_id": 16,
      "pbu_name": "R MX"
    },
    {
      "pbu_id": 17,
      "pbu_name": "Adj-others"
    },
    {
      "pbu_id": 18,
      "pbu_name": "Gold Finger"
    },
    {
      "pbu_id": 19,
      "pbu_name": "WH Gov. Subsidy"
    },
    {
      "pbu_id": 20,
      "pbu_name": "In-scope Consol. adj."
    }
  ]
}





