var common_currency_list = {
  "msg": null,
  "data": [
    {
      "currency_name": "NTD",
      "currency_id": 3
    },
    {
      "currency_name": "USD",
      "currency_id": 4
    }
  ],
  "result": true
};

var common_user_role = {
  "msg": null,
  "data": {
    "user_id": "test",
    "user_name": "testAccount",
    "pbu_id": 1,
    "role_id": 2,
    "role_name": "CM"
    // "role_name": "CM_Admin"
  }
};