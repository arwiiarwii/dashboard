/* Actual使用到的API------------------BEGIN------------------ */
var overall_actual_pbu_list = {
  "msg": null,
  "data": [
    {
      "subpbu_id": 2,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH L5",
      "pbu_id": "1",
      "order": 20
    },
    {
      "subpbu_id": 3,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH L6",
      "pbu_id": "1",
      "order": 30
    },
    {
      "subpbu_id": 4,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH Consol. adj.",
      "pbu_id": "1",
      "order": 40
    },
    {
      "subpbu_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH Subtotal",
      "pbu_id": "1",
      "order": 45
    },
    {
      "subpbu_id": 39,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH for G2",
      "pbu_id": "1",
      "order": 50
    },
    {
      "subpbu_id": 6,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L5",
      "pbu_id": "2",
      "order": 70
    },
    {
      "subpbu_id": 7,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L6",
      "pbu_id": "2",
      "order": 80
    },
    {
      "subpbu_id": 9,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L10",
      "pbu_id": "2",
      "order": 90
    },
    {
      "subpbu_id": 8,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH Consol. adj.",
      "pbu_id": "2",
      "order": 100
    },
    {
      "subpbu_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH Subtotal",
      "pbu_id": "2",
      "order": 105
    },
    {
      "subpbu_id": 11,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L5",
      "pbu_id": "3",
      "order": 120
    },
    {
      "subpbu_id": 12,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L6",
      "pbu_id": "3",
      "order": 130
    },
    {
      "subpbu_id": 13,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L10",
      "pbu_id": "3",
      "order": 140
    },
    {
      "subpbu_id": 14,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH Consol. adj.",
      "pbu_id": "3",
      "order": 150
    },
    {
      "subpbu_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH Subtotal",
      "pbu_id": "3",
      "order": 155
    },
    {
      "subpbu_id": 16,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L5",
      "pbu_id": "4",
      "order": 270
    },
    {
      "subpbu_id": 17,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L6",
      "pbu_id": "4",
      "order": 280
    },
    {
      "subpbu_id": 18,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L10",
      "pbu_id": "4",
      "order": 290
    },
    {
      "subpbu_id": 19,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT Consol. adj.",
      "pbu_id": "4",
      "order": 300
    },
    {
      "subpbu_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "Monitor Subtotal",
      "pbu_id": "4",
      "order": 303
    },
    {
      "subpbu_id": 21,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH L5",
      "pbu_id": "5",
      "order": 170
    },
    {
      "subpbu_id": 22,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH L10",
      "pbu_id": "5",
      "order": 180
    },
    {
      "subpbu_id": 23,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH Consol. adj.",
      "pbu_id": "5",
      "order": 190
    },
    {
      "subpbu_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH Subtotal",
      "pbu_id": "5",
      "order": 195
    },
    {"subpbu_id": 24, "pbu_name": "Brazil", "subpbu_name": "Brazil", "pbu_id": "6", "order": 310},
    {
      "subpbu_id": 25,
      "pbu_name": "G2",
      "subpbu_name": "G2",
      "pbu_id": "7",
      "order": 320
    },
    {"subpbu_id": 26, "pbu_name": "JSD", "subpbu_name": "JSD", "pbu_id": "8", "order": 330},
    {
      "subpbu_id": 27,
      "pbu_name": "FMX",
      "subpbu_name": "FMX",
      "pbu_id": "9",
      "order": 340
    },
    {"subpbu_id": 28, "pbu_name": "Czech", "subpbu_name": "Czech", "pbu_id": "10", "order": 350},
    {
      "subpbu_id": 29,
      "pbu_name": "I WH",
      "subpbu_name": "I WH",
      "pbu_id": "11",
      "order": 360
    },
    {
      "subpbu_id": 30,
      "pbu_name": "GSSD Subtotal",
      "subpbu_name": "GSSD Subtotal",
      "pbu_id": "12",
      "order": 370
    },
    {
      "subpbu_id": 31,
      "pbu_name": "Cartridge LH",
      "subpbu_name": "Cartridge LH",
      "pbu_id": "13",
      "order": 390
    },
    {"subpbu_id": 32, "pbu_name": "AP5", "subpbu_name": "AP5", "pbu_id": "14", "order": 40},
    {
      "subpbu_id": 33,
      "pbu_name": "SSBU",
      "subpbu_name": "S&F4",
      "pbu_id": "15",
      "order": 410
    },
    {"subpbu_id": 40, "pbu_name": "SSBU", "subpbu_name": "Others", "pbu_id": "15", "order": 415},
    {
      "subpbu_id": 34,
      "pbu_name": "R MX",
      "subpbu_name": "R MX",
      "pbu_id": "16",
      "order": 420
    },
    {
      "subpbu_id": 35,
      "pbu_name": "Adj-others",
      "subpbu_name": "Adj-others",
      "pbu_id": "17",
      "order": 430
    },
    {
      "subpbu_id": 36,
      "pbu_name": "Gold Finger",
      "subpbu_name": "Gold Finger",
      "pbu_id": "18",
      "order": 440
    },
    {
      "subpbu_id": 37,
      "pbu_name": "WH Gov. Subsidy",
      "subpbu_name": "WH Gov. Subsidy",
      "pbu_id": "19",
      "order": 200
    },
    {
      "subpbu_id": 38,
      "pbu_name": "In-scope Consol. adj.",
      "subpbu_name": "In-scope Consol. adj.",
      "pbu_id": "20",
      "order": 450
    }],
  "result": true
};
var overall_actual_version_list = {
  "msg": null,
  "data": [
    {
      "version_id": 3,
      "version_name": "110"
    },
    {
      "version_id": 4,
      "version_name": "120"
    },
    {
      "version_id": 5,
      "version_name": "130"
    }
  ],
  "result": true
};
/* Actual使用到的API------------------END------------------ */

/* Overview使用到的API------------------BEGIN------------------ */
var overall_overview_pbu_list = {
  "msg": null,
  "data": [
    {
      "subpbu_id": 2,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH L5",
      "pbu_id": "1",
      "order": 20
    },
    {
      "subpbu_id": 3,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH L6",
      "pbu_id": "1",
      "order": 30
    },
    {
      "subpbu_id": 4,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH Consol. adj.",
      "pbu_id": "1",
      "order": 40
    },
    {
      "subpbu_id": 39,
      "parent_id": 1,
      "pbu_name": "D WH",
      "subpbu_name": "D WH for G2",
      "pbu_id": "1",
      "order": 50
    },
    {
      "subpbu_id": 6,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L5",
      "pbu_id": "2",
      "order": 70
    },
    {
      "subpbu_id": 7,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L6",
      "pbu_id": "2",
      "order": 80
    },
    {
      "subpbu_id": 9,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH L10",
      "pbu_id": "2",
      "order": 90
    },
    {
      "subpbu_id": 8,
      "parent_id": 5,
      "pbu_name": "H WH",
      "subpbu_name": "H WH Consol. adj.",
      "pbu_id": "2",
      "order": 100
    },
    {
      "subpbu_id": 11,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L5",
      "pbu_id": "3",
      "order": 120
    },
    {
      "subpbu_id": 12,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L6",
      "pbu_id": "3",
      "order": 130
    },
    {
      "subpbu_id": 13,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH L10",
      "pbu_id": "3",
      "order": 140
    },
    {
      "subpbu_id": 14,
      "parent_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH Consol. adj.",
      "pbu_id": "3",
      "order": 150
    },
    {
      "subpbu_id": 10,
      "pbu_name": "L WH",
      "subpbu_name": "L WH Subtotal",
      "pbu_id": "3",
      "order": 155
    },
    {
      "subpbu_id": 16,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L5",
      "pbu_id": "4",
      "order": 270
    },
    {
      "subpbu_id": 17,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L6",
      "pbu_id": "4",
      "order": 280
    },
    {
      "subpbu_id": 18,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L10",
      "pbu_id": "4",
      "order": 290
    },
    {
      "subpbu_id": 19,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT Consol. adj.",
      "pbu_id": "4",
      "order": 300
    },
    {
      "subpbu_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "Monitor Subtotal",
      "pbu_id": "4",
      "order": 303
    },
    {
      "subpbu_id": 46,
      "parent_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT Abnormal",
      "pbu_id": "4",
      "order": 305
    },
    {
      "subpbu_id": 21,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH L5",
      "pbu_id": "5",
      "order": 170
    },
    {
      "subpbu_id": 22,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH L10",
      "pbu_id": "5",
      "order": 180
    },
    {
      "subpbu_id": 23,
      "parent_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH Consol. adj.",
      "pbu_id": "5",
      "order": 190
    },
    {
      "subpbu_id": 20,
      "pbu_name": "HW WH",
      "subpbu_name": "HW WH Subtotal",
      "pbu_id": "5",
      "order": 195
    },
    {
      "subpbu_id": 24,
      "pbu_name": "Brazil",
      "subpbu_name": "Brazil",
      "pbu_id": "6",
      "order": 360
    },
    {
      "subpbu_id": 25,
      "pbu_name": "G2",
      "subpbu_name": "G2",
      "pbu_id": "7",
      "order": 370
    },
    {
      "subpbu_id": 26,
      "pbu_name": "JSD",
      "subpbu_name": "JSD",
      "pbu_id": "8",
      "order": 380
    },
    {
      "subpbu_id": 27,
      "pbu_name": "FMX",
      "subpbu_name": "FMX",
      "pbu_id": "9",
      "order": 390
    },
    {
      "subpbu_id": 28,
      "pbu_name": "Czech",
      "subpbu_name": "Czech",
      "pbu_id": "10",
      "order": 400
    },
    {
      "subpbu_id": 29,
      "pbu_name": "I WH",
      "subpbu_name": "I WH",
      "pbu_id": "11",
      "order": 410
    },
    {
      "subpbu_id": 30,
      "pbu_name": "GSSD Subtotal",
      "subpbu_name": "GSSD Subtotal",
      "pbu_id": "12",
      "order": 420
    },
    {
      "subpbu_id": 31,
      "pbu_name": "Cartridge LH",
      "subpbu_name": "Cartridge LH",
      "pbu_id": "13",
      "order": 440
    },
    {
      "subpbu_id": 32,
      "pbu_name": "AP5",
      "subpbu_name": "AP5",
      "pbu_id": "14",
      "order": 450
    },
    {
      "subpbu_id": 47,
      "pbu_name": "AP5",
      "subpbu_name": "Metal Processing",
      "pbu_id": "14",
      "order": 455
    },
    {
      "subpbu_id": 33,
      "pbu_name": "SSBU",
      "subpbu_name": "S&F4",
      "pbu_id": "15",
      "order": 460
    },
    {
      "subpbu_id": 40,
      "pbu_name": "SSBU",
      "subpbu_name": "Others",
      "pbu_id": "15",
      "order": 465
    },
    {
      "subpbu_id": 34,
      "pbu_name": "R MX",
      "subpbu_name": "R MX",
      "pbu_id": "16",
      "order": 470
    },
    {
      "subpbu_id": 35,
      "pbu_name": "Adj-others",
      "subpbu_name": "Adj-others",
      "pbu_id": "17",
      "order": 480
    },
    {
      "subpbu_id": 36,
      "pbu_name": "Gold Finger",
      "subpbu_name": "Gold Finger",
      "pbu_id": "18",
      "order": 490
    },
    {
      "subpbu_id": 37,
      "pbu_name": "WH Gov. Subsidy",
      "subpbu_name": "WH Gov. Subsidy",
      "pbu_id": "19",
      "order": 200
    },
    {
      "subpbu_id": 38,
      "pbu_name": "In-scope Consol. adj.",
      "subpbu_name": "In-scope Consol. adj.",
      "pbu_id": "20",
      "order": 500
    },
    {
      "subpbu_id": 41,
      "pbu_name": "MTC WH",
      "subpbu_name": "MTC WH",
      "pbu_id": "22",
      "order": 210
    },
    {
      "subpbu_id": 42,
      "pbu_name": "Cutting Plant WH",
      "subpbu_name": "Cutting Plant WH",
      "pbu_id": "23",
      "order": 220
    },
    {
      "subpbu_id": 43,
      "pbu_name": "One-time Cost WH",
      "subpbu_name": "WH L5",
      "pbu_id": "24",
      "order": 230
    },
    {
      "subpbu_id": 44,
      "pbu_name": "One-time Cost WH",
      "subpbu_name": "WH L6",
      "pbu_id": "24",
      "order": 240
    },
    {
      "subpbu_id": 45,
      "pbu_name": "One-time Cost WH",
      "subpbu_name": "WH L10",
      "pbu_id": "24",
      "order": 250
    },
    {
      "subpbu_id": 48,
      "parent_id": 51,
      "pbu_name": "Printer",
      "subpbu_name": "PRT L5",
      "pbu_id": "25",
      "order": 310
    },
    {
      "subpbu_id": 49,
      "parent_id": 51,
      "pbu_name": "Printer",
      "subpbu_name": "PRT L6",
      "pbu_id": "25",
      "order": 320
    },
    {
      "subpbu_id": 50,
      "parent_id": 51,
      "pbu_name": "Printer",
      "subpbu_name": "PRT L10",
      "pbu_id": "25",
      "order": 330
    },
    {
      "subpbu_id": 51,
      "pbu_name": "Printer",
      "subpbu_name": "PRT Subtotal",
      "pbu_id": "25",
      "order": 340
    },
    {
      "subpbu_id": 52,
      "parent_id": 51,
      "pbu_name": "Printer",
      "subpbu_name": "PRT Abnormal",
      "pbu_id": "25",
      "order": 350
    }
  ],
  "result": true
};
var overall_overview_version_list_budget = {
  "msg": null,
  "data": [
    {
      "version_id": 1,
      "version_name": "Q1"
    },
    {
      "version_id": 2,
      "version_name": "Q2"
    },
    {
      "version_id": 3,
      "version_name": "Q3"
    },
    {
      "version_id": 4,
      "version_name": "Q4"
    }
  ],
  "result": true
};
var overall_overview_version_list_actual = {
  "msg": null,
  "data": [
    {
      "version_id": 3,
      "version_name": "110"
    },
    {
      "version_id": 4,
      "version_name": "120"
    },
    {
      "version_id": 5,
      "version_name": "130"
    }
  ],
  "result": true
};

var overall_actual_report = {
  "msg": null,
  "data": {
    "subpbu_list": {
      "2": {
        "subpbu_id": 2,
        "subpbu_order": 20,
        "categories": [
          "Jan-21",
          "Feb-21",
          "Mar-21",
          "Apr-21",
          "May-21",
          "Jun-21",
          "Jul-21",
          "FY2021"
        ],
        "pbu_name": "D WH",
        "subpbu_name": "D WH L5",
        "pbu_id": 1
      },
      "3": {
        "subpbu_id": 3,
        "subpbu_order": 30,
        "categories": [
          "Jan-21",
          "Feb-21",
          "Mar-21",
          "Apr-21",
          "May-21",
          "Jun-21",
          "Jul-21",
          "FY2021"
        ],
        "pbu_name": "D WH",
        "subpbu_name": "D WH L6",
        "pbu_id": 1
      },
      "4": {
        "subpbu_id": 4,
        "subpbu_order": 40,
        "categories": [
          "Jan-21",
          "Feb-21",
          "Mar-21",
          "Apr-21",
          "May-21",
          "Jun-21",
          "Jul-21",
          "FY2021"
        ],
        "pbu_name": "D WH",
        "subpbu_name": "D WH Consol. adj.",
        "pbu_id": 1
      }
    },
    "currency_name": "NTD",
    "actual_version": "Current Month:110",
    "time_id_e": 202107,
    "time_id_s": 202101,
    "items": [
      {
        "line_item": "0010000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Volume",
        "item_order": 100
      },
      {
        "item_flag": "positive",
        "line_item": "0020000",
        "item_level": 1,
        "item_type": "QS",
        "values": {
          "2": {
            "Apr-21": 684.446,
            "FY2021": 4159.952,
            "Jun-21": 996.425,
            "May-21": 751.022,
            "Jan-21": 752.756,
            "Feb-21": 372.257,
            "Mar-21": 603.046
          },
          "3": {
            "Apr-21": 382.915,
            "FY2021": 1824.653,
            "May-21": 443.305,
            "Jan-21": 517.562,
            "Feb-21": 207.418,
            "Mar-21": 273.453
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Production volume (K PCS)",
        "item_order": 200
      },
      {
        "item_flag": "positive",
        "line_item": "0030000",
        "item_level": 1,
        "item_type": "QS",
        "values": {
          "2": {
            "Apr-21": 592.434,
            "FY2021": 3732.84231,
            "Jun-21": 859.443,
            "May-21": 658.691,
            "Jan-21": 670.00731,
            "Feb-21": 366.263,
            "Mar-21": 586.004
          },
          "3": {
            "Apr-21": 231.074195913909,
            "FY2021": 1089.579684215452,
            "May-21": 264.516708301543,
            "Jan-21": 284.44897,
            "Feb-21": 131.80796,
            "Mar-21": 177.73185
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Normalized production volume (K PCS)",
        "item_order": 300
      },
      {
        "item_flag": "positive",
        "line_item": "0040000",
        "item_level": 1,
        "item_type": "QS",
        "values": {
          "2": {
            "Apr-21": 775.868000000002,
            "FY2021": 4309.111000000002,
            "Jun-21": 789.79,
            "May-21": 805.566,
            "Jan-21": 659.214,
            "Feb-21": 529.682,
            "Mar-21": 748.991
          },
          "3": {
            "Apr-21": 346.517,
            "FY2021": 1710.483,
            "May-21": 372.413,
            "Jan-21": 472.773,
            "Feb-21": 211.314,
            "Mar-21": 307.466
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Sales volume (K PCS)",
        "item_order": 400
      },
      {
        "item_flag": "positive",
        "line_item": "0050000",
        "item_level": 1,
        "item_type": "QS",
        "values": {
          "2": {
            "Apr-21": 8752.255000000003,
            "Jun-21": 9082.811000000003,
            "May-21": 8931.621000000003,
            "Jan-21": 8572.514,
            "Feb-21": 8564.696,
            "Mar-21": 8650.987
          },
          "3": {
            "Apr-21": 4546.57,
            "May-21": 4513.783,
            "Jan-21": 4687.073,
            "Feb-21": 4654.887,
            "Mar-21": 4605.653
          },
          "4": {
            "Apr-21": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Current FY sales vol (latest forecast vs. budget, K PCS)",
        "item_order": 500
      },
      {
        "line_item": "0060000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Production cost",
        "item_order": 600
      },
      {
        "item_flag": "negative",
        "line_item": "0070100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "COGS (production)",
        "item_order": 800
      },
      {
        "line_item": "0070101",
        "item_level": 1,
        "item_type": "C",
        "item_name": "BOM COGS",
        "item_order": 900
      },
      {
        "item_flag": "negative",
        "line_item": "0080100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": 3051155846.1128983,
            "Jun-21": 1500768430.0410583,
            "May-21": 1550387416.07184
          },
          "3": {
            "FY2021": 698657928.6877146,
            "May-21": 698657928.6877146
          },
          "4": {}
        },
        "item_name": "BOM GR",
        "item_order": 1200
      },
      {
        "item_flag": "negative",
        "line_item": "0080101",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Buy-sell",
        "item_order": 1300
      },
      {
        "item_flag": "negative",
        "line_item": "0080102",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "GTK",
        "item_order": 1400
      },
      {
        "item_flag": "negative",
        "line_item": "0080103",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "TK",
        "item_order": 1500
      },
      {
        "item_flag": "negative",
        "line_item": "0080104",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "BoM Savings",
        "item_order": 1600
      },
      {
        "item_flag": "negative",
        "line_item": "0080105",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": 22265184.506829012,
            "Jun-21": 22265184.506829012
          },
          "3": {},
          "4": {}
        },
        "item_name": "BoM Savings - Proc. targets",
        "item_order": 1700
      },
      {
        "item_flag": "negative",
        "line_item": "0080106",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "BoM Savings - R&D targets",
        "item_order": 1800
      },
      {
        "item_flag": "negative",
        "line_item": "0080107",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": -10644262.697654843,
            "FY2021": -38511135.8563097,
            "Jun-21": -15805095.989097333,
            "May-21": -12061777.169557523
          },
          "3": {
            "Apr-21": -227658.2952331622,
            "FY2021": -484770.68040506984,
            "May-21": -257112.3851719076
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Scrap sales",
        "item_order": 1900
      },
      {
        "line_item": "0280000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "MOH/MVA",
        "item_order": 2000
      },
      {
        "item_flag": "negative",
        "line_item": "0090100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 81553079.1628374,
            "FY2021": 536928218.6614935,
            "Jun-21": 94306244.0087622,
            "May-21": 96554103.83574387,
            "Jan-21": 96179979.75238,
            "Feb-21": 82051061.51048,
            "Mar-21": 86283750.39129001
          },
          "3": {
            "Apr-21": 33112850.047938593,
            "FY2021": 173648829.45758328,
            "May-21": 53451454.539234705,
            "Jan-21": 35264570.83021,
            "Feb-21": 23414390.88261,
            "Mar-21": 28405563.15759
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "MOH/MVA expense",
        "item_order": 2100
      },
      {
        "item_flag": "negative",
        "line_item": "0090101",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 43090645.52533701,
            "FY2021": 286547881.33996284,
            "Jun-21": 58210133.62173383,
            "May-21": 52128806.010751985,
            "Jan-21": 51421296.35172,
            "Feb-21": 36119817.69587,
            "Mar-21": 45577182.134550005
          },
          "3": {
            "Apr-21": 13240183.15835564,
            "FY2021": 68043074.83410315,
            "May-21": 16983353.36078752,
            "Jan-21": 16265685.12557,
            "Feb-21": 8636087.90864,
            "Mar-21": 12917765.280749999
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "DL",
        "item_order": 2200
      },
      {
        "item_flag": "negative",
        "line_item": "0090102",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 6299908.843168999,
            "FY2021": 42207804.39155985,
            "Jun-21": 6856064.418522852,
            "May-21": 7215928.433087999,
            "Jan-21": 6805219.55553,
            "Feb-21": 8553394.87356,
            "Mar-21": 6477288.26769
          },
          "3": {
            "Apr-21": 1384715.449030303,
            "FY2021": 7450896.3108238345,
            "May-21": 1578152.7666235312,
            "Jan-21": 1504679.20294,
            "Feb-21": 1894815.13552,
            "Mar-21": 1088533.75671
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "IDL",
        "item_order": 2300
      },
      {
        "item_flag": "negative",
        "line_item": "0090103",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 32162524.794331394,
            "FY2021": 218839997.7243273,
            "Jun-21": 37527306.66654203,
            "May-21": 39589573.48822388,
            "Jan-21": 37953463.845130004,
            "Feb-21": 37377848.94105,
            "Mar-21": 34229279.98905
          },
          "3": {
            "Apr-21": 18487951.44055265,
            "FY2021": 102905681.91872239,
            "May-21": 39640771.73999975,
            "Jan-21": 17494206.501700003,
            "Feb-21": 12883487.83845,
            "Mar-21": 14399264.39802
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "G&A",
        "item_order": 2400
      },
      {
        "item_flag": "negative",
        "line_item": "0090104",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Government Subsidy",
        "item_order": 2500
      },
      {
        "item_flag": "negative",
        "line_item": "0090105",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "MFG Savings",
        "item_order": 2600
      },
      {
        "item_flag": "negative",
        "line_item": "0090106",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "MFG Savings - targets",
        "item_order": 2700
      },
      {
        "item_flag": "negative",
        "line_item": "0090109",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "MFG Savings - DL",
        "item_order": 2706
      },
      {
        "item_flag": "negative",
        "line_item": "0090110",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "MFG Savings - IDL",
        "item_order": 2712
      },
      {
        "item_flag": "negative",
        "line_item": "0090111",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "MFG Savings - G&A",
        "item_order": 2718
      },
      {
        "item_flag": "negative",
        "line_item": "0090112",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "MFG Savings - Others",
        "item_order": 2724
      },
      {
        "item_flag": "negative",
        "line_item": "0090107",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "MFG Savings - Government subsidy",
        "item_order": 2730
      },
      {
        "item_flag": "negative",
        "line_item": "0090108",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "Inventory write-off reversal",
        "item_order": 2760
      },
      {
        "line_item": "0100000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Per unit cost",
        "item_order": 2800
      },
      {
        "item_flag": "positive",
        "line_item": "0100100",
        "item_level": 2,
        "item_type": "PEU",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Margin on BOM%",
        "item_order": 2900
      },
      {
        "item_flag": "negative",
        "line_item": "0100200",
        "item_level": 2,
        "item_type": "NS",
        "values": {
          "2": {
            "Apr-21": 137.65766171900566,
            "FY2021": 143.83897686304715,
            "Jun-21": 109.72949225110007,
            "May-21": 146.58482328700995,
            "Jan-21": 143.55064238385697,
            "Feb-21": 224.0222504333771,
            "Mar-21": 147.2408898084143
          },
          "3": {
            "Apr-21": 143.29964415531452,
            "FY2021": 159.3723083985533,
            "May-21": 202.07212951667788,
            "Jan-21": 123.97503436278924,
            "Feb-21": 177.64018867001658,
            "Mar-21": 159.82258192659336
          },
          "4": {}
        },
        "item_name": "Total MVA / N. production vol",
        "item_order": 3000
      },
      {
        "item_flag": "negative",
        "line_item": "0100300",
        "item_level": 2,
        "item_type": "NS",
        "values": {
          "2": {
            "Apr-21": 72.73493000965004,
            "FY2021": 76.76399310314365,
            "Jun-21": 67.73006891874601,
            "May-21": 79.14000041104552,
            "Jan-21": 76.74736616190053,
            "Feb-21": 98.61716224644586,
            "Mar-21": 77.77623042598687
          },
          "3": {
            "Apr-21": 57.298406280243064,
            "FY2021": 62.44892027616808,
            "May-21": 64.20521966206718,
            "Jan-21": 57.18313947689809,
            "Feb-21": 65.520230406722,
            "Mar-21": 72.68120643964488
          },
          "4": {}
        },
        "item_name": "DL MVA / N. production vol",
        "item_order": 3100
      },
      {
        "line_item": "0110000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "P&L",
        "item_order": 3200
      },
      {
        "item_flag": "positive",
        "line_item": "0120000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 2242071707.9417977,
            "FY2021": 11437119777.488955,
            "Jun-21": 1870480059.1188176,
            "May-21": 1887067822.2037802,
            "Jan-21": 1748671846.2950099,
            "Feb-21": 1454674083.66723,
            "Mar-21": 2234154258.26232
          },
          "3": {
            "Apr-21": 850034393.1111363,
            "FY2021": 4230918684.0767307,
            "May-21": 894894619.9553444,
            "Jan-21": 1159861999.9091802,
            "Feb-21": 538646999.88744,
            "Mar-21": 787480671.2136301
          },
          "4": {
            "Apr-21": -769066164.309818,
            "FY2021": -4558540424.914884,
            "Jun-21": -766989251.72789,
            "May-21": -731880877.157836,
            "Jan-21": -1054923846.4069799,
            "Feb-21": -486423728.73811,
            "Mar-21": -749256556.5742501
          }
        },
        "item_name": "Sales revenue (net)",
        "item_order": 3300
      },
      {
        "item_flag": "positive",
        "line_item": "0120100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 2035135942.5513113,
            "FY2021": 10549870999.39169,
            "Jun-21": 1763993698.2609575,
            "May-21": 1778440881.52281,
            "Jan-21": 1596114733.94924,
            "Feb-21": 1386902999.93078,
            "Mar-21": 1989282743.1765902
          },
          "3": {
            "Apr-21": 838995311.0845183,
            "FY2021": 4216000287.9401226,
            "May-21": 894894619.9553444,
            "Jan-21": 1159861999.9091802,
            "Feb-21": 538646999.88744,
            "Mar-21": 783601357.10364
          },
          "4": {
            "Apr-21": -769066164.309818,
            "FY2021": -4558540424.914884,
            "Jun-21": -766989251.72789,
            "May-21": -731880877.157836,
            "Jan-21": -1054923846.4069799,
            "Feb-21": -486423728.73811,
            "Mar-21": -749256556.5742501
          }
        },
        "item_name": "Sales revenue",
        "item_order": 3400
      },
      {
        "item_flag": "positive",
        "line_item": "0120200",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 396087.8447220003,
            "FY2021": 438285600.1348617,
            "Jun-21": 0,
            "May-21": 560435.4803396821,
            "Jan-21": 152557112.34577,
            "Feb-21": 67771083.45626,
            "Mar-21": 217000881.00777
          },
          "3": {
            "Apr-21": 4492293.668037191,
            "FY2021": 8371607.778027191,
            "May-21": 0,
            "Mar-21": 3879314.10999
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "NRE income",
        "item_order": 3500
      },
      {
        "item_flag": "positive",
        "line_item": "0120300",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 86901873.85144997,
            "FY2021": 273325373.7100105,
            "Jun-21": 106486360.85786,
            "May-21": 52066505.20063052,
            "Mar-21": 27870633.80007
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Customer claim",
        "item_order": 3530
      },
      {
        "item_flag": "positive",
        "line_item": "0120500",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": 56000000.00000005,
            "May-21": 56000000.00000005
          },
          "3": {},
          "4": {}
        },
        "item_name": "KAM-target",
        "item_order": 3545
      },
      {
        "item_flag": "positive",
        "line_item": "0120600",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": -1263729.95585781,
            "Jun-21": 301725.35646218044,
            "May-21": -1565455.3123199905
          },
          "3": {
            "FY2021": -2268960.4044250157,
            "May-21": -2268960.4044250157
          },
          "4": {}
        },
        "item_name": "Revenue-resell aging stock",
        "item_order": 3552
      },
      {
        "item_flag": "positive",
        "line_item": "0120400",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 119637803.69431458,
            "FY2021": 119637803.69431458,
            "Jun-21": 0,
            "May-21": 0
          },
          "3": {
            "Apr-21": 6546788.358580833,
            "FY2021": 6546788.358580833,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Shared revenue",
        "item_order": 3560
      },
      {
        "item_flag": "negative",
        "line_item": "0130000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 1982115027.2694454,
            "FY2021": 10117079934.73192,
            "Jun-21": 1595074674.0498207,
            "May-21": 1646941519.9075844,
            "Jan-21": 1546567285.53122,
            "Feb-21": 1328997146.63839,
            "Mar-21": 2017384281.33546
          },
          "3": {
            "Apr-21": 726337682.3541158,
            "FY2021": 3657330232.489025,
            "May-21": 752109383.2269493,
            "Jan-21": 1041356172.6157401,
            "Feb-21": 465776582.55535,
            "Mar-21": 671750411.73687
          },
          "4": {
            "Apr-21": -769066164.309818,
            "FY2021": -4558540424.914884,
            "Jun-21": -766989251.72789,
            "May-21": -731880877.157836,
            "Jan-21": -1054923846.4069799,
            "Feb-21": -486423728.73811,
            "Mar-21": -749256556.5742501
          }
        },
        "item_name": "COGS (net)",
        "item_order": 3600
      },
      {
        "item_flag": "negative",
        "line_item": "0130100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 1882017842.5638967,
            "FY2021": 5151900909.679956,
            "Jun-21": 1610879770.038918,
            "May-21": 1659003297.077142
          },
          "3": {
            "Apr-21": 723764051.098949,
            "FY2021": 1476130546.71107,
            "May-21": 752366495.6121212
          },
          "4": {
            "Apr-21": -769066164.309818,
            "FY2021": -2267936293.1955442,
            "Jun-21": -766989251.72789,
            "May-21": -731880877.157836
          }
        },
        "item_name": "COGS",
        "item_order": 3630
      },
      {
        "item_flag": "negative",
        "line_item": "0130300",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": -607022.2721728523,
            "Jun-21": -567213.4721728523,
            "May-21": -39808.799999999996
          },
          "3": {
            "FY2021": -20732.202844621148,
            "May-21": -20732.202844621148
          },
          "4": {}
        },
        "item_name": "Inventory Valuation Loss or Gain",
        "item_order": 3637
      },
      {
        "item_flag": "negative",
        "line_item": "0130400",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": -8796712.566325825,
            "Jun-21": -8021772.582325835,
            "May-21": -774939.9839999909
          },
          "3": {
            "FY2021": -2461130.7209064513,
            "May-21": -2461130.7209064513
          },
          "4": {}
        },
        "item_name": "Loss on Inventory Obsolescence",
        "item_order": 3645
      },
      {
        "item_flag": "negative",
        "line_item": "0130500",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "Scrap sales",
        "item_order": 3652
      },
      {
        "item_flag": "negative",
        "line_item": "0130200",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 110741447.40320337,
            "FY2021": 110741447.40320337,
            "Jun-21": 0,
            "May-21": 0
          },
          "3": {
            "Apr-21": 2801289.550399999,
            "FY2021": 2801289.550399999,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Shared COGS",
        "item_order": 3660
      },
      {
        "item_flag": "positive",
        "line_item": "0140000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 259956680.6723523,
            "FY2021": 1320039842.7570348,
            "Jun-21": 275405385.0689969,
            "May-21": 240126302.29619575,
            "Jan-21": 202104560.76378998,
            "Feb-21": 125676937.02883999,
            "Mar-21": 216769976.92686
          },
          "3": {
            "Apr-21": 123696710.75702047,
            "FY2021": 573588451.8678956,
            "May-21": 142785236.7283951,
            "Jan-21": 118505827.29344,
            "Feb-21": 72870417.61228001,
            "Mar-21": 115730259.47676001
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Gross profit",
        "item_order": 3700
      },
      {
        "item_flag": "positive",
        "line_item": "0210000",
        "item_level": 2,
        "item_type": "PEU",
        "values": {
          "2": {
            "Apr-21": 0.11594485571159108,
            "FY2021": 0.11541715645535125,
            "Jun-21": 0.14723780867182315,
            "May-21": 0.12724836885606386,
            "Jan-21": 0.11557603628833966,
            "Feb-21": 0.08639525405718973,
            "Mar-21": 0.09702551922061967
          },
          "3": {
            "Apr-21": 0.1455196539804572,
            "FY2021": 0.1355706631816451,
            "May-21": 0.15955536388800745,
            "Jan-21": 0.10217235093719708,
            "Feb-21": 0.13528417985713762,
            "Mar-21": 0.14696266677682604
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Gross profit %",
        "item_order": 3750
      },
      {
        "item_flag": "negative",
        "line_item": "0150000",
        "item_level": 1,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 188437308.00086296,
            "FY2021": 868681812.2295842,
            "Jun-21": 201555107.86146328,
            "May-21": 142565180.20980802,
            "Jan-21": 104889708.93576999,
            "Feb-21": 92383002.7564,
            "Mar-21": 138851504.46528
          },
          "3": {
            "Apr-21": 80906008.65200348,
            "FY2021": 355629344.589215,
            "May-21": 78844340.6961215,
            "Jan-21": 60908432.71804,
            "Feb-21": 55660434.44854,
            "Mar-21": 79310128.07451001
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D, SG&A expenses",
        "item_order": 3800
      },
      {
        "item_flag": "negative",
        "line_item": "0150100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 72096050.55284871,
            "FY2021": 380051908.93348086,
            "Jun-21": 58859738.86837626,
            "May-21": 60620758.321055874,
            "Jan-21": 58860745.554670006,
            "Feb-21": 46610018.09911,
            "Mar-21": 83004597.53742
          },
          "3": {
            "Apr-21": 70549286.27995975,
            "FY2021": 274128729.8593867,
            "May-21": 52135781.33285688,
            "Jan-21": 50435631.67606,
            "Feb-21": 38122057.3972,
            "Mar-21": 62885973.173310004
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses",
        "item_order": 3900
      },
      {
        "item_flag": "negative",
        "line_item": "0150101",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 24394420.806661047,
            "FY2021": 149125332.99256182,
            "Jun-21": 24014830.547710627,
            "May-21": 28930022.205370154,
            "Jan-21": 21705171.886439998,
            "Feb-21": 20511048.933679998,
            "Mar-21": 29569838.6127
          },
          "3": {
            "Apr-21": 16665245.72334284,
            "FY2021": 82277113.50606197,
            "May-21": 15626053.495239142,
            "Jan-21": 16705669.25416,
            "Feb-21": 15185155.66537,
            "Mar-21": 18094989.36795
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses - FTE/Non-FTE",
        "item_order": 4000
      },
      {
        "item_flag": "negative",
        "line_item": "0150102",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 36542385.81058875,
            "FY2021": 137925982.82307133,
            "Jun-21": 21263720.701557644,
            "May-21": 17074732.63517495,
            "Jan-21": 20061525.76441,
            "Feb-21": 10371162.52231,
            "Mar-21": 32612455.389030002
          },
          "3": {
            "Apr-21": 36542385.81058875,
            "FY2021": 116662262.1215137,
            "May-21": 17074732.63517495,
            "Jan-21": 20061525.76441,
            "Feb-21": 10371162.52231,
            "Mar-21": 32612455.389030002
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses - Project",
        "item_order": 4100
      },
      {
        "item_flag": "negative",
        "line_item": "0150103",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": 0,
            "Jun-21": 0
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {}
        },
        "item_name": "R&D Expenses - Project exp/NRE reclassification",
        "item_order": 4200
      },
      {
        "item_flag": "negative",
        "line_item": "0150104",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 2636280.414722,
            "FY2021": 24757352.498170014,
            "Jun-21": 6186283.7610180145,
            "May-21": 6119246.03808,
            "Jan-21": 3676299.42361,
            "Feb-21": 2919587.64532,
            "Mar-21": 3219655.2154200003
          },
          "3": {
            "Apr-21": 9737358.289488101,
            "FY2021": 41486946.72540806,
            "May-21": 12466248.693429962,
            "Jan-21": 7102503.51664,
            "Feb-21": 4842229.5705,
            "Mar-21": 7338606.65535
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses - MFG platform",
        "item_order": 4300
      },
      {
        "item_flag": "negative",
        "line_item": "0150108",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 7038483.930726521,
            "FY2021": 21048913.201581772,
            "Jun-21": 7262472.990399924,
            "May-21": 6747956.2804553285
          },
          "3": {
            "Apr-21": 7038483.930726521,
            "FY2021": 13786440.21118185,
            "May-21": 6747956.280455328
          },
          "4": {}
        },
        "item_name": "R&D Expenses - PBU",
        "item_order": 4400
      },
      {
        "item_flag": "negative",
        "line_item": "0150106",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0,
            "FY2021": 21922491.37618,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 7059023.128169999,
            "Feb-21": 6355502.417889999,
            "Mar-21": 8507965.83012
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 21922491.37618,
            "May-21": 0,
            "Jan-21": 7059023.128169999,
            "Feb-21": 6355502.417889999,
            "Mar-21": 8507965.83012
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses - FTE/Non-FTE - targets",
        "item_order": 4500
      },
      {
        "item_flag": "negative",
        "line_item": "0150107",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expenses - Project - targets",
        "item_order": 4600
      },
      {
        "item_flag": "negative",
        "line_item": "0150109",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": -10644262.697654843,
            "FY2021": -38511135.8563097,
            "Jun-21": -15805095.989097333,
            "May-21": -12061777.169557523
          },
          "3": {
            "Apr-21": -227658.2952331622,
            "FY2021": -484770.68040506984,
            "May-21": -257112.3851719076
          },
          "4": {}
        },
        "item_name": "R&D capitalization",
        "item_order": 4625
      },
      {
        "item_flag": "negative",
        "line_item": "0150105",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 1484479.5901503973,
            "FY2021": 25271835.761725895,
            "Jun-21": 132430.86769005284,
            "May-21": 1748801.1619754434,
            "Jan-21": 6358725.35204,
            "Feb-21": 6452716.29972,
            "Mar-21": 9094682.490149999
          },
          "3": {
            "Apr-21": 565812.5258135479,
            "FY2021": -2006524.0801789504,
            "May-21": 220790.22855750192,
            "Jan-21": -493089.70635,
            "Feb-21": 1368006.9409400001,
            "Mar-21": -3668044.0691400003
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "R&D Expense - Shared expenses",
        "item_order": 4650
      },
      {
        "item_flag": "negative",
        "line_item": "0150200",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 108978922.61560266,
            "FY2021": 429921315.0372369,
            "Jun-21": 132037818.23284322,
            "May-21": 70172672.31423105,
            "Jan-21": 37617620.685100004,
            "Feb-21": 34381939.78125,
            "Mar-21": 46732341.40821
          },
          "3": {
            "Apr-21": 9027860.377502844,
            "FY2021": 62220409.213424385,
            "May-21": 23644842.70356155,
            "Jan-21": 6334524.28206,
            "Feb-21": 9725202.129279999,
            "Mar-21": 13487979.72102
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Sales expense ",
        "item_order": 4700
      },
      {
        "item_flag": "negative",
        "line_item": "0150201",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 109789293.09306711,
            "FY2021": 433968264.4263912,
            "Jun-21": 131961545.50642864,
            "May-21": 70109800.06114541,
            "Jan-21": 37918104.89451,
            "Feb-21": 35548516.730239995,
            "Mar-21": 48641004.140999995
          },
          "3": {
            "Apr-21": 10552203.428874688,
            "FY2021": 55307269.99052644,
            "May-21": 24766606.480261747,
            "Jan-21": 6922743.68713,
            "Feb-21": 6011281.99814,
            "Mar-21": 7054434.39612
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Sales expenses - MFG platform",
        "item_order": 4800
      },
      {
        "item_flag": "negative",
        "line_item": "0150202",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Sales expenses - PBU",
        "item_order": 4900
      },
      {
        "item_flag": "negative",
        "line_item": "0150204",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          },
          "3": {
            "Apr-21": 0,
            "FY2021": 0,
            "May-21": 0
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Bad debt expense",
        "item_order": 4950
      },
      {
        "item_flag": "negative",
        "line_item": "0150203",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": -810370.4774644524,
            "FY2021": -4046948.831074233,
            "Jun-21": 76272.72641457617,
            "May-21": 62872.25308564305,
            "Jan-21": -300484.20941,
            "Feb-21": -1166576.6687999999,
            "Mar-21": -1908662.4549
          },
          "3": {
            "Apr-21": -1524343.0513718445,
            "FY2021": 6913139.503087958,
            "May-21": -1121763.7767001987,
            "Jan-21": -588219.40507,
            "Feb-21": 3713920.41133,
            "Mar-21": 6433545.3249
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Sales expenses - Shared expenses",
        "item_order": 5000
      },
      {
        "item_flag": "negative",
        "line_item": "0150300",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 7362334.832411592,
            "FY2021": 58708588.539836496,
            "Jun-21": 10657550.760243816,
            "May-21": 11771749.574521089,
            "Jan-21": 8411342.97697,
            "Feb-21": 11391044.87604,
            "Mar-21": 9114565.51965
          },
          "3": {
            "Apr-21": 1328861.9945408902,
            "FY2021": 19280205.516403973,
            "May-21": 3063716.6597030815,
            "Jan-21": 4138276.75992,
            "Feb-21": 7813174.92206,
            "Mar-21": 2936175.18018
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Administrative expense",
        "item_order": 5100
      },
      {
        "item_flag": "negative",
        "line_item": "0150301",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 3067792.2667630874,
            "FY2021": 16175432.647210762,
            "Jun-21": 3283422.8340301183,
            "May-21": 4323678.124207557,
            "Jan-21": 2574683.76738,
            "Feb-21": 2640866.12111,
            "Mar-21": 284989.53372
          },
          "3": {
            "Apr-21": -2240865.930741331,
            "FY2021": -819690.5337516423,
            "May-21": -773806.0123703107,
            "Jan-21": -2238467.47919,
            "Feb-21": 5719999.556229999,
            "Mar-21": -1286550.66768
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Administrative expenses - MFG platform",
        "item_order": 5200
      },
      {
        "item_flag": "negative",
        "line_item": "0150302",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 830470.3130602894,
            "FY2021": 5112364.41787989,
            "Jun-21": 855159.3818273755,
            "May-21": 836380.3095822247,
            "Jan-21": 615252.09071,
            "Feb-21": 1229438.6962499998,
            "Mar-21": 745663.62645
          },
          "3": {
            "Apr-21": 786304.5716165964,
            "FY2021": 4161369.090968636,
            "May-21": 705640.0007020395,
            "Jan-21": 810528.2075,
            "Feb-21": 1192278.21731,
            "Mar-21": 666618.09384
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Administrative expenses - PBU",
        "item_order": 5300
      },
      {
        "item_flag": "negative",
        "line_item": "0150303",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 3464072.2525882153,
            "FY2021": 37420791.19377584,
            "Jun-21": 6518968.544386323,
            "May-21": 6611691.140731307,
            "Jan-21": 5221406.837909999,
            "Feb-21": 7520740.058680001,
            "Mar-21": 8083912.35948
          },
          "3": {
            "Apr-21": 2783423.3536656247,
            "FY2021": 15938526.678216975,
            "May-21": 3131882.671371353,
            "Jan-21": 5566215.75064,
            "Feb-21": 900897.14852,
            "Mar-21": 3556107.75402
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Administrative expenses - Shared expenses",
        "item_order": 5400
      },
      {
        "line_item": "0290000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 0.115944855712,
            "FY2021": 0.39043103324,
            "Jun-21": 0.147237808672,
            "May-21": 0.127248368856
          },
          "3": {
            "Apr-21": 0.14551965398,
            "FY2021": 0.305075017868,
            "May-21": 0.159555363888
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Over-delivery & leakage to be recovered",
        "item_order": 5412
      },
      {
        "line_item": "0290100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0.029776566695,
            "FY2021": 0.129018901253,
            "Jun-21": 0.043078753475,
            "May-21": 0.056163581083
          },
          "3": {
            "Apr-21": 0.048080520572,
            "FY2021": 0.12202420475600001,
            "May-21": 0.073943684184
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Q1 over-delivery",
        "item_order": 5425
      },
      {
        "line_item": "0290200",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0.026049485316,
            "FY2021": 0.10813848987,
            "Jun-21": 0.035264834688,
            "May-21": 0.046824169866
          },
          "3": {
            "Apr-21": 0.050504106201,
            "FY2021": 0.117482955719,
            "May-21": 0.066978849518
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Q1 Leakage to be recovered",
        "item_order": 5437
      },
      {
        "item_flag": "positive",
        "line_item": "0250000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 66761197.7456523,
            "FY2021": 501107352.08365697,
            "Jun-21": 80577949.34602459,
            "May-21": 105984486.64218011,
            "Jan-21": 108494500.08952999,
            "Feb-21": 46100813.68185,
            "Mar-21": 93188404.57842
          },
          "3": {
            "Apr-21": 40870096.12494349,
            "FY2021": 235058750.01354575,
            "May-21": 66171805.15550227,
            "Jan-21": 62082301.214619994,
            "Feb-21": 23192807.384340003,
            "Mar-21": 42741740.13414
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Operating profit A - (excl. shared rev / exp)",
        "item_order": 5450
      },
      {
        "item_flag": "positive",
        "line_item": "0220000",
        "item_level": 2,
        "item_type": "PEU",
        "values": {
          "2": {
            "Apr-21": 0.029776566694621243,
            "FY2021": 0.043814121197712615,
            "Jun-21": 0.04307875347464801,
            "May-21": 0.05616358108338042,
            "Jan-21": 0.06204394513436137,
            "Feb-21": 0.03169150684642017,
            "Mar-21": 0.04171081931061468
          },
          "3": {
            "Apr-21": 0.048080520572065834,
            "FY2021": 0.055557378329723246,
            "May-21": 0.07394368418351231,
            "Jan-21": 0.053525592889051614,
            "Feb-21": 0.04305752633763218,
            "Mar-21": 0.05427655775762513
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Operating profit A %",
        "item_order": 5467
      },
      {
        "line_item": "0270000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "PBU Uncontrollable Accounts",
        "item_order": 5484
      },
      {
        "item_flag": "positive",
        "line_item": "0160000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 71519372.67148936,
            "FY2021": 451358030.2495607,
            "Jun-21": 73850277.20753363,
            "May-21": 97561122.08638772,
            "Jan-21": 97214851.82801999,
            "Feb-21": 33293934.27244,
            "Mar-21": 77918472.18369
          },
          "3": {
            "Apr-21": 42790702.10501699,
            "FY2021": 217959106.99849057,
            "May-21": 63940896.032273605,
            "Jan-21": 57597394.575399995,
            "Feb-21": 17209982.88355,
            "Mar-21": 36420131.40225
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Operating profit B - (incl. shared rev / exp)",
        "item_order": 5500
      },
      {
        "item_flag": "negative",
        "line_item": "0160100",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": -10880837.276055805,
            "FY2021": -51548466.19417884,
            "Jun-21": -10464971.01118958,
            "May-21": -11071353.853453457,
            "Jan-21": 1976024.0790499998,
            "Feb-21": -10767237.144979998,
            "Mar-21": -10340090.98755
          },
          "3": {
            "Apr-21": -1140548.911579836,
            "FY2021": -5977229.448085401,
            "May-21": -1252170.3160755658,
            "Jan-21": -1391106.35245,
            "Feb-21": -1157541.3818700002,
            "Mar-21": -1035862.48611
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Other expense (income)",
        "item_order": 5600
      },
      {
        "item_flag": "negative",
        "line_item": "0160101",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {
            "FY2021": -21536324.864643037,
            "Jun-21": -10464971.01118958,
            "May-21": -11071353.853453457
          },
          "3": {
            "FY2021": -1252170.3160755658,
            "May-21": -1252170.3160755658
          },
          "4": {}
        },
        "item_name": "Other expense (income) - MFG platform",
        "item_order": 5700
      },
      {
        "item_flag": "negative",
        "line_item": "0160102",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Other expense (income) - Procurement platform",
        "item_order": 5800
      },
      {
        "item_flag": "negative",
        "line_item": "0160103",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Other expense (income) - R&D platform",
        "item_order": 5900
      },
      {
        "item_flag": "negative",
        "line_item": "0160104",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Other expense (income) - PBU",
        "item_order": 6000
      },
      {
        "item_flag": "negative",
        "line_item": "0160105",
        "item_level": 3,
        "item_type": "N",
        "values": {
          "2": {},
          "3": {},
          "4": {}
        },
        "item_name": "Other expense (income) - D Group level",
        "item_order": 6100
      },
      {
        "item_flag": "negative",
        "line_item": "0160200",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 6759403.1082200995,
            "FY2021": 47507100.914616436,
            "Jun-21": 9258477.75858301,
            "May-21": 7862024.89062333,
            "Jan-21": 5177289.490449999,
            "Feb-21": 11133307.6215,
            "Mar-21": 7316598.04524
          },
          "3": {
            "Apr-21": 1534053.7390446393,
            "FY2021": 10467857.671491321,
            "May-21": 2283337.901986681,
            "Jan-21": 3058681.2845300003,
            "Feb-21": 1900723.22186,
            "Mar-21": 1691061.52407
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Corporate Allocation",
        "item_order": 6200
      },
      {
        "item_flag": "negative",
        "line_item": "0160300",
        "item_level": 2,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 17235992.806483097,
            "FY2021": 97443880.019912,
            "Jun-21": 9094600.387502152,
            "May-21": 12410066.794566765,
            "Jan-21": 19208822.27409,
            "Feb-21": 20176124.93794,
            "Mar-21": 19318272.81933
          },
          "3": {
            "Apr-21": -533029.986647251,
            "FY2021": -1054646.556132847,
            "May-21": 2970716.3623344046,
            "Jan-21": -1450226.37403,
            "Feb-21": -1533670.3992,
            "Mar-21": -508436.15858999995
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "ICC",
        "item_order": 6300
      },
      {
        "item_flag": "positive",
        "line_item": "0170000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 58404814.03284197,
            "FY2021": 357955515.5069111,
            "Jun-21": 65962170.07263805,
            "May-21": 88360384.25465108,
            "Jan-21": 70852715.98443,
            "Feb-21": 12751738.57779,
            "Mar-21": 61623692.58456001
          },
          "3": {
            "Apr-21": 42930227.264199436,
            "FY2021": 214523125.33043754,
            "May-21": 59939012.08402808,
            "Jan-21": 57380045.73638,
            "Feb-21": 18000471.72295,
            "Mar-21": 36273368.52288
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Profit before tax",
        "item_order": 6400
      },
      {
        "item_flag": "positive",
        "line_item": "0230000",
        "item_level": 2,
        "item_type": "PEU",
        "values": {
          "2": {
            "Apr-21": 0.0260494853157293,
            "FY2021": 0.031297697538453254,
            "Jun-21": 0.035264834688327445,
            "May-21": 0.04682416986553292,
            "Jan-21": 0.040518017222355845,
            "Feb-21": 0.00876604506876406,
            "Mar-21": 0.02758255942116086
          },
          "3": {
            "Apr-21": 0.05050410620101414,
            "FY2021": 0.05070367486328816,
            "May-21": 0.06697884951752091,
            "Jan-21": 0.04947144206886077,
            "Feb-21": 0.03341793740002548,
            "Mar-21": 0.04606255092836387
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Profit before tax %",
        "item_order": 6450
      },
      {
        "item_flag": "negative",
        "line_item": "0180000",
        "item_level": 1,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 13497810.159189356,
            "FY2021": 64509255.98959103,
            "Jun-21": 14807351.59491934,
            "May-21": 19698870.27642234,
            "Jan-21": 18468156.13321,
            "Feb-21": 6306243.334749999,
            "Mar-21": -8269175.508900001
          },
          "3": {
            "Apr-21": 7941405.675825827,
            "FY2021": 41431517.86328123,
            "May-21": 10119134.4622154,
            "Jan-21": 16177445.93513,
            "Feb-21": 2727451.5556699997,
            "Mar-21": 4466080.23444
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Income tax",
        "item_order": 6500
      },
      {
        "item_flag": "negative",
        "line_item": "0300000",
        "item_level": 1,
        "item_type": "N",
        "values": {
          "2": {
            "Apr-21": 0.020029245146,
            "FY2021": 0.08376303437799999,
            "Jun-21": 0.027348497103,
            "May-21": 0.036385292129
          },
          "3": {
            "Apr-21": 0.041161654013,
            "FY2021": 0.096832876647,
            "May-21": 0.055671222634
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0
          }
        },
        "item_name": "Income tax - Gov't subsidy",
        "item_order": 6550
      },
      {
        "item_flag": "positive",
        "line_item": "0190000",
        "item_level": 1,
        "item_type": "NME",
        "values": {
          "2": {
            "Apr-21": 44907003.873652615,
            "FY2021": 293446259.51732004,
            "Jun-21": 51154818.47771871,
            "May-21": 68661513.97822875,
            "Jan-21": 52384559.85122,
            "Feb-21": 6445495.243039999,
            "Mar-21": 69892868.09346001
          },
          "3": {
            "Apr-21": 34988821.58837361,
            "FY2021": 173091607.18926626,
            "May-21": 49819877.62181268,
            "Jan-21": 41202599.80125,
            "Feb-21": 15273020.167279998,
            "Mar-21": 31807288.01055
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Net income",
        "item_order": 6600
      },
      {
        "item_flag": "positive",
        "line_item": "0240000",
        "item_level": 2,
        "item_type": "PEU",
        "values": {
          "2": {
            "Apr-21": 0.02002924514616745,
            "FY2021": 0.025657356504641488,
            "Jun-21": 0.027348497102833443,
            "May-21": 0.03638529212905743,
            "Jan-21": 0.029956769740537386,
            "Feb-21": 0.004430886145156942,
            "Mar-21": 0.03128381481940341
          },
          "3": {
            "Apr-21": 0.04116165401297951,
            "FY2021": 0.0409111165007035,
            "May-21": 0.055671222634346276,
            "Jan-21": 0.03552370868644396,
            "Feb-21": 0.02835441424619755,
            "Mar-21": 0.040391198378913896
          },
          "4": {
            "Apr-21": 0,
            "FY2021": 0,
            "Jun-21": 0,
            "May-21": 0,
            "Jan-21": 0,
            "Feb-21": 0,
            "Mar-21": 0
          }
        },
        "item_name": "Net income %",
        "item_order": 6700
      }
    ],
    "actual_version_id": 3,
    "currency_id": 3
  },
  "result": true
};

var overall_overview_report = {
  "result": true,
  "msg": null,
  "data" : {
    "subpbu_list": [
      {
        "consolidated": 1,
        "values": {
          "I_OD_DO": -1.2055306314037496E8,
          "Y_M_VBpp": 0.5795120864558232,
          "I_O_BP": 0.0763911738231269,
          "I_O_VBpp": -0.0763911738231269,
          "Y_G_VBpp": 0.008971850475335857,
          "I_OD_DG": -2.077287862797972E8,
          "Y_G_LFYP": 0.286428,
          "I_M_LFYP": 0.382883,
          "I_G_BP": 0.13163204158727454,
          "I_G_LFYP": 0.286428,
          "Y_M_LFYP": 0.382883,
          "I_OD_RDp": 3.509304479767142E7,
          "Y_M_BP": 0.15371132070667598,
          "Y_R_LFY": 2.029367752425E10,
          "I_M_LFY": 3.088830684E9,
          "I_R_VB": -1.0,
          "Y_R_VB": 0.07887630603710909,
          "Y_OD_DOA": -9.111631780779475E7,
          "I_O_AP": null,
          "Y_O_VBpp": -0.012678622066962901,
          "consolated": 1,
          "I_OD_NRDp": 4.475040493050413E7,
          "Y_O_VBabs": -1.0140686606508446E8,
          "I_R_B": 1.5781019862255125E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.7332234071624992,
          "Y_R_A": 1.1437119777488955E10,
          "Y_R_B": 1.0600955562273293E10,
          "I_G_VBabs": -2.077287862797972E8,
          "I_OD_DOA": -1.2788533655162165E8,
          "I_OD_DGv": -2.0772878627979723E8,
          "Y_O_LFYP": 0.16362,
          "Y_G_VBabs": 1.9161788425030804E8,
          "addSubotal": 1,
          "I_M_BP": 0.18002024500325745,
          "Y_OD_RDp": -1.2039582967244089E7,
          "I_OD_DGm": 2.9802322387695312E-8,
          "I_R_LFY": 2.029367752425E10,
          "Y_M_LFY": 3.088830684E9,
          "Y_G_BP": 0.10644530598001539,
          "Y_O_BP": 0.052142931178942614,
          "Y_S_B": 4.5,
          "I_G_A": null,
          "FY_S_2019FY": 13.179738,
          "I_G_B": 2.077287862797972E8,
          "Y_O_LFY": 1.4445253756499999E9,
          "I_O_LFYP": 0.16362,
          "Y_OD_DG": 1.9161788425030804E8,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 4.3091110000000015,
          "I_G_LFY": 2.212116873435E9,
          "I_G_VBpp": -0.13163204158727454,
          "I_O_A": null,
          "I_M_B": 2.84090306200444E8,
          "I_O_B": 1.2055306314037496E8,
          "subPbuId": 2,
          "FY_S_B": 8.499600000000001,
          "Y_OD_DO": -1.0140686606508446E8,
          "FY_S_M": null,
          "Y_G_A": 1.3200398427570348E9,
          "I_O_LFY": 1.4445253756499999E9,
          "Y_OD_DS": -1.0290548257289708E7,
          "Y_G_AP": 0.11541715645535125,
          "Y_G_B": 1.1284219585067267E9,
          "Y_OD_DGm": 1.0261212851213746E8,
          "I_O_VBabs": -1.2055306314037496E8,
          "Y_M_B": 1.6294868802298107E9,
          "Y_O_A": 4.513580302495607E8,
          "I_M_VBpp": -0.18002024500325745,
          "Y_G_LFY": 2.212116873435E9,
          "Y_O_AP": 0.03946430911197971,
          "Y_M_A": 8.385963931376057E9,
          "Y_OD_NRDp": -2.706946190908587E8,
          "Y_O_B": 5.527648963146452E8,
          "Y_OD_DGv": 8.900575573817058E7,
          "Y_S_VB": -0.04241977777777745,
          "I_OD_DS": 7332273.411246687
        },
        "subpbu_name": "D WH consolidated"
      },
      {
        "subpbu_id": 2,
        "parent_id": 1,
        "values": {
          "I_OD_DO": -1.2055306314037496E8,
          "Y_M_VBpp": 0.5795120864558232,
          "I_O_BP": 0.0763911738231269,
          "I_O_VBpp": -0.0763911738231269,
          "Y_G_VBpp": 0.008971850475335857,
          "I_OD_DG": -2.077287862797972E8,
          "Y_G_LFYP": 0.1090052244,
          "I_M_LFYP": 0.152206552,
          "I_G_BP": 0.13163204158727454,
          "I_G_LFYP": 0.1090052244,
          "Y_M_LFYP": 0.152206552,
          "I_OD_RDp": 3.509304479767142E7,
          "Y_M_BP": 0.15371132070667598,
          "Y_R_LFY": 2.029367752425E10,
          "I_M_LFY": 3.088830684E9,
          "I_R_VB": -1.0,
          "Y_R_VB": 0.07887630603710909,
          "Y_OD_DOA": -9.111631780779475E7,
          "I_O_AP": null,
          "Y_O_VBpp": -0.012678622066962901,
          "I_OD_NRDp": 4.475040493050413E7,
          "Y_O_VBabs": -1.0140686606508446E8,
          "I_R_B": 1.5781019862255125E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.7332234071624992,
          "Y_R_A": 1.1437119777488955E10,
          "Y_R_B": 1.0600955562273293E10,
          "I_G_VBabs": -2.077287862797972E8,
          "I_OD_DOA": -1.2788533655162165E8,
          "I_OD_DGv": -2.0772878627979723E8,
          "Y_O_LFYP": 0.0711810549,
          "Y_G_VBabs": 1.9161788425030804E8,
          "addSubotal": 1,
          "I_M_BP": 0.18002024500325745,
          "Y_OD_RDp": -1.2039582967244089E7,
          "I_OD_DGm": 2.9802322387695312E-8,
          "I_R_LFY": 2.029367752425E10,
          "Y_M_LFY": 3.088830684E9,
          "Y_G_BP": 0.10644530598001539,
          "Y_O_BP": 0.052142931178942614,
          "Y_S_B": 4.5,
          "I_G_A": null,
          "FY_S_2019FY": 13.179738,
          "I_G_B": 2.077287862797972E8,
          "Y_O_LFY": 1.4445253756499999E9,
          "I_O_LFYP": 0.0711810549,
          "Y_OD_DG": 1.9161788425030804E8,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 4.3091110000000015,
          "I_G_LFY": 2.212116873435E9,
          "I_G_VBpp": -0.13163204158727454,
          "I_O_A": null,
          "I_M_B": 2.84090306200444E8,
          "I_O_B": 1.2055306314037496E8,
          "subPbuId": 2,
          "FY_S_B": 8.499600000000001,
          "Y_OD_DO": -1.0140686606508446E8,
          "FY_S_M": null,
          "Y_G_A": 1.3200398427570348E9,
          "I_O_LFY": 1.4445253756499999E9,
          "Y_OD_DS": -1.0290548257289708E7,
          "Y_G_AP": 0.11541715645535125,
          "Y_G_B": 1.1284219585067267E9,
          "Y_OD_DGm": 1.0261212851213746E8,
          "I_O_VBabs": -1.2055306314037496E8,
          "Y_M_B": 1.6294868802298107E9,
          "Y_O_A": 4.513580302495607E8,
          "I_M_VBpp": -0.18002024500325745,
          "Y_G_LFY": 2.212116873435E9,
          "Y_O_AP": 0.03946430911197971,
          "Y_M_A": 8.385963931376057E9,
          "Y_OD_NRDp": -2.706946190908587E8,
          "Y_O_B": 5.527648963146452E8,
          "Y_OD_DGv": 8.900575573817058E7,
          "Y_S_VB": -0.042419777777777404,
          "I_OD_DS": 7332273.411246687
        },
        "addSubtotal": 1,
        "pbu_name": "D WH",
        "subpbu_name": "D WH L5",
        "pbu_id": 1
      },
      {
        "subpbu_id": 3,
        "parent_id": 1,
        "values": {
          "I_OD_DO": -1.0276832834000254E8,
          "Y_M_VBpp": 0.5303251273237843,
          "I_O_BP": 0.09596068668836731,
          "I_O_VBpp": -0.09596068668836731,
          "Y_G_VBpp": -0.0025970948926150317,
          "I_OD_DG": -1.675558735350849E8,
          "Y_G_LFYP": 0.1345691415,
          "I_M_LFYP": 0.1690844125,
          "I_G_BP": 0.156456536199561,
          "I_G_LFYP": 0.1345691415,
          "Y_M_LFYP": 0.1690844125,
          "I_OD_RDp": 3.0710880811130088E7,
          "Y_M_BP": 0.17231316170843677,
          "Y_R_LFY": 1.220166297105E10,
          "I_M_LFY": 2.0631110156999998E9,
          "I_R_VB": -1.0,
          "Y_R_VB": -0.16025947164410517,
          "Y_OD_DOA": -1.3041406423463947E8,
          "I_O_AP": null,
          "Y_O_VBpp": -0.012093304664695909,
          "I_OD_NRDp": 3.0543616698789585E7,
          "Y_O_VBabs": -1.3167973152846277E8,
          "I_R_B": 1.0709419855835657E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.7026382890322211,
          "Y_R_A": 5.307857607994305E9,
          "Y_R_B": 6.320830576542989E9,
          "I_G_VBabs": -1.675558735350849E8,
          "I_OD_DOA": -1.0630137602516523E8,
          "I_OD_DGv": -1.675558735350849E8,
          "Y_O_LFYP": 0.0620479865,
          "Y_G_VBabs": -1.5502435566349113E8,
          "addSubotal": 1,
          "I_M_BP": 0.18474091705413118,
          "Y_OD_RDp": 2076133.7898161113,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 1.220166297105E10,
          "Y_M_LFY": 2.0631110156999998E9,
          "Y_G_BP": 0.139430518053605,
          "Y_O_BP": 0.06662585720807407,
          "Y_S_B": 2.632,
          "I_G_A": null,
          "FY_S_2019FY": 9.855822,
          "I_G_B": 1.675558735350849E8,
          "Y_O_LFY": 7.570886205E8,
          "I_O_LFYP": 0.0620479865,
          "Y_OD_DG": -1.5502435566349113E8,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 2.16476,
          "I_G_LFY": 1.641967311705E9,
          "I_G_VBpp": -0.156456536199561,
          "I_O_A": null,
          "I_M_B": 1.9784680452848005E8,
          "I_O_B": 1.0276832834000254E8,
          "subPbuId": 3,
          "FY_S_B": 4.599600000000001,
          "Y_OD_DO": -1.3167973152846277E8,
          "FY_S_M": null,
          "Y_G_A": 7.262923261529647E8,
          "I_O_LFY": 7.570886205E8,
          "Y_OD_DS": -1265667.2938233018,
          "Y_G_AP": 0.13683342316098998,
          "Y_G_B": 8.813166818164558E8,
          "Y_OD_DGm": -1.3785009884449959E7,
          "I_O_VBabs": -1.0276832834000254E8,
          "Y_M_B": 1.0891623012674837E9,
          "Y_O_A": 2.894510239007189E8,
          "I_M_VBpp": -0.18474091705413118,
          "Y_G_LFY": 1.641967311705E9,
          "Y_O_AP": 0.05453255254337816,
          "Y_M_A": 3.729503988107776E9,
          "Y_OD_NRDp": 2.2534157639035553E7,
          "Y_O_B": 4.211307554291817E8,
          "Y_OD_DGv": -1.4123934577904117E8,
          "Y_S_VB": -0.17752279635258367,
          "I_OD_DS": 3533047.6851626933
        },
        "addSubtotal": 1,
        "pbu_name": "D WH",
        "subpbu_name": "D WH L6",
        "pbu_id": 1
      },
      {
        "subpbu_id": 4,
        "parent_id": 1,
        "values": {
          "I_OD_DO": -0.0,
          "Y_M_VBpp": 1.0,
          "I_O_BP": 0.0,
          "I_O_VBpp": -0.0,
          "Y_G_VBpp": 0.0,
          "I_OD_DG": -0.0,
          "Y_G_LFYP": null,
          "I_M_LFYP": null,
          "I_G_BP": 0.0,
          "I_G_LFYP": null,
          "Y_M_LFYP": null,
          "I_OD_RDp": 0.0,
          "Y_M_BP": 0.0,
          "Y_R_LFY": -1.90396925922E10,
          "I_M_LFY": null,
          "I_R_VB": 1.0,
          "Y_R_VB": 0.19994537348078828,
          "Y_OD_DOA": 0.0,
          "I_O_AP": null,
          "Y_O_VBpp": 0.0,
          "I_OD_NRDp": 0.0,
          "Y_O_VBabs": 0.0,
          "I_R_B": -9.56206126813431E8,
          "I_R_A": null,
          "FY_S_VB": null,
          "I_G_AP": null,
          "Y_M_AP": 1.0,
          "Y_R_A": -4.558540424914884E9,
          "Y_R_B": -5.697786468341133E9,
          "I_G_VBabs": -0.0,
          "I_OD_DOA": -0.0,
          "I_OD_DGv": 0.0,
          "Y_O_LFYP": null,
          "Y_G_VBabs": 0.0,
          "addSubotal": 1,
          "I_M_BP": 0.0,
          "Y_OD_RDp": 0.0,
          "I_OD_DGm": -0.0,
          "I_R_LFY": -1.90396925922E10,
          "Y_M_LFY": null,
          "Y_G_BP": 0.0,
          "Y_O_BP": 0.0,
          "Y_S_B": 0.0,
          "I_G_A": null,
          "FY_S_2019FY": null,
          "I_G_B": 0.0,
          "Y_O_LFY": null,
          "I_O_LFYP": null,
          "Y_OD_DG": 0.0,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 0.0,
          "I_G_LFY": null,
          "I_G_VBpp": -0.0,
          "I_O_A": null,
          "I_M_B": 0.0,
          "I_O_B": 0.0,
          "subPbuId": 4,
          "FY_S_B": 0.0,
          "Y_OD_DO": 0.0,
          "FY_S_M": null,
          "Y_G_A": 0.0,
          "I_O_LFY": null,
          "Y_OD_DS": 0.0,
          "Y_G_AP": 0.0,
          "Y_G_B": 0.0,
          "Y_OD_DGm": 0.0,
          "I_O_VBabs": -0.0,
          "Y_M_B": 0.0,
          "Y_O_A": 0.0,
          "I_M_VBpp": -0.0,
          "Y_G_LFY": null,
          "Y_O_AP": 0.0,
          "Y_M_A": -4.558540424914884E9,
          "Y_OD_NRDp": 0.0,
          "Y_O_B": 0.0,
          "Y_OD_DGv": 0.0,
          "Y_S_VB": null,
          "I_OD_DS": 0.0
        },
        "addSubtotal": 1,
        "pbu_name": "D WH",
        "subpbu_name": "D WH Consol. adj.",
        "pbu_id": 1
      },
      {
        "subpbu_id": 39,
        "parent_id": 1,
        "values": {
          "I_OD_DO": -0.0,
          "Y_M_VBpp": 1.0,
          "I_O_BP": null,
          "I_O_VBpp": null,
          "Y_G_VBpp": 0.0,
          "I_OD_DG": -0.0,
          "Y_G_LFYP": null,
          "I_M_LFYP": null,
          "I_G_BP": null,
          "I_G_LFYP": null,
          "Y_M_LFYP": null,
          "I_OD_RDp": 0.0,
          "Y_M_BP": null,
          "Y_R_LFY": null,
          "I_M_LFY": null,
          "I_R_VB": null,
          "Y_R_VB": null,
          "Y_OD_DOA": 0.0,
          "I_O_AP": null,
          "Y_O_VBpp": 0.0,
          "I_OD_NRDp": 0.0,
          "Y_O_VBabs": 0.0,
          "I_R_B": 0.0,
          "I_R_A": null,
          "FY_S_VB": null,
          "I_G_AP": null,
          "Y_M_AP": 1.0,
          "Y_R_A": 6.253516082847592E8,
          "Y_R_B": 0.0,
          "I_G_VBabs": -0.0,
          "I_OD_DOA": -0.0,
          "I_OD_DGv": null,
          "Y_O_LFYP": null,
          "Y_G_VBabs": 0.0,
          "I_M_BP": null,
          "Y_OD_RDp": 0.0,
          "I_OD_DGm": -0.0,
          "I_R_LFY": null,
          "Y_M_LFY": null,
          "Y_G_BP": null,
          "Y_O_BP": null,
          "Y_S_B": 0.0,
          "I_G_A": null,
          "FY_S_2019FY": null,
          "I_G_B": 0.0,
          "Y_O_LFY": null,
          "I_O_LFYP": null,
          "Y_OD_DG": 0.0,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 0.0,
          "I_G_LFY": null,
          "I_G_VBpp": null,
          "I_O_A": null,
          "I_M_B": 0.0,
          "I_O_B": 0.0,
          "subPbuId": 39,
          "FY_S_B": 0.0,
          "Y_OD_DO": 0.0,
          "FY_S_M": null,
          "Y_G_A": 0.0,
          "I_O_LFY": null,
          "Y_OD_DS": 0.0,
          "Y_G_AP": 0.0,
          "Y_G_B": 0.0,
          "Y_OD_DGm": 0.0,
          "I_O_VBabs": -0.0,
          "Y_M_B": 0.0,
          "Y_O_A": 0.0,
          "I_M_VBpp": null,
          "Y_G_LFY": null,
          "Y_O_AP": 0.0,
          "Y_M_A": 6.253516082847592E8,
          "Y_OD_NRDp": 0.0,
          "Y_O_B": 0.0,
          "Y_OD_DGv": null,
          "Y_S_VB": null,
          "I_OD_DS": 0.0
        },
        "pbu_name": "D WH",
        "subpbu_name": "D WH for G2",
        "pbu_id": 1
      },
      {
        "consolidated": 1,
        "values": {
          "I_OD_DO": -3.2037921806271106E7,
          "Y_M_VBpp": 0.54370628689199,
          "I_O_BP": 0.0555130129027903,
          "I_O_VBpp": -0.0555130129027903,
          "Y_G_VBpp": 0.013345949493954262,
          "I_OD_DG": -9.893184216501255E7,
          "Y_G_LFYP": 0.082039,
          "I_M_LFYP": 0.123771,
          "I_G_BP": 0.1714219999603142,
          "I_G_LFYP": 0.082039,
          "Y_M_LFYP": 0.123771,
          "I_OD_RDp": 3.7656647098921075E7,
          "Y_M_BP": 0.24951822161453702,
          "Y_R_LFY": 7.83296141775E9,
          "I_M_LFY": 2.0179241579699998E9,
          "I_R_VB": -1.0,
          "Y_R_VB": -0.10623198990640863,
          "Y_OD_DOA": -1.3001535486632109E7,
          "I_O_AP": null,
          "Y_O_VBpp": 0.0013718504668587123,
          "consolated": 1,
          "I_OD_NRDp": 2.4890627785615817E7,
          "Y_O_VBabs": -1.1420202972772807E7,
          "I_R_B": 5.771245358700528E8,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.793224508506527,
          "Y_R_A": 3.556172010789203E9,
          "Y_R_B": 3.978853539876435E9,
          "I_G_VBabs": -9.893184216501255E7,
          "I_OD_DOA": -3.638456728047566E7,
          "I_OD_DGv": -9.893184216501255E7,
          "Y_O_LFYP": 0.023729,
          "Y_G_VBabs": -1.6952031512257576E7,
          "addSubotal": 5,
          "I_M_BP": 0.2576078341088425,
          "Y_OD_RDp": 7448912.937039465,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 7.83296141775E9,
          "Y_M_LFY": 2.0179241579699998E9,
          "Y_G_BP": 0.15239020190723965,
          "Y_O_BP": 0.038560329904172796,
          "Y_S_B": 3.2517724287913,
          "I_G_A": null,
          "FY_S_2019FY": 5.928247,
          "I_G_B": 9.893184216501255E7,
          "Y_O_LFY": 6.3893590089E8,
          "I_O_LFYP": 0.023729,
          "Y_OD_DG": -1.6952031512257576E7,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 3.039776,
          "I_G_LFY": 1.33085970573E9,
          "I_G_VBpp": -0.1714219999603142,
          "I_O_A": null,
          "I_M_B": 1.4867180169655532E8,
          "I_O_B": 3.2037921806271106E7,
          "subPbuId": 6,
          "FY_S_B": 5.8000046331510005,
          "Y_OD_DO": -1.1420202972772807E7,
          "FY_S_M": null,
          "Y_G_A": 5.893862627888476E8,
          "I_O_LFY": 6.3893590089E8,
          "Y_OD_DS": 1581332.5138593018,
          "Y_G_AP": 0.1657361514011939,
          "Y_G_B": 6.063382943011051E8,
          "Y_OD_DGm": 4.746049204780644E7,
          "I_O_VBabs": -3.2037921806271106E7,
          "Y_M_B": 9.927964593346734E8,
          "Y_O_A": 1.4200570216524827E8,
          "I_M_VBpp": -0.2576078341088425,
          "Y_G_LFY": 1.33085970573E9,
          "Y_O_AP": 0.03993218037103151,
          "Y_M_A": 2.8208427954229336E9,
          "Y_OD_NRDp": -3498416.9114139974,
          "Y_O_B": 1.5342590513802108E8,
          "Y_OD_DGv": -6.441252356006402E7,
          "Y_S_VB": -0.06519411595789329,
          "I_OD_DS": 4346645.474204555
        },
        "subpbu_name": "H WH consolidated"
      },
      {
        "subpbu_id": 6,
        "parent_id": 5,
        "values": {
          "I_OD_DO": -3.2037921806271106E7,
          "Y_M_VBpp": 0.54370628689199,
          "I_O_BP": 0.0555130129027903,
          "I_O_VBpp": -0.0555130129027903,
          "Y_G_VBpp": 0.013345949493954262,
          "I_OD_DG": -9.893184216501255E7,
          "Y_G_LFYP": 0.1699050505,
          "I_M_LFYP": 0.2576195707,
          "I_G_BP": 0.1714219999603142,
          "I_G_LFYP": 0.1699050505,
          "Y_M_LFYP": 0.2576195707,
          "I_OD_RDp": 3.7656647098921075E7,
          "Y_M_BP": 0.24951822161453702,
          "Y_R_LFY": 7.83296141775E9,
          "I_M_LFY": 2.0179241579699998E9,
          "I_R_VB": -1.0,
          "Y_R_VB": -0.10623198990640863,
          "Y_OD_DOA": -1.3001535486632109E7,
          "I_O_AP": null,
          "Y_O_VBpp": 0.0013718504668587123,
          "I_OD_NRDp": 2.4890627785615817E7,
          "Y_O_VBabs": -1.1420202972772807E7,
          "I_R_B": 5.771245358700528E8,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.793224508506527,
          "Y_R_A": 3.556172010789203E9,
          "Y_R_B": 3.978853539876435E9,
          "I_G_VBabs": -9.893184216501255E7,
          "I_OD_DOA": -3.638456728047566E7,
          "I_OD_DGv": -9.893184216501255E7,
          "Y_O_LFYP": 0.081570158,
          "Y_G_VBabs": -1.6952031512257576E7,
          "addSubotal": 5,
          "I_M_BP": 0.2576078341088425,
          "Y_OD_RDp": 7448912.937039465,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 7.83296141775E9,
          "Y_M_LFY": 2.0179241579699998E9,
          "Y_G_BP": 0.15239020190723965,
          "Y_O_BP": 0.038560329904172796,
          "Y_S_B": 3.2517724287913,
          "I_G_A": null,
          "FY_S_2019FY": 5.928247,
          "I_G_B": 9.893184216501255E7,
          "Y_O_LFY": 6.3893590089E8,
          "I_O_LFYP": 0.081570158,
          "Y_OD_DG": -1.6952031512257576E7,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 3.039776,
          "I_G_LFY": 1.33085970573E9,
          "I_G_VBpp": -0.1714219999603142,
          "I_O_A": null,
          "I_M_B": 1.4867180169655532E8,
          "I_O_B": 3.2037921806271106E7,
          "subPbuId": 6,
          "FY_S_B": 5.8000046331510005,
          "Y_OD_DO": -1.1420202972772807E7,
          "FY_S_M": null,
          "Y_G_A": 5.893862627888476E8,
          "I_O_LFY": 6.3893590089E8,
          "Y_OD_DS": 1581332.5138593018,
          "Y_G_AP": 0.1657361514011939,
          "Y_G_B": 6.063382943011051E8,
          "Y_OD_DGm": 4.746049204780644E7,
          "I_O_VBabs": -3.2037921806271106E7,
          "Y_M_B": 9.927964593346734E8,
          "Y_O_A": 1.4200570216524827E8,
          "I_M_VBpp": -0.2576078341088425,
          "Y_G_LFY": 1.33085970573E9,
          "Y_O_AP": 0.03993218037103151,
          "Y_M_A": 2.8208427954229336E9,
          "Y_OD_NRDp": -3498416.9114139974,
          "Y_O_B": 1.5342590513802108E8,
          "Y_OD_DGv": -6.441252356006402E7,
          "Y_S_VB": -0.06519411595789329,
          "I_OD_DS": 4346645.474204555
        },
        "addSubtotal": 5,
        "pbu_name": "H WH",
        "subpbu_name": "H WH L5",
        "pbu_id": 2
      },
      {
        "subpbu_id": 7,
        "parent_id": 5,
        "values": {
          "I_OD_DO": -7.324132964013425E7,
          "Y_M_VBpp": 0.6857856429546474,
          "I_O_BP": 0.07092922251638535,
          "I_O_VBpp": -0.07092922251638535,
          "Y_G_VBpp": -0.007131950568819767,
          "I_OD_DG": -1.513867198041333E8,
          "Y_G_LFYP": 0.0911332848,
          "I_M_LFYP": 0.1414009601,
          "I_G_BP": 0.14660769251148417,
          "I_G_LFYP": 0.0911332848,
          "Y_M_LFYP": 0.1414009601,
          "I_OD_RDp": 4.336543687228953E7,
          "Y_M_BP": 0.15705603832633855,
          "Y_R_LFY": 1.0086785826449999E10,
          "I_M_LFY": 1.426281200685E9,
          "I_R_VB": -1.0,
          "Y_R_VB": -0.31084594743278765,
          "Y_OD_DOA": -1.0576987947617543E8,
          "I_O_AP": null,
          "Y_O_VBpp": -0.004726988142715341,
          "I_OD_NRDp": 3.081941509740164E7,
          "Y_O_VBabs": -1.198757164158015E8,
          "I_R_B": 1.0325973842898781E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.8428416812809859,
          "Y_R_A": 4.759310460633337E9,
          "Y_R_B": 6.906018244983283E9,
          "I_G_VBabs": -1.513867198041333E8,
          "I_OD_DOA": -7.720186783444212E7,
          "I_OD_DGv": -1.513867198041333E8,
          "Y_O_LFYP": 0.0054803219,
          "Y_G_VBabs": -2.9784791639395034E8,
          "addSubotal": 5,
          "I_M_BP": 0.17784648147839643,
          "Y_OD_RDp": 8.94724897394481E7,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 1.0086785826449999E10,
          "Y_M_LFY": 1.426281200685E9,
          "Y_G_BP": 0.12293464036930425,
          "Y_O_BP": 0.04536179214087781,
          "Y_S_B": 3.2517780000000003,
          "I_G_A": null,
          "FY_S_2019FY": 6.151931,
          "I_G_B": 1.513867198041333E8,
          "Y_O_LFY": 5.52788342415E7,
          "I_O_LFYP": 0.0054803219,
          "Y_OD_DG": -2.9784791639395034E8,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 2.636058,
          "I_G_LFY": 9.1924192638E8,
          "I_G_VBpp": -0.14660769251148417,
          "I_O_A": null,
          "I_M_B": 1.8364381157975042E8,
          "I_O_B": 7.324132964013425E7,
          "subPbuId": 7,
          "FY_S_B": 5.800020000000001,
          "Y_OD_DO": -1.198757164158015E8,
          "FY_S_M": null,
          "Y_G_A": 5.511409529369233E8,
          "I_O_LFY": 5.52788342415E7,
          "Y_OD_DS": -1.4105836939626068E7,
          "Y_G_AP": 0.11580268980048448,
          "Y_G_B": 8.489888693308736E8,
          "Y_OD_DGm": -3.3943166946903795E7,
          "I_O_VBabs": -7.324132964013425E7,
          "Y_M_B": 1.0846318661664877E9,
          "Y_O_A": 1.9339364773423997E8,
          "I_M_VBpp": -0.17784648147839643,
          "Y_G_LFY": 9.1924192638E8,
          "Y_O_AP": 0.040634803998162466,
          "Y_M_A": 4.0113452303783855E9,
          "Y_OD_NRDp": 1.0260554717832682E8,
          "Y_O_B": 3.1326936415004146E8,
          "Y_OD_DGv": -2.6390474944704655E8,
          "Y_S_VB": -0.18934871937752215,
          "I_OD_DS": 3960538.1943078786
        },
        "addSubtotal": 5,
        "pbu_name": "H WH",
        "subpbu_name": "H WH L6",
        "pbu_id": 2
      },
      {
        "subpbu_id": 9,
        "parent_id": 5,
        "values": {
          "I_OD_DO": -1.2522221550421586E7,
          "Y_M_VBpp": 0.6071117743168738,
          "I_O_BP": 0.005826462621482136,
          "I_O_VBpp": -0.005826462621482136,
          "Y_G_VBpp": -0.0024397296662310945,
          "I_OD_DG": -3.2577532652084056E7,
          "Y_G_LFYP": 0.0131177329,
          "I_M_LFYP": 0.0180490009,
          "I_G_BP": 0.015157995371124156,
          "I_G_LFYP": 0.0131177329,
          "Y_M_LFYP": 0.0180490009,
          "I_OD_RDp": 5383351.179534735,
          "Y_M_BP": 0.01634199848982714,
          "Y_R_LFY": 2.84346690852E10,
          "I_M_LFY": 5.1321736836E8,
          "I_R_VB": -1.0,
          "Y_R_VB": 0.3262666574139162,
          "Y_OD_DOA": -4.299336778302997E7,
          "I_O_AP": null,
          "Y_O_VBpp": -0.002182488883108115,
          "I_OD_NRDp": 1.2486241657804577E7,
          "Y_O_VBabs": -1.9118735761527643E7,
          "I_R_B": 2.149197954905301E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.6234537728067009,
          "Y_R_A": 1.9224843607807278E10,
          "Y_R_B": 1.449545873775772E10,
          "I_G_VBabs": -3.2577532652084056E7,
          "I_OD_DOA": -1.4707939814744744E7,
          "I_OD_DGv": -3.2577532652084056E7,
          "Y_O_LFYP": 0.0022682526,
          "Y_G_VBabs": 1.1849453909805238E7,
          "addSubotal": 5,
          "I_M_BP": 0.019017145010520934,
          "Y_OD_RDp": -1.8853102031190798E7,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 2.84346690852E10,
          "Y_M_LFY": 5.1321736836E8,
          "Y_G_BP": 0.012422942264753819,
          "Y_O_BP": 0.004829226700631053,
          "Y_S_B": 1.1872446683259,
          "I_G_A": null,
          "FY_S_2019FY": 13.027839,
          "I_G_B": 3.2577532652084056E7,
          "Y_O_LFY": 6.4497013122E7,
          "I_O_LFYP": 0.0022682526,
          "Y_OD_DG": 1.1849453909805238E7,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 1.7539899999999997,
          "I_G_LFY": 3.72998394645E8,
          "I_G_VBpp": -0.015157995371124156,
          "I_O_A": null,
          "I_M_B": 4.0871609164749146E7,
          "I_O_B": 1.2522221550421586E7,
          "subPbuId": 9,
          "FY_S_B": 2.1086919530709003,
          "Y_OD_DO": -1.9118735761527643E7,
          "FY_S_M": null,
          "Y_G_A": 1.9192570091009068E8,
          "I_O_LFY": 6.4497013122E7,
          "Y_OD_DS": 2.387463202150233E7,
          "Y_G_AP": 0.009983212598522724,
          "Y_G_B": 1.8007624700028545E8,
          "Y_OD_DGm": -4.6903421278620645E7,
          "I_O_VBabs": -1.2522221550421586E7,
          "Y_M_B": 2.3688476480178833E8,
          "Y_O_A": 5.088312061274764E7,
          "I_M_VBpp": -0.019017145010520934,
          "Y_G_LFY": 3.72998394645E8,
          "Y_O_AP": 0.002646737817522938,
          "Y_M_A": 1.1985801278906235E10,
          "Y_OD_NRDp": -3.5989719661644414E7,
          "Y_O_B": 7.000185637427528E7,
          "Y_OD_DGv": 5.875287518842588E7,
          "Y_S_VB": 0.47736186718214646,
          "I_OD_DS": 2185718.264323158
        },
        "addSubtotal": 5,
        "pbu_name": "H WH",
        "subpbu_name": "H WH L10",
        "pbu_id": 2
      },
      {
        "subpbu_id": 8,
        "parent_id": 5,
        "values": {
          "I_OD_DO": -0.0,
          "Y_M_VBpp": 1.0,
          "I_O_BP": 0.0,
          "I_O_VBpp": -0.0,
          "Y_G_VBpp": 0.0,
          "I_OD_DG": -0.0,
          "Y_G_LFYP": null,
          "I_M_LFYP": 0.0,
          "I_G_BP": 0.0,
          "I_G_LFYP": null,
          "Y_M_LFYP": 0.0,
          "I_OD_RDp": 0.0,
          "Y_M_BP": 0.0,
          "Y_R_LFY": -1.43809239156E10,
          "I_M_LFY": 0.0,
          "I_R_VB": 1.0,
          "Y_R_VB": -0.08726713022742677,
          "Y_OD_DOA": 0.0,
          "I_O_AP": null,
          "Y_O_VBpp": 0.0,
          "I_OD_NRDp": 0.0,
          "Y_O_VBabs": 0.0,
          "I_R_B": -5.404504471638608E8,
          "I_R_A": null,
          "FY_S_VB": null,
          "I_G_AP": null,
          "Y_M_AP": 1.0,
          "Y_R_A": -4.064429757592936E9,
          "Y_R_B": -3.738207147624124E9,
          "I_G_VBabs": -0.0,
          "I_OD_DOA": -0.0,
          "I_OD_DGv": 0.0,
          "Y_O_LFYP": null,
          "Y_G_VBabs": 0.0,
          "addSubotal": 5,
          "I_M_BP": 0.0,
          "Y_OD_RDp": 0.0,
          "I_OD_DGm": -0.0,
          "I_R_LFY": -1.43809239156E10,
          "Y_M_LFY": 0.0,
          "Y_G_BP": 0.0,
          "Y_O_BP": 0.0,
          "Y_S_B": 0.0,
          "I_G_A": null,
          "FY_S_2019FY": null,
          "I_G_B": 0.0,
          "Y_O_LFY": null,
          "I_O_LFYP": null,
          "Y_OD_DG": 0.0,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 0.0,
          "I_G_LFY": null,
          "I_G_VBpp": -0.0,
          "I_O_A": null,
          "I_M_B": 0.0,
          "I_O_B": 0.0,
          "subPbuId": 8,
          "FY_S_B": 0.0,
          "Y_OD_DO": 0.0,
          "FY_S_M": null,
          "Y_G_A": 0.0,
          "I_O_LFY": null,
          "Y_OD_DS": 0.0,
          "Y_G_AP": 0.0,
          "Y_G_B": 0.0,
          "Y_OD_DGm": 0.0,
          "I_O_VBabs": -0.0,
          "Y_M_B": 0.0,
          "Y_O_A": 0.0,
          "I_M_VBpp": -0.0,
          "Y_G_LFY": null,
          "Y_O_AP": 0.0,
          "Y_M_A": -4.064429757592936E9,
          "Y_OD_NRDp": 0.0,
          "Y_O_B": 0.0,
          "Y_OD_DGv": -0.0,
          "Y_S_VB": null,
          "I_OD_DS": 0.0
        },
        "addSubtotal": 5,
        "pbu_name": "H WH",
        "subpbu_name": "H WH Consol. adj.",
        "pbu_id": 2
      },
      {
        "values": {
          "I_OD_DO": -3.4112286447720444E8,
          "Y_M_VBpp": 0.4789204566390429,
          "I_O_BP": 0.0694566325262698,
          "I_O_VBpp": -0.0694566325262698,
          "Y_G_VBpp": -0.017797725679086607,
          "I_OD_DG": -6.58180754436112E8,
          "Y_G_LFYP": 0.142577,
          "I_M_LFYP": 0.200518,
          "I_G_BP": 0.1340133528334245,
          "I_G_LFYP": 0.142577,
          "Y_M_LFYP": 0.200518,
          "I_OD_RDp": 1.5220936075954685E8,
          "Y_M_BP": 0.15313525921014223,
          "Y_R_LFY": 4.54291403169E10,
          "I_M_LFY": 9.109364426715E9,
          "I_R_VB": -1.0,
          "Y_R_VB": 0.10410603770599908,
          "Y_OD_DOA": -3.832951647882717E8,
          "I_O_AP": null,
          "Y_O_VBpp": -0.014902110261887334,
          "I_OD_NRDp": 1.4349030617011574E8,
          "Y_O_VBabs": -3.835012527436492E8,
          "I_R_B": 4.911307272897018E9,
          "I_R_A": null,
          "FY_S_VB": -1.0,
          "I_G_AP": null,
          "Y_M_AP": 0.6320557158491852,
          "Y_R_A": 3.628768489049001E10,
          "Y_R_B": 3.2866123045468468E10,
          "I_G_VBabs": -6.58180754436112E8,
          "I_OD_DOA": -3.6248108750644946E8,
          "I_OD_DGv": -6.58180754436112E8,
          "Y_O_LFYP": 0.065163,
          "Y_G_VBabs": -2.6635696540958577E8,
          "addSubotal": 1,
          "I_M_BP": 0.17411338481894034,
          "Y_OD_RDp": 6.810485146786879E7,
          "I_OD_DGm": 0.0,
          "I_R_LFY": 4.54291403169E10,
          "Y_M_LFY": 9.109364426715E9,
          "Y_G_BP": 0.11090879340750334,
          "Y_O_BP": 0.045962000912500175,
          "Y_S_B": 14.822795097117199,
          "I_G_A": null,
          "FY_S_2019FY": 48.143577,
          "I_G_B": 6.58180754436112E8,
          "Y_O_LFY": 2.9603257444034996E9,
          "I_O_LFYP": 0.065163,
          "Y_OD_DG": -2.6635696540958577E8,
          "I_M_AP": null,
          "I_M_A": null,
          "Y_S_M": 13.903695,
          "I_G_LFY": 6.477184211895E9,
          "I_G_VBpp": -0.1340133528334245,
          "I_O_A": null,
          "I_M_B": 8.55124333169979E8,
          "I_O_B": 3.4112286447720444E8,
          "subPbuId": 2,
          "FY_S_B": 26.8079165862219,
          "Y_OD_DO": -3.835012527436492E8,
          "FY_S_M": null,
          "Y_G_A": 3.3787850855458603E9,
          "I_O_LFY": 2.9603257444034996E9,
          "Y_OD_DS": -206087.95537744462,
          "Y_G_AP": 0.09311106772841674,
          "Y_G_B": 3.6451420509554467E9,
          "Y_OD_DGm": -6.458382612100763E8,
          "I_O_VBabs": -3.4112286447720444E8,
          "Y_M_B": 5.032962271800243E9,
          "Y_O_A": 1.1270915246625156E9,
          "I_M_VBpp": -0.17411338481894034,
          "Y_G_LFY": 6.477184211895E9,
          "Y_O_AP": 0.03105989065061284,
          "Y_M_A": 2.2935838649968327E10,
          "Y_OD_NRDp": -1.8504305084655476E8,
          "Y_O_B": 1.5105927774061646E9,
          "Y_OD_DGv": 3.7948129580049056E8,
          "Y_S_VB": -0.06200585591957273,
          "I_OD_DS": 2.1358223029244974E7
        },
        "subpbu_name": "Total (in-scope)"
      }
    ],
    "currency_name": "NTD",
    "columns": [
      {
        "title": "2021FY",
        "items": [
          {
            "items_name": "Sales Vol (M PCS)",
            "sub_items": [
              {
                "FY_S_M": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "FY_S_B": {
                  "type": "value",
                  "sub_item_name": "Budget"
                }
              },
              {
                "FY_S_VB": {
                  "css_type": "P",
                  "type": "percentage",
                  "sub_item_name": "vs. budget %"
                }
              },
              {
                "FY_S_2019FY": {
                  "css_type": "G",
                  "type": "value",
                  "sub_item_name": "2019FY"
                }
              }
            ]
          }
        ]
      },
      {
        "title": "YTM",
        "items": [
          {
            "items_name": "Sales Vol (M PCS)",
            "sub_items": [
              {
                "Y_S_M": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "Y_S_B": {
                  "type": "value",
                  "sub_item_name": "Budget"
                }
              },
              {
                "Y_S_B": {
                  "type": "value",
                  "sub_item_name": "Budget"
                }
              }
            ]
          },
          {
            "items_name": "Revenue",
            "sub_items": [
              {
                "Y_R_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "Y_R_B": {
                  "type": "value",
                  "sub_item_name": "Budget"
                }
              },
              {
                "Y_R_VB": {
                  "css_type": "P",
                  "type": "percentage",
                  "sub_item_name": "vs. budget %"
                }
              },
              {
                "Y_R_LFY": {
                  "css_type": "G",
                  "type": "value",
                  "sub_item_name": "2020FY"
                }
              }
            ]
          },
          {
            "items_name": "MOB",
            "sub_items": [
              {
                "Y_M_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "Y_M_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual MOB%"
                }
              },
              {
                "Y_M_B": {
                  "type": "value",
                  "sub_item_name": "Budget MOB"
                }
              },
              {
                "Y_M_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget MOB %"
                }
              },
              {
                "Y_M_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "Y_M_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY MOB"
                }
              },
              {
                "Y_M_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "GP",
            "sub_items": [
              {
                "Y_G_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "Y_G_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual GP %"
                }
              },
              {
                "Y_G_B": {
                  "type": "value",
                  "sub_item_name": "Budget GP"
                }
              },
              {
                "Y_G_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget GP %"
                }
              },
              {
                "Y_G_VBabs": {
                  "type": "value",
                  "sub_item_name": "vs. budget (abs)"
                }
              },
              {
                "Y_G_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "Y_G_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY GP"
                }
              },
              {
                "Y_G_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "OP B",
            "sub_items": [
              {
                "Y_O_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "Y_O_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual OP %"
                }
              },
              {
                "Y_O_B": {
                  "type": "value",
                  "sub_item_name": "Budget OP"
                }
              },
              {
                "Y_O_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget OP %"
                }
              },
              {
                "Y_O_VBabs": {
                  "type": "value",
                  "sub_item_name": "vs. budget (abs)"
                }
              },
              {
                "Y_O_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "Y_O_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY OP"
                }
              },
              {
                "Y_O_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "OP deviation bridge",
            "sub_items": [
              {
                "Y_OD_DGv": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔGP (vol)"
                }
              },
              {
                "Y_OD_DGm": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔGP (margin)"
                }
              },
              {
                "Y_OD_DG": {
                  "css_type": "BSP",
                  "type": "value",
                  "sub_item_name": "ΔGP"
                }
              },
              {
                "Y_OD_RDp": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔR&D platform"
                }
              },
              {
                "Y_OD_NRDp": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔNon-R&D platform OPEX"
                }
              },
              {
                "Y_OD_DOA": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔOP A"
                }
              },
              {
                "Y_OD_DS": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔShared expense"
                }
              },
              {
                "Y_OD_DO": {
                  "css_type": "BSP",
                  "type": "value",
                  "sub_item_name": "ΔOP B"
                }
              }
            ]
          }
        ]
      },
      {
        "title": "In-month",
        "items": [
          {
            "items_name": "Revenue",
            "sub_items": [
              {
                "I_R_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "I_R_B": {
                  "type": "value",
                  "sub_item_name": "Budget"
                }
              },
              {
                "I_R_VB": {
                  "css_type": "P",
                  "type": "percentage",
                  "sub_item_name": "vs. budget %"
                }
              },
              {
                "I_R_LFY": {
                  "css_type": "G",
                  "type": "value",
                  "sub_item_name": "2020FY"
                }
              }
            ]
          },
          {
            "items_name": "MOB",
            "sub_items": [
              {
                "I_M_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "I_M_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual MOB%"
                }
              },
              {
                "I_M_B": {
                  "type": "value",
                  "sub_item_name": "Budget MOB"
                }
              },
              {
                "I_M_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget MOB %"
                }
              },
              {
                "I_M_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "I_M_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY MOB"
                }
              },
              {
                "I_M_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "GP",
            "sub_items": [
              {
                "I_G_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "I_G_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual GP %"
                }
              },
              {
                "I_G_B": {
                  "type": "value",
                  "sub_item_name": "Budget GP"
                }
              },
              {
                "I_G_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget GP %"
                }
              },
              {
                "I_G_VBabs": {
                  "type": "value",
                  "sub_item_name": "vs. budget (abs)"
                }
              },
              {
                "I_G_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "I_G_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY GP"
                }
              },
              {
                "I_G_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "OP B",
            "sub_items": [
              {
                "I_O_A": {
                  "css_type": "B",
                  "type": "value",
                  "sub_item_name": "Actual"
                }
              },
              {
                "I_O_AP": {
                  "css_type": "B",
                  "type": "percentage",
                  "sub_item_name": "Actual OP %"
                }
              },
              {
                "I_O_B": {
                  "type": "value",
                  "sub_item_name": "Budget OP"
                }
              },
              {
                "I_O_BP": {
                  "type": "percentage",
                  "sub_item_name": "Budget OP %"
                }
              },
              {
                "I_O_VBabs": {
                  "type": "value",
                  "sub_item_name": "vs. budget (abs)"
                }
              },
              {
                "I_O_VBpp": {
                  "css_type": "BSP",
                  "type": "percentage",
                  "sub_item_name": "vs. budget (p.p.)"
                }
              },
              {
                "I_O_LFY": {
                  "type": "value",
                  "sub_item_name": "2020FY OP"
                }
              },
              {
                "I_O_LFYP": {
                  "css_type": "G",
                  "type": "percentage",
                  "sub_item_name": "2020FY %"
                }
              }
            ]
          },
          {
            "items_name": "OP deviation bridge",
            "sub_items": [
              {
                "I_OD_DGv": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔGP (vol)"
                }
              },
              {
                "I_OD_DGm": {
                  "css_type": "BSP",
                  "type": "value",
                  "sub_item_name": "ΔGP (margin)"
                }
              },
              {
                "I_OD_DG": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔGP"
                }
              },
              {
                "I_OD_RDp": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔR&D platform"
                }
              },
              {
                "I_OD_NRDp": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔNon-R&D platform OPEX"
                }
              },
              {
                "I_OD_DOA": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔOP A"
                }
              },
              {
                "I_OD_DS": {
                  "css_type": "BP",
                  "type": "value",
                  "sub_item_name": "ΔShared expense"
                }
              },
              {
                "I_OD_DO": {
                  "css_type": "BSP",
                  "type": "value",
                  "sub_item_name": "ΔOP B"
                }
              }
            ]
          }
        ]
      }
    ],
    "budget_version_id": 8,
    "actual_version": "Current Month:110",
    "time_id_e": 202107,
    "time_id_s": 202101,
    "budget_version": "Q2",
    "actual_version_id": 3,
    "currency_id": 3
  }
};