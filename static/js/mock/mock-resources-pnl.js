var actual_pbu_list = {
  "msg": null,
  "data": [
    {
      "pbu_id": 1,
      "subpbu_id": 2,
      "pbu_name": "Dell WH",
      "subpbu_name": "Dell WH L5"
    },
    {
      "pbu_id": 1,
      "subpbu_id": 1,
      "pbu_name": "Dell WH",
      "subpbu_name": "Dell WH Subtotal"
    }
  ]
};

var actual_version_list = {
  "msg": null,
  "data": [
    {
      "version_id": 3,
      "version_name": "110"
    },
    {
      "version_id": 4,
      "version_name": "120"
    },
    {
      "version_id": 5,
      "version_name": "130"
    }
  ]
};

var actual_application_history = {
  "msg": null,
  "data": {
    "time_id": 202106,
    "subpbu_id": 2,
    "actual_version": "Current Month:110",
    "pbu_id": 1,
    "items": [
      {
        "line_item": "0010000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Volume",
        "item_order": 100
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0020000",
        "item_level": 1,
        "is_required": "Y",
        "item_type": "QES",
        "item_name": "Production volume (K PCS)",
        "row": 7,
        "item_order": 200,
        "products": {
          "317": 996.425
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0030000",
        "item_level": 1,
        "is_required": "Y",
        "item_type": "QES",
        "item_name": "Normalized production volume (K PCS)",
        "row": 8,
        "item_order": 300,
        "products": {
          "317": 859.443
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0040000",
        "item_level": 1,
        "is_required": "Y",
        "item_type": "QES",
        "item_name": "Sales volume (K PCS)",
        "row": 9,
        "item_order": 400,
        "products": {
          "317": 789.79
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0050000",
        "item_level": 1,
        "is_required": "Y",
        "item_type": "QES",
        "item_name": "Current FY sales vol (latest forecast vs. budget, K PCS)",
        "row": 10,
        "item_order": 500,
        "products": {
          "317": 9082.811000000003
        }
      },
      {
        "line_item": "0060000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Production cost (NTD)",
        "item_order": 600
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0070100",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "COGS (production)",
        "row": 12,
        "item_order": 800,
        "products": {}
      },
      {
        "line_item": "0070101",
        "item_level": 1,
        "item_type": "C",
        "item_name": "BOM COGS",
        "item_order": 900
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080100",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "BOM GR",
        "row": 14,
        "item_order": 1200,
        "products": {
          "317": 1500768430.0410583
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080101",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Buy-sell",
        "row": 15,
        "item_order": 1300,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080102",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "GTK",
        "row": 16,
        "item_order": 1400,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080103",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "TK",
        "row": 17,
        "item_order": 1500,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080104",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "BoM Savings",
        "row": 18,
        "item_order": 1600,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080105",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "BoM Savings - Proc. targets",
        "row": 19,
        "item_order": 1700,
        "products": {
          "317": 22265184.506829012
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080106",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "BoM Savings - R&D targets",
        "row": 20,
        "item_order": 1800,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0080107",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Scrap sales",
        "row": 21,
        "item_order": 1900,
        "products": {
          "317": -15805095.989097333
        }
      },
      {
        "line_item": "0280000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "MOH/MVA",
        "item_order": 2000
      },
      {
        "item_flag": "negative",
        "line_item": "0090100",
        "item_level": 2,
        "item_type": "N",
        "item_name": "MOH/MVA expense",
        "item_order": 2100,
        "products": {
          "317": 94306244.0087622
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090101",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "DL",
        "row": 24,
        "item_order": 2200,
        "products": {
          "317": 58210133.62173383
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090102",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "IDL",
        "row": 25,
        "item_order": 2300,
        "products": {
          "317": 6856064.418522852
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090103",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "G&A",
        "row": 26,
        "item_order": 2400,
        "products": {
          "317": 37527306.66654203
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090104",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Government Subsidy",
        "row": 27,
        "item_order": 2500,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090105",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings",
        "row": 28,
        "item_order": 2600,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090106",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - targets",
        "row": 29,
        "item_order": 2700,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090109",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - DL",
        "row": 30,
        "item_order": 2706,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090110",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - IDL",
        "row": 31,
        "item_order": 2712,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090111",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - G&A",
        "row": 32,
        "item_order": 2718,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090112",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - Others",
        "row": 33,
        "item_order": 2724,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090107",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "MFG Savings - Government subsidy",
        "row": 34,
        "item_order": 2730,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0090108",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Inventory write-off reversal",
        "row": 35,
        "item_order": 2760,
        "products": {}
      },
      {
        "line_item": "0100000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Per unit cost",
        "item_order": 2800
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0100100",
        "item_level": 2,
        "is_required": "PEU",
        "item_type": "PEU",
        "item_name": "Margin on BOM%",
        "row": 37,
        "item_order": 2900,
        "products": {}
      },
      {
        "item_flag": "negative",
        "line_item": "0100200",
        "item_level": 2,
        "item_type": "NES",
        "item_name": "Total MVA / N. production vol (NTD)",
        "item_order": 3000,
        "products": {
          "317": 109.7294922511
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0100300",
        "item_level": 2,
        "item_type": "NES",
        "item_name": "DL MVA / N. production vol (NTD)",
        "item_order": 3100,
        "products": {
          "317": 67.730068918746
        }
      },
      {
        "line_item": "0110000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "P&L (NTD)",
        "item_order": 3200
      },
      {
        "item_flag": "positive",
        "line_item": "0120000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Sales revenue (net)",
        "item_order": 3300,
        "products": {
          "317": 1870480059.1188176
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120100",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Sales revenue",
        "row": 42,
        "item_order": 3400,
        "products": {
          "317": 1763993698.2609575
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120200",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "NRE income",
        "row": 43,
        "item_order": 3500,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120300",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Customer claim",
        "row": 44,
        "item_order": 3530,
        "products": {
          "317": 106486360.85786
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120500",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "KAM-target",
        "row": 45,
        "item_order": 3545,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120600",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Revenue-resell aging stock",
        "row": 46,
        "item_order": 3552,
        "products": {
          "317": 301725.35646218044
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0120400",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Shared revenue",
        "row": 47,
        "item_order": 3560,
        "products": {
          "317": 0
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0130000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "COGS (net)",
        "item_order": 3600,
        "products": {
          "317": 1595074674.0498207
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0130100",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "COGS",
        "row": 49,
        "item_order": 3630,
        "products": {
          "317": 1610879770.038918
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0130300",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Inventory Valuation Loss or Gain",
        "row": 50,
        "item_order": 3637,
        "products": {
          "317": -567213.4721728523
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0130400",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Loss on Inventory Obsolescence",
        "row": 51,
        "item_order": 3645,
        "products": {
          "317": -8021772.582325835
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0130500",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Scrap sales",
        "row": 52,
        "item_order": 3652,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0130200",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Shared COGS",
        "row": 53,
        "item_order": 3660,
        "products": {
          "317": 0
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0140000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Gross profit",
        "item_order": 3700,
        "products": {
          "317": 275405385.0689969
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0210000",
        "item_level": 2,
        "item_type": "PEU",
        "item_name": "Gross profit %",
        "item_order": 3750,
        "products": {
          "317": 0
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0150000",
        "item_level": 1,
        "item_type": "N",
        "item_name": "R&D, SG&A expenses",
        "item_order": 3800,
        "products": {
          "317": 201555107.86146328
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150100",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses",
        "row": 57,
        "item_order": 3900,
        "products": {
          "317": 58859738.86837626
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150101",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses - FTE/Non-FTE",
        "row": 58,
        "item_order": 4000,
        "products": {
          "317": 24014830.547710627
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150102",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses - Project",
        "row": 59,
        "item_order": 4100,
        "products": {
          "317": 21263720.701557644
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150103",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "R&D Expenses - Project exp/NRE reclassification",
        "row": 60,
        "item_order": 4200,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150104",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses - MFG platform",
        "row": 61,
        "item_order": 4300,
        "products": {
          "317": 6186283.7610180145
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150108",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "R&D Expenses - PBU",
        "row": 62,
        "item_order": 4400,
        "products": {
          "317": 7262472.990399924
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150106",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses - FTE/Non-FTE - targets",
        "row": 63,
        "item_order": 4500,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150107",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "R&D Expenses - Project - targets",
        "row": 64,
        "item_order": 4600,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150109",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "R&D capitalization",
        "row": 65,
        "item_order": 4625,
        "products": {
          "317": -15805095.989097333
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0150105",
        "item_level": 3,
        "item_type": "N",
        "item_name": "R&D Expense - Shared expenses",
        "item_order": 4650,
        "products": {
          "317": 132430.86769005284
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150200",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Sales expense ",
        "row": 67,
        "item_order": 4700,
        "products": {
          "317": 132037818.23284322
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150201",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Sales expenses - MFG platform",
        "row": 68,
        "item_order": 4800,
        "products": {
          "317": 131961545.50642864
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150202",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Sales expenses - PBU",
        "row": 69,
        "item_order": 4900,
        "products": {
          "317": 0
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150204",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Bad debt expense",
        "row": 70,
        "item_order": 4950,
        "products": {
          "317": 0
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0150203",
        "item_level": 3,
        "item_type": "N",
        "item_name": "Sales expenses - Shared expenses",
        "item_order": 5000,
        "products": {
          "317": 76272.72641457617
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150300",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Administrative expense",
        "row": 72,
        "item_order": 5100,
        "products": {
          "317": 10657550.760243816
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150301",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Administrative expenses - MFG platform",
        "row": 73,
        "item_order": 5200,
        "products": {
          "317": 3283422.8340301183
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0150302",
        "item_level": 3,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Administrative expenses - PBU",
        "row": 74,
        "item_order": 5300,
        "products": {
          "317": 855159.3818273755
        }
      },
      {
        "item_flag": "negative",
        "line_item": "0150303",
        "item_level": 3,
        "item_type": "N",
        "item_name": "Administrative expenses - Shared expenses",
        "item_order": 5400,
        "products": {
          "317": 6518968.544386323
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0290000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Over-delivery & leakage to be recovered",
        "item_order": 5412,
        "products": {
          "317": 0.147237808672
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0290100",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Q1 over-delivery",
        "row": 77,
        "item_order": 5425,
        "products": {
          "317": 0.043078753475
        }
      },
      {
        "col": 2,
        "item_flag": "positive",
        "line_item": "0290200",
        "item_level": 2,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Q1 Leakage to be recovered",
        "row": 78,
        "item_order": 5437,
        "products": {
          "317": 0.035264834688
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0250000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Operating profit A - (excl. shared rev / exp)",
        "item_order": 5450,
        "products": {
          "317": 80577949.34602459
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0220000",
        "item_level": 2,
        "item_type": "PEU",
        "item_name": "Operating profit A %",
        "item_order": 5467,
        "products": {}
      },
      {
        "line_item": "0270000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "PBU Uncontrollable Accounts",
        "item_order": 5484
      },
      {
        "item_flag": "positive",
        "line_item": "0160000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Operating profit B - (incl. shared rev / exp)",
        "item_order": 5500,
        "products": {
          "317": 73850277.20753363
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160100",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Other expense (income)",
        "row": 83,
        "item_order": 5600,
        "products": {
          "317": -10464971.01118958
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160101",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Other expense (income) - MFG platform",
        "row": 84,
        "item_order": 5700,
        "products": {
          "317": -10464971.01118958
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160102",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Other expense (income) - Procurement platform",
        "row": 85,
        "item_order": 5800,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160103",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Other expense (income) - R&D platform",
        "row": 86,
        "item_order": 5900,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160104",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Other expense (income) - PBU",
        "row": 87,
        "item_order": 6000,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160105",
        "item_level": 3,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Other expense (income) - D Group level",
        "row": 88,
        "item_order": 6100,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160200",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Corporate Allocation",
        "row": 89,
        "item_order": 6200,
        "products": {
          "317": 9258477.75858301
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0160300",
        "item_level": 2,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "ICC",
        "row": 90,
        "item_order": 6300,
        "products": {
          "317": 9094600.387502152
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0170000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Profit before tax",
        "item_order": 6400,
        "products": {
          "317": 65962170.07263805
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0230000",
        "item_level": 2,
        "item_type": "PEU",
        "item_name": "Profit before tax %",
        "item_order": 6450,
        "products": {}
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0180000",
        "item_level": 1,
        "is_required": "Y",
        "item_type": "N",
        "item_name": "Income tax",
        "row": 93,
        "item_order": 6500,
        "products": {
          "317": 14807351.59491934
        }
      },
      {
        "col": 2,
        "item_flag": "negative",
        "line_item": "0300000",
        "item_level": 1,
        "is_required": "N",
        "item_type": "N",
        "item_name": "Income tax - Gov't subsidy",
        "row": 94,
        "item_order": 6550,
        "products": {
          "317": 0.027348497103
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0190000",
        "item_level": 1,
        "item_type": "NME",
        "item_name": "Net income",
        "item_order": 6600,
        "products": {
          "317": 51154818.47771871
        }
      },
      {
        "item_flag": "positive",
        "line_item": "0240000",
        "item_level": 2,
        "item_type": "PEU",
        "item_name": "Net income %",
        "item_order": 6700,
        "products": {}
      }
    ],
    "actual_version_id": 3,
    "products": [
      {
        "name": "Procuct XX",
        "key": "317"
      }
    ],
    "status": "3"
  },
  "result": true
};

var actual_cache = {
  "msg": null,
  "data": {
    "time_id": "202103",
    "pbu_id": 1,
    "subpbu_id": 1,
    "actual_version_id": 2,
    "status": 0
  }
};

var actual_publish = {
  "msg": null,
  "data": {
    "time_id": "202103",
    "pbu_id": 1,
    "subpbu_id": 1,
    "actual_version_id": 2,
    "status": 3
  }
};

var actual_recall = {
  "msg": null,
  "data": {
    "time_id": "202103",
    "pbu_id": 1,
    "subpbu_id": 1,
    "actual_version_id": 2,
    "status": 1
  }
};

var actual_store = {
  "data": {
    "actual_version_id": 2,
    "pbu_id": 1,
    "status": 1,
    "subpbu_id": 1,
    "time_id": "202103"
  },
  "msg": null,
  "result": true
};

var pnl_pbu_list = {
  "msg": null,
  "data": [
    {
      "subpbu_id": 1, "pbu_name": "Dell WH", "subpbu_name": "Dell WH Subtotal", "pbu_id": 1
    },
    {
      "subpbu_id": 2,
      "pbu_name": "Dell WH",
      "subpbu_name": "Dell WH L5",
      "pbu_id": 1
    },
    {
      "subpbu_id": 3, "pbu_name": "Dell WH", "subpbu_name": "Dell WH L6", "pbu_id": 1
    },
    {
      "subpbu_id": 5,
      "pbu_name": "HP WH",
      "subpbu_name": "HP WH Subtotal",
      "pbu_id": 2
    },
    {
      "subpbu_id": 6, "pbu_name": "HP WH", "subpbu_name": "HP WH L5", "pbu_id": 2
    },
    {
      "subpbu_id": 7,
      "pbu_name": "HP WH",
      "subpbu_name": "HP WH L6",
      "pbu_id": 2
    },
    {"subpbu_id": 9, "pbu_name": "HP WH", "subpbu_name": "HP WH L10", "pbu_id": 2},
    {
      "subpbu_id": 10,
      "pbu_name": "Lenovo WH",
      "subpbu_name": "Lenovo WH Subtotal",
      "pbu_id": 3
    },
    {
      "subpbu_id": 11, "pbu_name": "Lenovo WH", "subpbu_name": "Lenovo WH L5", "pbu_id": 3
    },
    {
      "subpbu_id": 12,
      "pbu_name": "Lenovo WH",
      "subpbu_name": "Lenovo WH L6",
      "pbu_id": 3
    },
    {
      "subpbu_id": 13, "pbu_name": "Lenovo WH", "subpbu_name": "Lenovo WH L10", "pbu_id": 3
    },
    {
      "subpbu_id": 15,
      "pbu_name": "Monitor",
      "subpbu_name": "Monitor Subtotal",
      "pbu_id": 4
    },
    {
      "subpbu_id": 16, "pbu_name": "Monitor", "subpbu_name": "MNT L5", "pbu_id": 4
    },
    {
      "subpbu_id": 17,
      "pbu_name": "Monitor",
      "subpbu_name": "MNT L6",
      "pbu_id": 4
    },
    {"subpbu_id": 18, "pbu_name": "Monitor", "subpbu_name": "MNT L10", "pbu_id": 4},
    {
      "subpbu_id": 20,
      "pbu_name": "WH Huawei",
      "subpbu_name": "WH Huawei Subtotal",
      "pbu_id": 5
    },
    {
      "subpbu_id": 21, "pbu_name": "WH Huawei", "subpbu_name": "Huawei WH L5", "pbu_id": 5
    },
    {
      "subpbu_id": 22,
      "pbu_name": "WH Huawei",
      "subpbu_name": "Huawei WH L10",
      "pbu_id": 5
    },
    {
      "subpbu_id": 24, "pbu_name": "Brazil", "subpbu_name": "Brazil", "pbu_id": 6
    },
    {
      "subpbu_id": 25,
      "pbu_name": "G2",
      "subpbu_name": "G2",
      "pbu_id": 7
    },
    {
      "subpbu_id": 26, "pbu_name": "JSD", "subpbu_name": "JSD", "pbu_id": 8
    },
    {
      "subpbu_id": 27,
      "pbu_name": "FMX",
      "subpbu_name": "FMX",
      "pbu_id": 9
    },
    {
      "subpbu_id": 28, "pbu_name": "Czech", "subpbu_name": "Czech", "pbu_id": 10
    },
    {
      "subpbu_id": 29,
      "pbu_name": "Intel",
      "subpbu_name": "Intel",
      "pbu_id": 11
    },
    {
      "subpbu_id": 30, "pbu_name": "GSSD Subtotal", "subpbu_name": "GSSD Subtotal", "pbu_id": 12
    },
    {
      "subpbu_id": 31,
      "pbu_name": "Cartridge LH",
      "subpbu_name": "Cartridge LH",
      "pbu_id": 13
    },
    {
      "subpbu_id": 32, "pbu_name": "AP5", "subpbu_name": "AP5", "pbu_id": 14
    },
    {
      "subpbu_id": 33,
      "pbu_name": "SSBU",
      "subpbu_name": "SSBU",
      "pbu_id": 15
    },
    {
      "subpbu_id": 34, "pbu_name": "Resideo", "subpbu_name": "Resideo", "pbu_id": 16
    },
    {
      "subpbu_id": 35,
      "pbu_name": "Adj-others",
      "subpbu_name": "Adj-others",
      "pbu_id": 17
    },
    {
      "subpbu_id": 36,
      "pbu_name": "Module Capacity",
      "subpbu_name": "Module Capacity",
      "pbu_id": 18
    },
    {
      "subpbu_id": 37,
      "pbu_name": "WH Gov. Subsidy",
      "subpbu_name": "WH Gov. Subsidy",
      "pbu_id": 19
    },
    {"subpbu_id": 38, "pbu_name": "In-scope Consol. adj.", "subpbu_name": "In-scope Consol. adj.", "pbu_id": 20}
  ],
  "result": true
};

var pnl_version_list_actual = {
  "msg": null,
  "data": [
    {
      "version_id": 3,
      "version_name": "110"
    },
    {
      "version_id": 4,
      "version_name": "120"
    },
    {
      "version_id": 5,
      "version_name": "130"
    }
  ],
  "result": true
};

var pnl_version_list_budget = {
  "msg": null,
  "data": [
    {
      "version_id": 3,
      "version_name": "110"
    },
    {
      "version_id": 4,
      "version_name": "120"
    },
    {
      "version_id": 5,
      "version_name": "130"
    }
  ]
};

var pnl_action_item_save = {
  "msg": null,
  "data": {
    "time_id": "202103",
    "pbu_id": 1,
    "subpbu_id": 1,
    "actual_version_id": 1,
    "budget_version_id": 2,
    "line_item": "0020000",
    "note": "虧錢"
  }
};

var pnl_deviation_explanation_save = {
  "msg": null,
  "data": {
    "time_id": "202103",
    "pbu_id": 1,
    "subpbu_id": 1,
    "actual_version_id": 1,
    "budget_version_id": 2,
    "line_item": "0020000",
    "note": "虧錢"
  }
};

var pnl_report = {
  "msg": null,
  "data": {
    "time_id": 202105,
    "subpbu_id": 2,
    "currency_name": "NTD",
    "budget_version_id": 8,
    "actual_version": "110",
    "budget_version": "Q2",
    "pbu_id": 1,
    "items": [
      {
        "line_item": "0010000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Volume",
        "item_order": 100
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0020000",
        "ytm": {
          "actual": 4053,
          "abs": 1032.6999999999998,
          "percentage": 0.34191967685329266,
          "budget": 3020.3
        },
        "item_level": 1,
        "item_type": "QS",
        "decimal_place": 0,
        "action_item_rich_text": "",
        "item_name": "Production volume (K PCS)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 200,
        "in_month": {
          "actual": 751,
          "abs": 121.79999999999995,
          "percentage": 0.19357914812460258,
          "products": {
            "287": 751
          },
          "budget": 629.2
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0030000",
        "ytm": {
          "actual": 4016.98878,
          "abs": 1455.4734749582003,
          "percentage": 0.5682079947339801,
          "budget": 2561.5153050418
        },
        "item_level": 1,
        "item_type": "QS",
        "decimal_place": 0,
        "action_item_rich_text": "",
        "item_name": "Normalized production volume (K PCS)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 300,
        "in_month": {
          "actual": 659,
          "abs": 124.07713481580004,
          "percentage": 0.23195332054664414,
          "products": {
            "287": 659
          },
          "budget": 534.9228651842
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0040000",
        "ytm": {
          "actual": 5500.553,
          "abs": 2413.2529999999997,
          "percentage": 0.7816710394195574,
          "budget": 3087.3
        },
        "item_level": 1,
        "item_type": "QS",
        "decimal_place": 0,
        "action_item_rich_text": "",
        "item_name": "Sales volume (K PCS)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 400,
        "in_month": {
          "actual": 806,
          "abs": 179.79999999999995,
          "percentage": 0.28712871287128705,
          "products": {
            "287": 806
          },
          "budget": 626.2
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0050000",
        "ytm": {
          "actual": 8932,
          "abs": 432.39999999999964,
          "percentage": 0.050872982257988565,
          "budget": 8499.6
        },
        "item_level": 0,
        "item_type": "QS",
        "action_item_rich_text": "",
        "item_name": "Current FY sales vol (latest forecast vs. budget, K PCS)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 500,
        "in_month": {
          "actual": 8932,
          "abs": 432.39999999999964,
          "percentage": 0.050872982257988565,
          "products": {
            "287": 8932
          },
          "budget": 8499.6
        }
      },
      {
        "line_item": "0060000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Production cost",
        "item_order": 600
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0070100",
        "ytm": {
          "actual": 0,
          "abs": -6727873569.789731,
          "percentage": -1,
          "budget": 6727873569.789731
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "COGS (production)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 800,
        "in_month": {
          "abs": -1302765384.2395592,
          "percentage": -1,
          "products": {},
          "budget": 1302765384.2395592
        }
      },
      {
        "line_item": "0070101",
        "item_level": 1,
        "item_type": "C",
        "item_name": "BOM COGS",
        "item_order": 900
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080100",
        "ytm": {
          "actual": 1549773669,
          "abs": -4827517829.882618,
          "percentage": -0.7569855997218349,
          "budget": 6377291498.882618
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "BOM GR",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1200,
        "in_month": {
          "actual": 1549773669,
          "abs": 326515778.6900053,
          "percentage": 0.26692309224121213,
          "products": {
            "287": 1549773669
          },
          "budget": 1223257890.3099947
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080101",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Buy-sell",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1300,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080102",
        "ytm": {
          "actual": 0,
          "abs": -5804768945.79425,
          "percentage": -1,
          "budget": 5804768945.79425
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "GTK",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1400,
        "in_month": {
          "abs": -1101840793.835015,
          "percentage": -1,
          "products": {},
          "budget": 1101840793.835015
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080103",
        "ytm": {
          "actual": 0,
          "abs": -616631090.47697,
          "percentage": -1,
          "budget": 616631090.47697
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "TK",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1500,
        "in_month": {
          "abs": -128435737.05526926,
          "percentage": -1,
          "products": {},
          "budget": 128435737.05526926
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080104",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "BoM Savings",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1600,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080105",
        "ytm": {
          "actual": 0,
          "abs": 8768082.39856452,
          "percentage": 1,
          "budget": -8768082.39856452
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "BoM Savings - Proc. targets",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1700,
        "in_month": {
          "abs": -892490.0727780575,
          "percentage": -1,
          "products": {},
          "budget": 892490.0727780575
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080106",
        "ytm": {
          "actual": 0,
          "abs": 1565899.0214651707,
          "percentage": 1,
          "budget": -1565899.0214651707
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "BoM Savings - R&D targets",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1800,
        "in_month": {
          "abs": 543810.2079994847,
          "percentage": 1,
          "products": {},
          "budget": -543810.2079994847
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0080107",
        "ytm": {
          "actual": -22706040,
          "abs": 11068515.96857129,
          "percentage": 0.327717586542693,
          "budget": -33774555.96857129
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Scrap sales",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 1900,
        "in_month": {
          "actual": -12061777,
          "abs": -4694456.554931834,
          "percentage": -0.637199995566149,
          "products": {
            "287": -12061777
          },
          "budget": -7367320.445068166
        }
      },
      {
        "line_item": "0280000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "MOH/MVA",
        "item_order": 2000
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090100",
        "ytm": {
          "actual": 615611203.42054,
          "abs": 265029132.51342726,
          "percentage": 0.7559688715046902,
          "budget": 350582070.9071127
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MOH/MVA expense",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2100,
        "in_month": {
          "actual": 96531828,
          "abs": 17024334.070435554,
          "percentage": 0.2141223830487901,
          "products": {
            "287": 96531828
          },
          "budget": 79507493.92956445
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090101",
        "ytm": {
          "actual": 303478432.02158,
          "abs": 98275668.02991313,
          "percentage": 0.4789198065280643,
          "budget": 205202763.99166685
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "DL",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2200,
        "in_month": {
          "actual": 52128806,
          "abs": 7893548.516102895,
          "percentage": 0.17844472859633229,
          "products": {
            "287": 52128806
          },
          "budget": 44235257.483897105
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090102",
        "ytm": {
          "actual": 44263700.089,
          "abs": 15845196.59358824,
          "percentage": 0.557566185571541,
          "budget": 28418503.49541176
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "IDL",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2300,
        "in_month": {
          "actual": 7215928,
          "abs": 1651696.5157641089,
          "percentage": 0.2968418047386338,
          "products": {
            "287": 7215928
          },
          "budget": 5564231.484235891
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090103",
        "ytm": {
          "actual": 270249275.30996,
          "abs": 132275043.8478432,
          "percentage": 0.9586938259856268,
          "budget": 137974231.4621168
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "G&A",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2400,
        "in_month": {
          "actual": 39567298,
          "abs": 2714440.366986677,
          "percentage": 0.07365617054768209,
          "products": {
            "287": 39567298
          },
          "budget": 36852857.63301332
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090104",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Government Subsidy",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2500,
        "in_month": {
          "actual": 0,
          "abs": 0,
          "products": {
            "287": 0
          },
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090105",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2600,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090106",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - targets",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2700,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090109",
        "ytm": {
          "actual": -1565455,
          "abs": 2778408.776532055,
          "percentage": 0.6396169215854672,
          "budget": -4343863.776532055
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - DL",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2706,
        "in_month": {
          "actual": -1565455,
          "abs": 155316.85396234877,
          "percentage": 0.09025999211034699,
          "products": {
            "287": -1565455
          },
          "budget": -1720771.8539623488
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090110",
        "ytm": {
          "actual": -39809,
          "abs": 172.71326431779744,
          "percentage": 0.004319806486930555,
          "budget": -39981.7132643178
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - IDL",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2712,
        "in_month": {
          "actual": -39809,
          "abs": -20018.976403692803,
          "percentage": -1.0115691022939624,
          "products": {
            "287": -39809
          },
          "budget": -19790.023596307197
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090111",
        "ytm": {
          "actual": -774940,
          "abs": 5588492.005373001,
          "percentage": 0.8782198035045122,
          "budget": -6363432.005373001
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - G&A",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2718,
        "in_month": {
          "actual": -774940,
          "abs": 2096745.0964437635,
          "percentage": 0.7301445061090891,
          "products": {
            "287": -774940
          },
          "budget": -2871685.0964437635
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090112",
        "ytm": {
          "actual": 0,
          "abs": 10211734.930157797,
          "percentage": 1,
          "budget": -10211734.930157797
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - Others",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2724,
        "in_month": {
          "abs": 2478190.080823978,
          "percentage": 1,
          "products": {},
          "budget": -2478190.080823978
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090107",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "MFG Savings - Government subsidy",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2730,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0090108",
        "ytm": {
          "actual": 0,
          "abs": 54415.6167554784,
          "percentage": 1,
          "budget": -54415.6167554784
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Inventory write-off reversal",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2760,
        "in_month": {
          "abs": 54415.6167554784,
          "percentage": 1,
          "products": {},
          "budget": -54415.6167554784
        }
      },
      {
        "line_item": "0100000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "Per unit cost",
        "item_order": 2800
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0100100",
        "ytm": {
          "actual": 0,
          "abs": -20903.979141883287,
          "percentage": -1,
          "budget": 20903.979141883287
        },
        "item_level": 2,
        "item_type": "PEU",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Margin on BOM%",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 2900,
        "in_month": {
          "abs": -4614.642043658208,
          "percentage": -1,
          "products": {},
          "budget": 4614.642043658208
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0100200",
        "ytm": {
          "actual": 153.25191010878052,
          "abs": 16.38680131280094,
          "percentage": 0.11972957503163366,
          "budget": 136.86510879597958
        },
        "item_level": 2,
        "item_type": "NS",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Total MVA / N. production vol",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3000,
        "in_month": {
          "actual": 146.48228831562975,
          "abs": -2.151279445931806,
          "percentage": -0.014473711950337458,
          "products": {
            "287": 146.48228831562975
          },
          "budget": 148.63356776156155
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0100300",
        "ytm": {
          "actual": 75.54873778402238,
          "abs": -4.561173559379981,
          "percentage": -0.056936444977798956,
          "budget": 80.10991134340236
        },
        "item_level": 2,
        "item_type": "NS",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "DL MVA / N. production vol",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3100,
        "in_month": {
          "actual": 79.10289226100151,
          "abs": -3.5917546927508823,
          "percentage": -0.04343394433692423,
          "products": {
            "287": 79.10289226100151
          },
          "budget": 82.6946469537524
        }
      },
      {
        "line_item": "0110000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "P&L",
        "item_order": 3200
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120000",
        "ytm": {
          "actual": 15009253022.483639,
          "abs": 7515603560.819555,
          "percentage": 1.0029296939051906,
          "budget": 7493649461.6640835
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales revenue (net)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3300,
        "in_month": {
          "actual": 1886936568,
          "abs": 421630281.47610044,
          "percentage": 0.2877420818799057,
          "products": {
            "287": 1886936568
          },
          "budget": 1465306286.5238996
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120100",
        "ytm": {
          "actual": 14220731976.70788,
          "abs": 6774111299.587505,
          "percentage": 0.9096893199355321,
          "budget": 7446620677.120375
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales revenue",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3400,
        "in_month": {
          "actual": 1778309628,
          "abs": 344910693.95519996,
          "percentage": 0.24062435499510423,
          "products": {
            "287": 1778309628
          },
          "budget": 1433398934.0448
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120200",
        "ytm": {
          "actual": 438285600,
          "abs": 438285600,
          "budget": 0
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "NRE income",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3500,
        "in_month": {
          "actual": 560435,
          "abs": 560435,
          "products": {
            "287": 560435
          },
          "budget": 0
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120300",
        "ytm": {
          "actual": 166839013,
          "abs": 166839013
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Customer claim",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3530,
        "in_month": {
          "actual": 52066505,
          "abs": 52066505,
          "products": {
            "287": 52066505
          }
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120500",
        "ytm": {
          "actual": 56000000,
          "abs": 8971215.45629076,
          "percentage": 0.19076009604187794,
          "budget": 47028784.54370924
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "KAM-target",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3545,
        "in_month": {
          "actual": 56000000,
          "abs": 24092647.5209005,
          "percentage": 0.7550813730685452,
          "products": {
            "287": 56000000
          },
          "budget": 31907352.4790995
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120600",
        "ytm": {
          "actual": 0,
          "abs": 0
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Revenue-resell aging stock",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3552,
        "in_month": {
          "products": {}
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0120400",
        "ytm": {
          "actual": 127396432.21998,
          "abs": 127396432.21998
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Shared revenue",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3560,
        "in_month": {
          "actual": 0,
          "abs": 0,
          "products": {
            "287": 0
          }
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130000",
        "ytm": {
          "actual": 8449238246.732039,
          "abs": 1721364676.9423084,
          "percentage": 0.25585568145510007,
          "budget": 6727873569.789731
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "COGS (net)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3600,
        "in_month": {
          "actual": 1646305498,
          "abs": 343540113.7604408,
          "percentage": 0.2637006769725998,
          "products": {
            "287": 1646305498
          },
          "budget": 1302765384.2395592
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130100",
        "ytm": {
          "actual": 3540385117,
          "abs": 3540385117
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "COGS",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3630,
        "in_month": {
          "actual": 1658367275,
          "abs": 1658367275,
          "products": {
            "287": 1658367275
          }
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130300",
        "ytm": {
          "actual": 0,
          "abs": 0
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Inventory Valuation Loss or Gain",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3637,
        "in_month": {
          "products": {}
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130400",
        "ytm": {
          "actual": 0,
          "abs": 0
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Loss on Inventory Obsolescence",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3645,
        "in_month": {
          "products": {}
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130500",
        "ytm": {
          "actual": -22706040,
          "abs": -22706040
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Scrap sales",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3652,
        "in_month": {
          "actual": -12061777,
          "abs": -12061777,
          "products": {
            "287": -12061777
          }
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0130200",
        "ytm": {
          "actual": 110741447,
          "abs": 110741447
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Shared COGS",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3660,
        "in_month": {
          "actual": 0,
          "abs": 0,
          "products": {
            "287": 0
          }
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0140000",
        "ytm": {
          "actual": 6560014775.19582,
          "abs": 5794238883.321466,
          "percentage": 7.566494250869118,
          "budget": 765775891.8743534
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Gross profit",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3700,
        "in_month": {
          "actual": 240631070,
          "abs": 78090167.71565968,
          "percentage": 0.48043394996696237,
          "products": {
            "287": 240631070
          },
          "budget": 162540902.28434032
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0210000",
        "ytm": {
          "actual": 0.4370647070423168,
          "abs": 0.33487472660729645,
          "percentage": 3.2769820013835274,
          "budget": 0.10218998043502034
        },
        "item_level": 2,
        "item_type": "PEU",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Gross profit %",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3750,
        "in_month": {
          "actual": 0.1275247266287544,
          "abs": 0.01659849654348597,
          "percentage": 0.14963545169367778,
          "products": {
            "287": 0.1275247266287544
          },
          "budget": 0.11092623008526842
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150000",
        "ytm": {
          "actual": 1048641689,
          "abs": 591449595.6002675,
          "percentage": 1.293656657975384,
          "budget": 457192093.3997325
        },
        "item_level": 1,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D, SG&A expenses",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3800,
        "in_month": {
          "actual": 137868642,
          "abs": 60580450.147213966,
          "percentage": 0.7838254291496949,
          "products": {
            "287": 137868642
          },
          "budget": 77288191.85278603
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150100",
        "ytm": {
          "actual": 628786865,
          "abs": 374413124.9192111,
          "percentage": 1.4719016389046202,
          "budget": 254373740.0807889
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 3900,
        "in_month": {
          "actual": 55644781,
          "abs": 5729338.949270308,
          "percentage": 0.1147808917217944,
          "products": {
            "287": 55644781
          },
          "budget": 49915442.05072969
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150101",
        "ytm": {
          "actual": 224466573.42068,
          "abs": 106848324.34425138,
          "percentage": 0.9084332166415867,
          "budget": 117618249.0764286
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - FTE/Non-FTE",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4000,
        "in_month": {
          "actual": 28930022,
          "abs": 4778030.12140445,
          "percentage": 0.19783172110284328,
          "products": {
            "287": 28930022
          },
          "budget": 24151991.87859555
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150102",
        "ytm": {
          "actual": 242142452.1914,
          "abs": 118071397.16528179,
          "percentage": 0.9516433719405913,
          "budget": 124071055.0261182
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - Project",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4100,
        "in_month": {
          "actual": 17074733,
          "abs": -5324769.343493622,
          "percentage": -0.23771819845990044,
          "products": {
            "287": 17074733
          },
          "budget": 22399502.34349362
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150103",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - Project exp/NRE reclassification",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4200,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150104",
        "ytm": {
          "actual": 61831504,
          "abs": 52335324.26744178,
          "percentage": 5.511197738602926,
          "budget": 9496179.732558217
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - MFG platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4300,
        "in_month": {
          "actual": 2721950,
          "abs": -5483.254469737411,
          "percentage": -0.0020104083063266216,
          "products": {
            "287": 2721950
          },
          "budget": 2727433.2544697374
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150108",
        "ytm": {
          "actual": 13786440,
          "abs": -366161.80147640035,
          "percentage": -0.025872401881483185,
          "budget": 14152601.8014764
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - PBU",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4400,
        "in_month": {
          "actual": 6747956,
          "abs": -156797.6237031808,
          "percentage": -0.02270864859897587,
          "products": {
            "287": 6747956
          },
          "budget": 6904753.623703181
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150106",
        "ytm": {
          "actual": 21922491,
          "abs": 27041317.51688981,
          "percentage": 5.282718104953488,
          "budget": -5118826.516889812
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - FTE/Non-FTE - targets",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4500,
        "in_month": {
          "actual": 0,
          "abs": 2827079.0543427267,
          "percentage": 1,
          "products": {
            "287": 0
          },
          "budget": -2827079.0543427267
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150107",
        "ytm": {
          "actual": 0,
          "abs": 5845519.038902722,
          "percentage": 1,
          "budget": -5845519.038902722
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expenses - Project - targets",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4600,
        "in_month": {
          "actual": 0,
          "abs": 3441159.9951896733,
          "percentage": 1,
          "products": {
            "287": 0
          },
          "budget": -3441159.9951896733
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150109",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D capitalization",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4625,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150105",
        "ytm": {
          "actual": 64637404.38792001,
          "abs": 64637404.38792001
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "R&D Expense - Shared expenses",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4650,
        "in_month": {
          "actual": 170120,
          "abs": 170120,
          "products": {
            "287": 170120
          }
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150200",
        "ytm": {
          "actual": 356964898,
          "abs": 218660512.8609383,
          "percentage": 1.5810092546312287,
          "budget": 138304385.1390617
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales expense ",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4700,
        "in_month": {
          "actual": 70452549,
          "abs": 47070286.6425389,
          "percentage": 2.0130766614000946,
          "products": {
            "287": 70452549
          },
          "budget": 23382262.357461102
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150201",
        "ytm": {
          "actual": 342013947.78454,
          "abs": 226683519.9459401,
          "percentage": 1.9655135612882162,
          "budget": 115330427.83859989
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales expenses - MFG platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4800,
        "in_month": {
          "actual": 70390573,
          "abs": 47008310.6425389,
          "percentage": 2.010426105219836,
          "products": {
            "287": 70390573
          },
          "budget": 23382262.357461102
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150202",
        "ytm": {
          "actual": 0,
          "abs": -22973957.30046177,
          "percentage": -1,
          "budget": 22973957.30046177
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales expenses - PBU",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4900,
        "in_month": {
          "actual": 0,
          "abs": 0,
          "products": {
            "287": 0
          },
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150204",
        "ytm": {
          "actual": 0,
          "abs": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Bad debt expense",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 4950,
        "in_month": {
          "actual": 0,
          "abs": 0,
          "products": {
            "287": 0
          }
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150203",
        "ytm": {
          "actual": 14950950.215460002,
          "abs": 14950950.215460002,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Sales expenses - Shared expenses",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5000,
        "in_month": {
          "actual": 61976,
          "abs": 61976,
          "products": {
            "287": 61976
          },
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150300",
        "ytm": {
          "actual": 62889926,
          "abs": -1624042.1798818707,
          "percentage": -0.025173496929434182,
          "budget": 64513968.17988187
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Administrative expense",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5100,
        "in_month": {
          "actual": 11771312,
          "abs": 7780824.555404749,
          "percentage": 1.9498431365679803,
          "products": {
            "287": 11771312
          },
          "budget": 3990487.444595251
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150301",
        "ytm": {
          "actual": 15084062.973310001,
          "abs": -25804232.10562434,
          "percentage": -0.6310909284872258,
          "budget": 40888295.07893434
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Administrative expenses - MFG platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5200,
        "in_month": {
          "actual": 4343165,
          "abs": 1505525.3177831648,
          "percentage": 0.5305554920232194,
          "products": {
            "287": 4343165
          },
          "budget": 2837639.682216835
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150302",
        "ytm": {
          "actual": 6907957.41595,
          "abs": 9618632.736300204,
          "percentage": 3.548426720119891,
          "budget": -2710675.3203502027
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Administrative expenses - PBU",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5300,
        "in_month": {
          "actual": 836380,
          "abs": 5573280.967088147,
          "percentage": 1.1765669170225732,
          "products": {
            "287": 836380
          },
          "budget": -4736900.967088147
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0150303",
        "ytm": {
          "actual": 40897905.61074,
          "abs": 14561557.189442262,
          "percentage": 0.5529072199571369,
          "budget": 26336348.421297736
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Administrative expenses - Shared expenses",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5400,
        "in_month": {
          "actual": 6591767,
          "abs": 702018.2705334369,
          "percentage": 0.11919324622818314,
          "products": {
            "287": 6591767
          },
          "budget": 5889748.729466563
        }
      },
      {
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0290000",
        "ytm": {
          "actual": 0,
          "abs": -46656498.26541473,
          "percentage": -1,
          "budget": 46656498.26541473
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Over-delivery & leakage to be recovered",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5412,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0290100",
        "ytm": {
          "actual": 0,
          "abs": -46656498.26541473,
          "percentage": -1,
          "budget": 46656498.26541473
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Q1 over-delivery",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5425,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0290200",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Q1 Leakage to be recovered",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5437,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0250000",
        "ytm": {
          "actual": 5615204361.1899605,
          "abs": 5233627716.028627,
          "percentage": 13.715796766900686,
          "budget": 381576645.16133344
        },
        "item_level": 1,
        "item_type": "NME",
        "action_item_rich_text": "",
        "item_name": "Operating profit A - (excl. shared rev / exp)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5450,
        "in_month": {
          "actual": 109586291,
          "abs": 18443831.838979155,
          "percentage": 0.20236267496792626,
          "products": {
            "287": 109586291
          },
          "budget": 91142459.16102085
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0220000",
        "ytm": {
          "actual": 0.3741161770528132,
          "abs": 0.3231961751080467,
          "percentage": 6.3471359537382055,
          "budget": 0.05092000194476649
        },
        "item_level": 2,
        "item_type": "PEU",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Operating profit A %",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5467,
        "in_month": {
          "actual": 0.05807629830193635,
          "abs": -0.004123980233164501,
          "percentage": -0.06630163610661739,
          "products": {
            "287": 0.05807629830193635
          },
          "budget": 0.06220027853510085
        }
      },
      {
        "line_item": "0270000",
        "item_level": 1,
        "item_type": "C",
        "item_name": "PBU Uncontrollable Accounts",
        "item_order": 5484
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160000",
        "ytm": {
          "actual": 5511373086.19582,
          "abs": 5156132789.455784,
          "percentage": 14.51449296932953,
          "budget": 355240296.7400357
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Operating profit B - (incl. shared rev / exp)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5500,
        "in_month": {
          "actual": 102762428,
          "abs": 17509717.568445727,
          "percentage": 0.20538605142065863,
          "products": {
            "287": 102762428
          },
          "budget": 85252710.43155427
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160100",
        "ytm": {
          "actual": -44638224.14112,
          "abs": -27852186.009373754,
          "percentage": -1.6592471547350345,
          "budget": -16786038.131746247
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income)",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5600,
        "in_month": {
          "actual": -11067365,
          "abs": -2758664.2567468733,
          "percentage": -0.33202113567358627,
          "products": {
            "287": -11067365
          },
          "budget": -8308700.743253127
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160101",
        "ytm": {
          "actual": -14626082.14112,
          "abs": 2159955.9906262476,
          "percentage": 0.12867574669339488,
          "budget": -16786038.131746247
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income) - MFG platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5700,
        "in_month": {
          "actual": -11067365,
          "abs": -2758664.2567468733,
          "percentage": -0.33202113567358627,
          "products": {
            "287": -11067365
          },
          "budget": -8308700.743253127
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160102",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income) - Procurement platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5800,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160103",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income) - R&D platform",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 5900,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160104",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income) - PBU",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6000,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160105",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 3,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Other expense (income) - D Group level",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6100,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160200",
        "ytm": {
          "actual": 44674651.44843,
          "abs": 8577181.400176153,
          "percentage": 0.23761170488431663,
          "budget": 36097470.04825385
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Corporate Allocation",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6200,
        "in_month": {
          "actual": 7684029,
          "abs": 221732.47837393824,
          "percentage": 0.02971370512165361,
          "products": {
            "287": 7684029
          },
          "budget": 7462296.521626062
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0160300",
        "ytm": {
          "actual": 84886764.9491,
          "abs": 6904093.128455952,
          "percentage": 0.08853368276910278,
          "budget": 77982671.82064405
        },
        "item_level": 2,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "ICC",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6300,
        "in_month": {
          "actual": 12411398,
          "abs": 127582.21441720612,
          "percentage": 0.010386203818437766,
          "products": {
            "287": 12411398
          },
          "budget": 12283815.785582794
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0170000",
        "ytm": {
          "actual": 5426449894.2173,
          "abs": 5168503701.2144165,
          "percentage": 20.03713891275235,
          "budget": 257946193.0028841
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Profit before tax",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6400,
        "in_month": {
          "actual": 93734366,
          "abs": 19919067.13240145,
          "percentage": 0.26985011830853656,
          "products": {
            "287": 93734366
          },
          "budget": 73815298.86759855
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0230000",
        "ytm": {
          "actual": 0.3615403035773039,
          "abs": 0.3271183314364289,
          "percentage": 9.50318389944852,
          "budget": 0.03442197214087501
        },
        "item_level": 2,
        "item_type": "PEU",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Profit before tax %",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6450,
        "in_month": {
          "actual": 0.049675419719779366,
          "abs": -0.0006999178778694382,
          "percentage": -0.013894058308050045,
          "products": {
            "287": 0.049675419719779366
          },
          "budget": 0.050375337597648805
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0180000",
        "ytm": {
          "actual": 73307264.04818,
          "abs": 27246964.653614722,
          "percentage": 0.5915498816064932,
          "budget": 46060299.39456528
        },
        "item_level": 1,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Income tax",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6500,
        "in_month": {
          "actual": 20148445,
          "abs": 6350647.1340558585,
          "percentage": 0.4602652681070642,
          "products": {
            "287": 20148445
          },
          "budget": 13797797.865944142
        }
      },
      {
        "item_flag": "negative",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0300000",
        "ytm": {
          "actual": 0,
          "abs": 0,
          "budget": 0
        },
        "item_level": 1,
        "item_type": "N",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Income tax - Gov't subsidy",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6550,
        "in_month": {
          "abs": 0,
          "products": {},
          "budget": 0
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0190000",
        "ytm": {
          "actual": 5353142630.16912,
          "abs": 5141256736.560801,
          "percentage": 24.264270966828303,
          "budget": 211885893.6083188
        },
        "item_level": 1,
        "item_type": "NME",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Net income",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6600,
        "in_month": {
          "actual": 73585921,
          "abs": 13568419.998345591,
          "percentage": 0.22607439116752914,
          "products": {
            "287": 73585921
          },
          "budget": 60017501.00165441
        }
      },
      {
        "item_flag": "positive",
        "in_month_deviation_explanation_rich_text": "",
        "line_item": "0240000",
        "ytm": {
          "actual": 0.3566561655100485,
          "abs": 0.32838077116550624,
          "percentage": 11.613658404339478,
          "budget": 0.028275394344542265
        },
        "item_level": 2,
        "item_type": "PEU",
        "decimal_place": 2,
        "action_item_rich_text": "",
        "item_name": "Net income %",
        "in_month_deviation_explanation": "",
        "action_item": "",
        "item_order": 6700,
        "in_month": {
          "actual": 0.0389975594558513,
          "abs": -0.001961454747269366,
          "percentage": -0.047888231331503155,
          "products": {
            "287": 0.0389975594558513
          },
          "budget": 0.04095901420312067
        }
      }
    ],
    "actual_version_id": 3,
    "currency_id": 3,
    "products": [
      {
        "name": "Procuct XX",
        "key": "287"
      }
    ],
    "status": "3"
  },
  "result": true
};